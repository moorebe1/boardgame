﻿using System.Windows.Forms;

namespace ScrumGame
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.gameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rulesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.Player4Panel = new System.Windows.Forms.Panel();
            this.Player4BudgetAmountLabel = new System.Windows.Forms.Label();
            this.Player4ScoreAmountLabel = new System.Windows.Forms.Label();
            this.Player4BudgetLabel = new System.Windows.Forms.Label();
            this.Player4FullStackDevelopersLabel = new System.Windows.Forms.Label();
            this.Player4MoneyAmountLabel = new System.Windows.Forms.Label();
            this.Player4EpicAmountLabel = new System.Windows.Forms.Label();
            this.Player4BackEndDevelopersLabel = new System.Windows.Forms.Label();
            this.Player4FeatureAmountLabel = new System.Windows.Forms.Label();
            this.Player4FrontEndDevelopersLabel = new System.Windows.Forms.Label();
            this.Player4StoryAmountLabel = new System.Windows.Forms.Label();
            this.Player4ScrumMastersLabel = new System.Windows.Forms.Label();
            this.Player4TaskAmountLabel = new System.Windows.Forms.Label();
            this.Player4ProductOwnersLabel = new System.Windows.Forms.Label();
            this.Player4FullStackDeveloperHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player4BackEndDeveloperHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player4FrontEndDeveloperHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player4ScrumMasterHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player4ProductOwnerHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player4EpicsLabel = new System.Windows.Forms.Label();
            this.Player4FeaturesLabel = new System.Windows.Forms.Label();
            this.Player4StoriesLabel = new System.Windows.Forms.Label();
            this.Player4TasksLabel = new System.Windows.Forms.Label();
            this.Player4ClientCardHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player4TechnologyCardHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player4ResearchLabel = new System.Windows.Forms.Label();
            this.Player4Research3PictureBox = new System.Windows.Forms.PictureBox();
            this.Player4Research2PictureBox = new System.Windows.Forms.PictureBox();
            this.Player4Research1PictureBox = new System.Windows.Forms.PictureBox();
            this.Player4ScoreLabel = new System.Windows.Forms.Label();
            this.Player4MoneyLabel = new System.Windows.Forms.Label();
            this.Player4TitleTextBox = new System.Windows.Forms.TextBox();
            this.Player3Panel = new System.Windows.Forms.Panel();
            this.Player3BudgetAmountLabel = new System.Windows.Forms.Label();
            this.Player3ScoreAmountLabel = new System.Windows.Forms.Label();
            this.Player3BudgetLabel = new System.Windows.Forms.Label();
            this.Player3FullStackDevelopersLabel = new System.Windows.Forms.Label();
            this.Player3MoneyAmountLabel = new System.Windows.Forms.Label();
            this.Player3EpicAmountLabel = new System.Windows.Forms.Label();
            this.Player3BackEndDevelopersLabel = new System.Windows.Forms.Label();
            this.Player3FeatureAmountLabel = new System.Windows.Forms.Label();
            this.Player3FrontEndDevelopersLabel = new System.Windows.Forms.Label();
            this.Player3StoryAmountLabel = new System.Windows.Forms.Label();
            this.Player3ScrumMastersLabel = new System.Windows.Forms.Label();
            this.Player3TaskAmountLabel = new System.Windows.Forms.Label();
            this.Player3ProductOwnersLabel = new System.Windows.Forms.Label();
            this.Player3FullStackDeveloperHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player3BackEndDeveloperHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player3FrontEndDeveloperHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player3ScrumMasterHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player3ProductOwnerHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player3EpicsLabel = new System.Windows.Forms.Label();
            this.Player3FeaturesLabel = new System.Windows.Forms.Label();
            this.Player3StoriesLabel = new System.Windows.Forms.Label();
            this.Player3TasksLabel = new System.Windows.Forms.Label();
            this.Player3ClientCardHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player3TechnologyCardHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player3ResearchLabel = new System.Windows.Forms.Label();
            this.Player3Research3PictureBox = new System.Windows.Forms.PictureBox();
            this.Player3Research2PictureBox = new System.Windows.Forms.PictureBox();
            this.Player3Research1PictureBox = new System.Windows.Forms.PictureBox();
            this.Player3ScoreLabel = new System.Windows.Forms.Label();
            this.Player3MoneyLabel = new System.Windows.Forms.Label();
            this.Player3TitleTextBox = new System.Windows.Forms.TextBox();
            this.Player2Panel = new System.Windows.Forms.Panel();
            this.Player2BudgetAmountLabel = new System.Windows.Forms.Label();
            this.Player2ScoreAmountLabel = new System.Windows.Forms.Label();
            this.Player2BudgetLabel = new System.Windows.Forms.Label();
            this.Player2FullStackDevelopersLabel = new System.Windows.Forms.Label();
            this.Player2MoneyAmountLabel = new System.Windows.Forms.Label();
            this.Player2EpicAmountLabel = new System.Windows.Forms.Label();
            this.Player2BackEndDevelopersLabel = new System.Windows.Forms.Label();
            this.Player2FeatureAmountLabel = new System.Windows.Forms.Label();
            this.Player2FrontEndDevelopersLabel = new System.Windows.Forms.Label();
            this.Player2StoryAmountLabel = new System.Windows.Forms.Label();
            this.Player2ScrumMastersLabel = new System.Windows.Forms.Label();
            this.Player2TaskAmountLabel = new System.Windows.Forms.Label();
            this.Player2ProductOwnersLabel = new System.Windows.Forms.Label();
            this.Player2FullStackDeveloperHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player2BackEndDeveloperHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player2FrontEndDeveloperHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player2ScrumMasterHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player2ProductOwnerHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player2EpicsLabel = new System.Windows.Forms.Label();
            this.Player2FeaturesLabel = new System.Windows.Forms.Label();
            this.Player2StoriesLabel = new System.Windows.Forms.Label();
            this.Player2TasksLabel = new System.Windows.Forms.Label();
            this.Player2ClientCardHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player2TechnologyCardHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player2ResearchLabel = new System.Windows.Forms.Label();
            this.Player2Research3PictureBox = new System.Windows.Forms.PictureBox();
            this.Player2Research2PictureBox = new System.Windows.Forms.PictureBox();
            this.Player2Research1PictureBox = new System.Windows.Forms.PictureBox();
            this.Player2ScoreLabel = new System.Windows.Forms.Label();
            this.Player2MoneyLabel = new System.Windows.Forms.Label();
            this.Player2TitleTextBox = new System.Windows.Forms.TextBox();
            this.CenterPanel = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.ScrumMasterSupplyLabel = new System.Windows.Forms.Label();
            this.ProductOwnerSupplyLabel = new System.Windows.Forms.Label();
            this.ScrumMasterSupplyPictureBox = new System.Windows.Forms.PictureBox();
            this.ProductOwnerSupplyPictureBox = new System.Windows.Forms.PictureBox();
            this.TextLabel = new System.Windows.Forms.Label();
            this.DiceAmount = new System.Windows.Forms.TextBox();
            this.diceImage6 = new System.Windows.Forms.PictureBox();
            this.diceImage5 = new System.Windows.Forms.PictureBox();
            this.diceImage4 = new System.Windows.Forms.PictureBox();
            this.diceImage3 = new System.Windows.Forms.PictureBox();
            this.diceImage2 = new System.Windows.Forms.PictureBox();
            this.diceImage1 = new System.Windows.Forms.PictureBox();

            this.RollDiceBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();

            this.button1 = new System.Windows.Forms.Button();
            this.FullStackDeveloperSupplyLabel = new System.Windows.Forms.Label();
            this.Dice7PictureBox = new System.Windows.Forms.PictureBox();
            this.BackEndDeveloperSupplyLabel = new System.Windows.Forms.Label();
            this.Dice6PictureBox = new System.Windows.Forms.PictureBox();
            this.FrontEndDeveloperSupplyLabel = new System.Windows.Forms.Label();
            this.Dice5PictureBox = new System.Windows.Forms.PictureBox();
            this.FullStackDeveloperSupplyPictureBox = new System.Windows.Forms.PictureBox();
            this.Dice4PictureBox = new System.Windows.Forms.PictureBox();
            this.BackEndDeveloperSupplyPictureBox = new System.Windows.Forms.PictureBox();
            this.FrontEndDeveloperSupplyPictureBox = new System.Windows.Forms.PictureBox();
            this.Dice3PictureBox = new System.Windows.Forms.PictureBox();
            this.Dice2PictureBox = new System.Windows.Forms.PictureBox();
            this.Dice1PictureBox = new System.Windows.Forms.PictureBox();
            this.WorkerSupplyPictureBox = new System.Windows.Forms.PictureBox();
            this.GameBoardPanel = new System.Windows.Forms.Panel();
            this.Player4BudgetPictureBox = new System.Windows.Forms.PictureBox();
            this.Player3BudgetPictureBox = new System.Windows.Forms.PictureBox();
            this.Player2BudgetPictureBox = new System.Windows.Forms.PictureBox();
            this.Player1BudgetPictureBox = new System.Windows.Forms.PictureBox();
            this.TechnologyCardSlotsPictureBox = new System.Windows.Forms.PictureBox();
            this.EpicSupplyLabel = new System.Windows.Forms.Label();
            this.FeatureSupplyLabel = new System.Windows.Forms.Label();
            this.ContractOutPanel = new System.Windows.Forms.Panel();
            this.BudgetStripPictureBox = new System.Windows.Forms.PictureBox();
            this.TechnologyCard4PictureBox = new System.Windows.Forms.PictureBox();
            this.BigWigPanel = new System.Windows.Forms.Panel();
            this.TechnologyCard3PictureBox = new System.Windows.Forms.PictureBox();
            this.JobFairPanel = new System.Windows.Forms.Panel();
            this.TechnologyCard2PictureBox = new System.Windows.Forms.PictureBox();
            this.GooglePanel = new System.Windows.Forms.Panel();
            this.TechnologyCard1PictureBox = new System.Windows.Forms.PictureBox();
            this.Client4PictureBox = new System.Windows.Forms.PictureBox();
            this.TasklandPanel = new System.Windows.Forms.Panel();
            this.TaskSupplyLabel = new System.Windows.Forms.Label();
            this.Client3PictureBox = new System.Windows.Forms.PictureBox();
            this.StorylandPanel = new System.Windows.Forms.Panel();
            this.StorySupplyLabel = new System.Windows.Forms.Label();
            this.Client2PictureBox = new System.Windows.Forms.PictureBox();
            this.EpiclandPanel = new System.Windows.Forms.Panel();
            this.Client1PictureBox = new System.Windows.Forms.PictureBox();
            this.FeaturelandPanel = new System.Windows.Forms.Panel();
            this.TechnologyCardSupplyPictureBox = new System.Windows.Forms.PictureBox();
            this.Player1Panel = new System.Windows.Forms.Panel();
            this.Player1BudgetAmountLabel = new System.Windows.Forms.Label();
            this.Player1BudgetLabel = new System.Windows.Forms.Label();
            this.Player1ScoreAmountLabel = new System.Windows.Forms.Label();
            this.Player1MoneyAmountLabel = new System.Windows.Forms.Label();
            this.Player1EpicAmountLabel = new System.Windows.Forms.Label();
            this.Player1FeatureAmountLabel = new System.Windows.Forms.Label();
            this.Player1StoryAmountLabel = new System.Windows.Forms.Label();
            this.Player1TaskAmountLabel = new System.Windows.Forms.Label();
            this.Player1FullStackDevelopersLabel = new System.Windows.Forms.Label();
            this.Player1BackEndDevelopersLabel = new System.Windows.Forms.Label();
            this.Player1FrontEndDevelopersLabel = new System.Windows.Forms.Label();
            this.Player1ScrumMastersLabel = new System.Windows.Forms.Label();
            this.Player1ProductOwnersLabel = new System.Windows.Forms.Label();
            this.Player1FullStackDeveloperHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player1BackEndDeveloperHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player1FrontEndDeveloperHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player1ScrumMasterHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player1ProductOwnerHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player1EpicsLabel = new System.Windows.Forms.Label();
            this.Player1FeaturesLabel = new System.Windows.Forms.Label();
            this.Player1StoriesLabel = new System.Windows.Forms.Label();
            this.Player1TasksLabel = new System.Windows.Forms.Label();
            this.Player1ClientCardHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player1TechnologyCardHomePictureBox = new System.Windows.Forms.PictureBox();
            this.Player1ResearchLabel = new System.Windows.Forms.Label();
            this.Player1Research3PictureBox = new System.Windows.Forms.PictureBox();
            this.Player1Research2PictureBox = new System.Windows.Forms.PictureBox();
            this.Player1Research1PictureBox = new System.Windows.Forms.PictureBox();
            this.Player1ScoreLabel = new System.Windows.Forms.Label();
            this.Player1MoneyLabel = new System.Windows.Forms.Label();
            this.Player1TitleTextBox = new System.Windows.Forms.TextBox();
            this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.MainTableLayoutPanel.SuspendLayout();
            this.Player4Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Player4FullStackDeveloperHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4BackEndDeveloperHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4FrontEndDeveloperHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4ScrumMasterHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4ProductOwnerHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4ClientCardHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4TechnologyCardHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4Research3PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4Research2PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4Research1PictureBox)).BeginInit();
            this.Player3Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Player3FullStackDeveloperHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3BackEndDeveloperHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3FrontEndDeveloperHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3ScrumMasterHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3ProductOwnerHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3ClientCardHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3TechnologyCardHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3Research3PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3Research2PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3Research1PictureBox)).BeginInit();
            this.Player2Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Player2FullStackDeveloperHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2BackEndDeveloperHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2FrontEndDeveloperHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2ScrumMasterHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2ProductOwnerHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2ClientCardHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2TechnologyCardHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2Research3PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2Research2PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2Research1PictureBox)).BeginInit();
            this.CenterPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScrumMasterSupplyPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProductOwnerSupplyPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceImage6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceImage5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceImage4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceImage3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceImage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceImage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice7PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice6PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice5PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FullStackDeveloperSupplyPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice4PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackEndDeveloperSupplyPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrontEndDeveloperSupplyPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice3PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice2PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice1PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkerSupplyPictureBox)).BeginInit();
            this.GameBoardPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Player4BudgetPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3BudgetPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2BudgetPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1BudgetPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TechnologyCardSlotsPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BudgetStripPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TechnologyCard4PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TechnologyCard3PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TechnologyCard2PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TechnologyCard1PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Client4PictureBox)).BeginInit();
            this.TasklandPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Client3PictureBox)).BeginInit();
            this.StorylandPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Client2PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Client1PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TechnologyCardSupplyPictureBox)).BeginInit();
            this.Player1Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Player1FullStackDeveloperHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1BackEndDeveloperHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1FrontEndDeveloperHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1ScrumMasterHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1ProductOwnerHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1ClientCardHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1TechnologyCardHomePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1Research3PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1Research2PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1Research1PictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gameToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1685, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // gameToolStripMenuItem
            // 
            this.gameToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.openToolStripMenuItem,
            this.resetToolStripMenuItem,
            this.rulesToolStripMenuItem});
            this.gameToolStripMenuItem.Name = "gameToolStripMenuItem";
            this.gameToolStripMenuItem.Size = new System.Drawing.Size(62, 24);
            this.gameToolStripMenuItem.Text = "Game";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";

            this.saveToolStripMenuItem.Size = new System.Drawing.Size(128, 26);

            this.saveToolStripMenuItem.Size = new System.Drawing.Size(224, 26);

            this.saveToolStripMenuItem.Size = new System.Drawing.Size(224, 26);

            this.saveToolStripMenuItem.Text = "Save";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";

            this.openToolStripMenuItem.Size = new System.Drawing.Size(128, 26);

            this.openToolStripMenuItem.Size = new System.Drawing.Size(224, 26);


            this.openToolStripMenuItem.Size = new System.Drawing.Size(224, 26);

            this.openToolStripMenuItem.Text = "Open";
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";

            this.resetToolStripMenuItem.Size = new System.Drawing.Size(128, 26);

            this.resetToolStripMenuItem.Size = new System.Drawing.Size(224, 26);

            this.resetToolStripMenuItem.Size = new System.Drawing.Size(224, 26);

            this.resetToolStripMenuItem.Text = "Reset";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // rulesToolStripMenuItem
            // 
            this.rulesToolStripMenuItem.Name = "rulesToolStripMenuItem";

            this.rulesToolStripMenuItem.Size = new System.Drawing.Size(128, 26);

            this.rulesToolStripMenuItem.Size = new System.Drawing.Size(224, 26);

            this.rulesToolStripMenuItem.Size = new System.Drawing.Size(224, 26);

            this.rulesToolStripMenuItem.Text = "Rules";
            this.rulesToolStripMenuItem.Click += new System.EventHandler(this.rulesToolStripMenuItem_Click);
            // 
            // MainTableLayoutPanel
            // 
            this.MainTableLayoutPanel.AutoSize = true;
            this.MainTableLayoutPanel.ColumnCount = 3;
            this.MainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.MainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.MainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.MainTableLayoutPanel.Controls.Add(this.Player4Panel, 0, 1);
            this.MainTableLayoutPanel.Controls.Add(this.Player3Panel, 2, 1);
            this.MainTableLayoutPanel.Controls.Add(this.Player2Panel, 2, 0);
            this.MainTableLayoutPanel.Controls.Add(this.CenterPanel, 1, 0);
            this.MainTableLayoutPanel.Controls.Add(this.Player1Panel, 0, 0);
            this.MainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTableLayoutPanel.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this.MainTableLayoutPanel.Location = new System.Drawing.Point(0, 28);
            this.MainTableLayoutPanel.Margin = new System.Windows.Forms.Padding(4);
            this.MainTableLayoutPanel.Name = "MainTableLayoutPanel";
            this.MainTableLayoutPanel.RowCount = 2;
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.MainTableLayoutPanel.Size = new System.Drawing.Size(1685, 869);
            this.MainTableLayoutPanel.TabIndex = 6;
            // 
            // Player4Panel
            // 
            this.Player4Panel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Player4Panel.BackColor = System.Drawing.Color.White;
            this.Player4Panel.Controls.Add(this.Player4BudgetAmountLabel);
            this.Player4Panel.Controls.Add(this.Player4ScoreAmountLabel);
            this.Player4Panel.Controls.Add(this.Player4BudgetLabel);
            this.Player4Panel.Controls.Add(this.Player4FullStackDevelopersLabel);
            this.Player4Panel.Controls.Add(this.Player4MoneyAmountLabel);
            this.Player4Panel.Controls.Add(this.Player4EpicAmountLabel);
            this.Player4Panel.Controls.Add(this.Player4BackEndDevelopersLabel);
            this.Player4Panel.Controls.Add(this.Player4FeatureAmountLabel);
            this.Player4Panel.Controls.Add(this.Player4FrontEndDevelopersLabel);
            this.Player4Panel.Controls.Add(this.Player4StoryAmountLabel);
            this.Player4Panel.Controls.Add(this.Player4ScrumMastersLabel);
            this.Player4Panel.Controls.Add(this.Player4TaskAmountLabel);
            this.Player4Panel.Controls.Add(this.Player4ProductOwnersLabel);
            this.Player4Panel.Controls.Add(this.Player4FullStackDeveloperHomePictureBox);
            this.Player4Panel.Controls.Add(this.Player4BackEndDeveloperHomePictureBox);
            this.Player4Panel.Controls.Add(this.Player4FrontEndDeveloperHomePictureBox);
            this.Player4Panel.Controls.Add(this.Player4ScrumMasterHomePictureBox);
            this.Player4Panel.Controls.Add(this.Player4ProductOwnerHomePictureBox);
            this.Player4Panel.Controls.Add(this.Player4EpicsLabel);
            this.Player4Panel.Controls.Add(this.Player4FeaturesLabel);
            this.Player4Panel.Controls.Add(this.Player4StoriesLabel);
            this.Player4Panel.Controls.Add(this.Player4TasksLabel);
            this.Player4Panel.Controls.Add(this.Player4ClientCardHomePictureBox);
            this.Player4Panel.Controls.Add(this.Player4TechnologyCardHomePictureBox);
            this.Player4Panel.Controls.Add(this.Player4ResearchLabel);
            this.Player4Panel.Controls.Add(this.Player4Research3PictureBox);
            this.Player4Panel.Controls.Add(this.Player4Research2PictureBox);
            this.Player4Panel.Controls.Add(this.Player4Research1PictureBox);
            this.Player4Panel.Controls.Add(this.Player4ScoreLabel);
            this.Player4Panel.Controls.Add(this.Player4MoneyLabel);
            this.Player4Panel.Controls.Add(this.Player4TitleTextBox);
            this.Player4Panel.Location = new System.Drawing.Point(4, 438);
            this.Player4Panel.Margin = new System.Windows.Forms.Padding(4);
            this.Player4Panel.Name = "Player4Panel";
            this.Player4Panel.Size = new System.Drawing.Size(328, 426);
            this.Player4Panel.TabIndex = 45;
            // 
            // Player4BudgetAmountLabel
            // 
            this.Player4BudgetAmountLabel.AutoSize = true;
            this.Player4BudgetAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4BudgetAmountLabel.Location = new System.Drawing.Point(129, 96);
            this.Player4BudgetAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4BudgetAmountLabel.Name = "Player4BudgetAmountLabel";
            this.Player4BudgetAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player4BudgetAmountLabel.TabIndex = 54;
            this.Player4BudgetAmountLabel.Text = "0";
            this.Player4BudgetAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player4BudgetAmountLabel.UseMnemonic = false;
            // 
            // Player4ScoreAmountLabel
            // 
            this.Player4ScoreAmountLabel.AutoSize = true;
            this.Player4ScoreAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4ScoreAmountLabel.Location = new System.Drawing.Point(128, 75);
            this.Player4ScoreAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4ScoreAmountLabel.Name = "Player4ScoreAmountLabel";
            this.Player4ScoreAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player4ScoreAmountLabel.TabIndex = 56;
            this.Player4ScoreAmountLabel.Text = "0";
            this.Player4ScoreAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player4ScoreAmountLabel.UseMnemonic = false;
            // 
            // Player4BudgetLabel
            // 
            this.Player4BudgetLabel.AutoSize = true;
            this.Player4BudgetLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4BudgetLabel.Location = new System.Drawing.Point(13, 96);
            this.Player4BudgetLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4BudgetLabel.Name = "Player4BudgetLabel";
            this.Player4BudgetLabel.Size = new System.Drawing.Size(80, 25);
            this.Player4BudgetLabel.TabIndex = 53;
            this.Player4BudgetLabel.Text = "Budget:";
            this.Player4BudgetLabel.UseMnemonic = false;
            // 
            // Player4FullStackDevelopersLabel
            // 
            this.Player4FullStackDevelopersLabel.AutoSize = true;
            this.Player4FullStackDevelopersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4FullStackDevelopersLabel.Location = new System.Drawing.Point(276, 327);
            this.Player4FullStackDevelopersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4FullStackDevelopersLabel.Name = "Player4FullStackDevelopersLabel";
            this.Player4FullStackDevelopersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player4FullStackDevelopersLabel.TabIndex = 44;
            this.Player4FullStackDevelopersLabel.Text = "0";
            this.Player4FullStackDevelopersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player4FullStackDevelopersLabel.UseMnemonic = false;
            // 
            // Player4MoneyAmountLabel
            // 
            this.Player4MoneyAmountLabel.AutoSize = true;
            this.Player4MoneyAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4MoneyAmountLabel.Location = new System.Drawing.Point(128, 50);
            this.Player4MoneyAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4MoneyAmountLabel.Name = "Player4MoneyAmountLabel";
            this.Player4MoneyAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player4MoneyAmountLabel.TabIndex = 55;
            this.Player4MoneyAmountLabel.Text = "0";
            this.Player4MoneyAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player4MoneyAmountLabel.UseMnemonic = false;
            // 
            // Player4EpicAmountLabel
            // 
            this.Player4EpicAmountLabel.AutoSize = true;
            this.Player4EpicAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4EpicAmountLabel.Location = new System.Drawing.Point(128, 283);
            this.Player4EpicAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4EpicAmountLabel.Name = "Player4EpicAmountLabel";
            this.Player4EpicAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player4EpicAmountLabel.TabIndex = 54;
            this.Player4EpicAmountLabel.Text = "0";
            this.Player4EpicAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player4EpicAmountLabel.UseMnemonic = false;
            // 
            // Player4BackEndDevelopersLabel
            // 
            this.Player4BackEndDevelopersLabel.AutoSize = true;
            this.Player4BackEndDevelopersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4BackEndDevelopersLabel.Location = new System.Drawing.Point(215, 327);
            this.Player4BackEndDevelopersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4BackEndDevelopersLabel.Name = "Player4BackEndDevelopersLabel";
            this.Player4BackEndDevelopersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player4BackEndDevelopersLabel.TabIndex = 43;
            this.Player4BackEndDevelopersLabel.Text = "0";
            this.Player4BackEndDevelopersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player4BackEndDevelopersLabel.UseMnemonic = false;
            // 
            // Player4FeatureAmountLabel
            // 
            this.Player4FeatureAmountLabel.AutoSize = true;
            this.Player4FeatureAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4FeatureAmountLabel.Location = new System.Drawing.Point(128, 258);
            this.Player4FeatureAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4FeatureAmountLabel.Name = "Player4FeatureAmountLabel";
            this.Player4FeatureAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player4FeatureAmountLabel.TabIndex = 53;
            this.Player4FeatureAmountLabel.Text = "0";
            this.Player4FeatureAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player4FeatureAmountLabel.UseMnemonic = false;
            // 
            // Player4FrontEndDevelopersLabel
            // 
            this.Player4FrontEndDevelopersLabel.AutoSize = true;
            this.Player4FrontEndDevelopersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4FrontEndDevelopersLabel.Location = new System.Drawing.Point(153, 327);
            this.Player4FrontEndDevelopersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4FrontEndDevelopersLabel.Name = "Player4FrontEndDevelopersLabel";
            this.Player4FrontEndDevelopersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player4FrontEndDevelopersLabel.TabIndex = 42;
            this.Player4FrontEndDevelopersLabel.Text = "0";
            this.Player4FrontEndDevelopersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player4FrontEndDevelopersLabel.UseMnemonic = false;
            // 
            // Player4StoryAmountLabel
            // 
            this.Player4StoryAmountLabel.AutoSize = true;
            this.Player4StoryAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4StoryAmountLabel.Location = new System.Drawing.Point(128, 234);
            this.Player4StoryAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4StoryAmountLabel.Name = "Player4StoryAmountLabel";
            this.Player4StoryAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player4StoryAmountLabel.TabIndex = 52;
            this.Player4StoryAmountLabel.Text = "0";
            this.Player4StoryAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player4StoryAmountLabel.UseMnemonic = false;
            // 
            // Player4ScrumMastersLabel
            // 
            this.Player4ScrumMastersLabel.AutoSize = true;
            this.Player4ScrumMastersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4ScrumMastersLabel.Location = new System.Drawing.Point(92, 327);
            this.Player4ScrumMastersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4ScrumMastersLabel.Name = "Player4ScrumMastersLabel";
            this.Player4ScrumMastersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player4ScrumMastersLabel.TabIndex = 41;
            this.Player4ScrumMastersLabel.Text = "0";
            this.Player4ScrumMastersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player4ScrumMastersLabel.UseMnemonic = false;
            // 
            // Player4TaskAmountLabel
            // 
            this.Player4TaskAmountLabel.AutoSize = true;
            this.Player4TaskAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4TaskAmountLabel.Location = new System.Drawing.Point(128, 209);
            this.Player4TaskAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4TaskAmountLabel.Name = "Player4TaskAmountLabel";
            this.Player4TaskAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player4TaskAmountLabel.TabIndex = 51;
            this.Player4TaskAmountLabel.Text = "0";
            this.Player4TaskAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player4TaskAmountLabel.UseMnemonic = false;
            // 
            // Player4ProductOwnersLabel
            // 
            this.Player4ProductOwnersLabel.AutoSize = true;
            this.Player4ProductOwnersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4ProductOwnersLabel.Location = new System.Drawing.Point(31, 327);
            this.Player4ProductOwnersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4ProductOwnersLabel.Name = "Player4ProductOwnersLabel";
            this.Player4ProductOwnersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player4ProductOwnersLabel.TabIndex = 40;
            this.Player4ProductOwnersLabel.Text = "0";
            this.Player4ProductOwnersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player4ProductOwnersLabel.UseMnemonic = false;
            // 
            // Player4FullStackDeveloperHomePictureBox
            // 
            this.Player4FullStackDeveloperHomePictureBox.Location = new System.Drawing.Point(260, 356);
            this.Player4FullStackDeveloperHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player4FullStackDeveloperHomePictureBox.Name = "Player4FullStackDeveloperHomePictureBox";
            this.Player4FullStackDeveloperHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player4FullStackDeveloperHomePictureBox.TabIndex = 39;
            this.Player4FullStackDeveloperHomePictureBox.TabStop = false;
            // 
            // Player4BackEndDeveloperHomePictureBox
            // 
            this.Player4BackEndDeveloperHomePictureBox.Location = new System.Drawing.Point(199, 356);
            this.Player4BackEndDeveloperHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player4BackEndDeveloperHomePictureBox.Name = "Player4BackEndDeveloperHomePictureBox";
            this.Player4BackEndDeveloperHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player4BackEndDeveloperHomePictureBox.TabIndex = 38;
            this.Player4BackEndDeveloperHomePictureBox.TabStop = false;
            // 
            // Player4FrontEndDeveloperHomePictureBox
            // 
            this.Player4FrontEndDeveloperHomePictureBox.Location = new System.Drawing.Point(137, 356);
            this.Player4FrontEndDeveloperHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player4FrontEndDeveloperHomePictureBox.Name = "Player4FrontEndDeveloperHomePictureBox";
            this.Player4FrontEndDeveloperHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player4FrontEndDeveloperHomePictureBox.TabIndex = 37;
            this.Player4FrontEndDeveloperHomePictureBox.TabStop = false;
            // 
            // Player4ScrumMasterHomePictureBox
            // 
            this.Player4ScrumMasterHomePictureBox.Location = new System.Drawing.Point(76, 356);
            this.Player4ScrumMasterHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player4ScrumMasterHomePictureBox.Name = "Player4ScrumMasterHomePictureBox";
            this.Player4ScrumMasterHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player4ScrumMasterHomePictureBox.TabIndex = 36;
            this.Player4ScrumMasterHomePictureBox.TabStop = false;
            // 
            // Player4ProductOwnerHomePictureBox
            // 
            this.Player4ProductOwnerHomePictureBox.Location = new System.Drawing.Point(15, 356);
            this.Player4ProductOwnerHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player4ProductOwnerHomePictureBox.Name = "Player4ProductOwnerHomePictureBox";
            this.Player4ProductOwnerHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player4ProductOwnerHomePictureBox.TabIndex = 35;
            this.Player4ProductOwnerHomePictureBox.TabStop = false;
            // 
            // Player4EpicsLabel
            // 
            this.Player4EpicsLabel.AutoSize = true;
            this.Player4EpicsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4EpicsLabel.Location = new System.Drawing.Point(13, 279);
            this.Player4EpicsLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4EpicsLabel.Name = "Player4EpicsLabel";
            this.Player4EpicsLabel.Size = new System.Drawing.Size(66, 25);
            this.Player4EpicsLabel.TabIndex = 34;
            this.Player4EpicsLabel.Text = "Epics:";
            this.Player4EpicsLabel.UseMnemonic = false;
            // 
            // Player4FeaturesLabel
            // 
            this.Player4FeaturesLabel.AutoSize = true;
            this.Player4FeaturesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4FeaturesLabel.Location = new System.Drawing.Point(13, 255);
            this.Player4FeaturesLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4FeaturesLabel.Name = "Player4FeaturesLabel";
            this.Player4FeaturesLabel.Size = new System.Drawing.Size(95, 25);
            this.Player4FeaturesLabel.TabIndex = 33;
            this.Player4FeaturesLabel.Text = "Features:";
            this.Player4FeaturesLabel.UseMnemonic = false;
            // 
            // Player4StoriesLabel
            // 
            this.Player4StoriesLabel.AutoSize = true;
            this.Player4StoriesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4StoriesLabel.Location = new System.Drawing.Point(13, 230);
            this.Player4StoriesLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4StoriesLabel.Name = "Player4StoriesLabel";
            this.Player4StoriesLabel.Size = new System.Drawing.Size(79, 25);
            this.Player4StoriesLabel.TabIndex = 32;
            this.Player4StoriesLabel.Text = "Stories:";
            this.Player4StoriesLabel.UseMnemonic = false;
            // 
            // Player4TasksLabel
            // 
            this.Player4TasksLabel.AutoSize = true;
            this.Player4TasksLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4TasksLabel.Location = new System.Drawing.Point(13, 206);
            this.Player4TasksLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4TasksLabel.Name = "Player4TasksLabel";
            this.Player4TasksLabel.Size = new System.Drawing.Size(72, 25);
            this.Player4TasksLabel.TabIndex = 31;
            this.Player4TasksLabel.Text = "Tasks:";
            this.Player4TasksLabel.UseMnemonic = false;
            // 
            // Player4ClientCardHomePictureBox
            // 
            this.Player4ClientCardHomePictureBox.BackColor = System.Drawing.Color.Transparent;
            this.Player4ClientCardHomePictureBox.Location = new System.Drawing.Point(217, 206);
            this.Player4ClientCardHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player4ClientCardHomePictureBox.Name = "Player4ClientCardHomePictureBox";
            this.Player4ClientCardHomePictureBox.Size = new System.Drawing.Size(80, 111);
            this.Player4ClientCardHomePictureBox.TabIndex = 18;
            this.Player4ClientCardHomePictureBox.TabStop = false;
            // 
            // Player4TechnologyCardHomePictureBox
            // 
            this.Player4TechnologyCardHomePictureBox.BackColor = System.Drawing.Color.Transparent;
            this.Player4TechnologyCardHomePictureBox.Image = global::ScrumGame.Properties.Resources.TechnologyCardBack;
            this.Player4TechnologyCardHomePictureBox.Location = new System.Drawing.Point(204, 50);
            this.Player4TechnologyCardHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player4TechnologyCardHomePictureBox.Name = "Player4TechnologyCardHomePictureBox";
            this.Player4TechnologyCardHomePictureBox.Size = new System.Drawing.Size(107, 148);
            this.Player4TechnologyCardHomePictureBox.TabIndex = 18;
            this.Player4TechnologyCardHomePictureBox.TabStop = false;
            // 
            // Player4ResearchLabel
            // 
            this.Player4ResearchLabel.AutoSize = true;
            this.Player4ResearchLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4ResearchLabel.Location = new System.Drawing.Point(13, 121);
            this.Player4ResearchLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4ResearchLabel.Name = "Player4ResearchLabel";
            this.Player4ResearchLabel.Size = new System.Drawing.Size(101, 25);
            this.Player4ResearchLabel.TabIndex = 30;
            this.Player4ResearchLabel.Text = "Research:";
            this.Player4ResearchLabel.UseMnemonic = false;
            // 
            // Player4Research3PictureBox
            // 
            this.Player4Research3PictureBox.Location = new System.Drawing.Point(140, 149);
            this.Player4Research3PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player4Research3PictureBox.Name = "Player4Research3PictureBox";
            this.Player4Research3PictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player4Research3PictureBox.TabIndex = 29;
            this.Player4Research3PictureBox.TabStop = false;
            this.Player4Research3PictureBox.Click += new System.EventHandler(this.Player4Research3PictureBox_Click);
            // 
            // Player4Research2PictureBox
            // 
            this.Player4Research2PictureBox.Location = new System.Drawing.Point(79, 149);
            this.Player4Research2PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player4Research2PictureBox.Name = "Player4Research2PictureBox";
            this.Player4Research2PictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player4Research2PictureBox.TabIndex = 28;
            this.Player4Research2PictureBox.TabStop = false;
            this.Player4Research2PictureBox.Click += new System.EventHandler(this.Player4Research2PictureBox_Click);
            // 
            // Player4Research1PictureBox
            // 
            this.Player4Research1PictureBox.Location = new System.Drawing.Point(17, 149);
            this.Player4Research1PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player4Research1PictureBox.Name = "Player4Research1PictureBox";
            this.Player4Research1PictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player4Research1PictureBox.TabIndex = 27;
            this.Player4Research1PictureBox.TabStop = false;
            this.Player4Research1PictureBox.Click += new System.EventHandler(this.Player4Research1PictureBox_Click);
            // 
            // Player4ScoreLabel
            // 
            this.Player4ScoreLabel.AutoSize = true;
            this.Player4ScoreLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4ScoreLabel.Location = new System.Drawing.Point(12, 71);
            this.Player4ScoreLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4ScoreLabel.Name = "Player4ScoreLabel";
            this.Player4ScoreLabel.Size = new System.Drawing.Size(75, 25);
            this.Player4ScoreLabel.TabIndex = 2;
            this.Player4ScoreLabel.Text = "Score: ";
            this.Player4ScoreLabel.UseMnemonic = false;
            // 
            // Player4MoneyLabel
            // 
            this.Player4MoneyLabel.AutoSize = true;
            this.Player4MoneyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4MoneyLabel.Location = new System.Drawing.Point(12, 47);
            this.Player4MoneyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player4MoneyLabel.Name = "Player4MoneyLabel";
            this.Player4MoneyLabel.Size = new System.Drawing.Size(78, 25);
            this.Player4MoneyLabel.TabIndex = 1;
            this.Player4MoneyLabel.Text = "Money:";
            this.Player4MoneyLabel.UseMnemonic = false;
            // 
            // Player4TitleTextBox
            // 
            this.Player4TitleTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player4TitleTextBox.Location = new System.Drawing.Point(17, 11);
            this.Player4TitleTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player4TitleTextBox.Name = "Player4TitleTextBox";
            this.Player4TitleTextBox.Size = new System.Drawing.Size(292, 30);
            this.Player4TitleTextBox.TabIndex = 0;
            this.Player4TitleTextBox.Text = "Player 4";
            this.Player4TitleTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Player4TitleTextBox.TextChanged += new System.EventHandler(this.Player4TitleTextBox_TextChanged);
            // 
            // Player3Panel
            // 
            this.Player3Panel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Player3Panel.BackColor = System.Drawing.Color.White;
            this.Player3Panel.Controls.Add(this.Player3BudgetAmountLabel);
            this.Player3Panel.Controls.Add(this.Player3ScoreAmountLabel);
            this.Player3Panel.Controls.Add(this.Player3BudgetLabel);
            this.Player3Panel.Controls.Add(this.Player3FullStackDevelopersLabel);
            this.Player3Panel.Controls.Add(this.Player3MoneyAmountLabel);
            this.Player3Panel.Controls.Add(this.Player3EpicAmountLabel);
            this.Player3Panel.Controls.Add(this.Player3BackEndDevelopersLabel);
            this.Player3Panel.Controls.Add(this.Player3FeatureAmountLabel);
            this.Player3Panel.Controls.Add(this.Player3FrontEndDevelopersLabel);
            this.Player3Panel.Controls.Add(this.Player3StoryAmountLabel);
            this.Player3Panel.Controls.Add(this.Player3ScrumMastersLabel);
            this.Player3Panel.Controls.Add(this.Player3TaskAmountLabel);
            this.Player3Panel.Controls.Add(this.Player3ProductOwnersLabel);
            this.Player3Panel.Controls.Add(this.Player3FullStackDeveloperHomePictureBox);
            this.Player3Panel.Controls.Add(this.Player3BackEndDeveloperHomePictureBox);
            this.Player3Panel.Controls.Add(this.Player3FrontEndDeveloperHomePictureBox);
            this.Player3Panel.Controls.Add(this.Player3ScrumMasterHomePictureBox);
            this.Player3Panel.Controls.Add(this.Player3ProductOwnerHomePictureBox);
            this.Player3Panel.Controls.Add(this.Player3EpicsLabel);
            this.Player3Panel.Controls.Add(this.Player3FeaturesLabel);
            this.Player3Panel.Controls.Add(this.Player3StoriesLabel);
            this.Player3Panel.Controls.Add(this.Player3TasksLabel);
            this.Player3Panel.Controls.Add(this.Player3ClientCardHomePictureBox);
            this.Player3Panel.Controls.Add(this.Player3TechnologyCardHomePictureBox);
            this.Player3Panel.Controls.Add(this.Player3ResearchLabel);
            this.Player3Panel.Controls.Add(this.Player3Research3PictureBox);
            this.Player3Panel.Controls.Add(this.Player3Research2PictureBox);
            this.Player3Panel.Controls.Add(this.Player3Research1PictureBox);
            this.Player3Panel.Controls.Add(this.Player3ScoreLabel);
            this.Player3Panel.Controls.Add(this.Player3MoneyLabel);
            this.Player3Panel.Controls.Add(this.Player3TitleTextBox);
            this.Player3Panel.Location = new System.Drawing.Point(1352, 438);
            this.Player3Panel.Margin = new System.Windows.Forms.Padding(4);
            this.Player3Panel.Name = "Player3Panel";
            this.Player3Panel.Size = new System.Drawing.Size(328, 426);
            this.Player3Panel.TabIndex = 45;
            // 
            // Player3BudgetAmountLabel
            // 
            this.Player3BudgetAmountLabel.AutoSize = true;
            this.Player3BudgetAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3BudgetAmountLabel.Location = new System.Drawing.Point(129, 96);
            this.Player3BudgetAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3BudgetAmountLabel.Name = "Player3BudgetAmountLabel";
            this.Player3BudgetAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player3BudgetAmountLabel.TabIndex = 64;
            this.Player3BudgetAmountLabel.Text = "0";
            this.Player3BudgetAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player3BudgetAmountLabel.UseMnemonic = false;
            // 
            // Player3ScoreAmountLabel
            // 
            this.Player3ScoreAmountLabel.AutoSize = true;
            this.Player3ScoreAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3ScoreAmountLabel.Location = new System.Drawing.Point(129, 73);
            this.Player3ScoreAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3ScoreAmountLabel.Name = "Player3ScoreAmountLabel";
            this.Player3ScoreAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player3ScoreAmountLabel.TabIndex = 68;
            this.Player3ScoreAmountLabel.Text = "0";
            this.Player3ScoreAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player3ScoreAmountLabel.UseMnemonic = false;
            // 
            // Player3BudgetLabel
            // 
            this.Player3BudgetLabel.AutoSize = true;
            this.Player3BudgetLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3BudgetLabel.Location = new System.Drawing.Point(13, 96);
            this.Player3BudgetLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3BudgetLabel.Name = "Player3BudgetLabel";
            this.Player3BudgetLabel.Size = new System.Drawing.Size(80, 25);
            this.Player3BudgetLabel.TabIndex = 63;
            this.Player3BudgetLabel.Text = "Budget:";
            this.Player3BudgetLabel.UseMnemonic = false;
            // 
            // Player3FullStackDevelopersLabel
            // 
            this.Player3FullStackDevelopersLabel.AutoSize = true;
            this.Player3FullStackDevelopersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3FullStackDevelopersLabel.Location = new System.Drawing.Point(276, 327);
            this.Player3FullStackDevelopersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3FullStackDevelopersLabel.Name = "Player3FullStackDevelopersLabel";
            this.Player3FullStackDevelopersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player3FullStackDevelopersLabel.TabIndex = 44;
            this.Player3FullStackDevelopersLabel.Text = "0";
            this.Player3FullStackDevelopersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player3FullStackDevelopersLabel.UseMnemonic = false;
            // 
            // Player3MoneyAmountLabel
            // 
            this.Player3MoneyAmountLabel.AutoSize = true;
            this.Player3MoneyAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3MoneyAmountLabel.Location = new System.Drawing.Point(129, 48);
            this.Player3MoneyAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3MoneyAmountLabel.Name = "Player3MoneyAmountLabel";
            this.Player3MoneyAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player3MoneyAmountLabel.TabIndex = 67;
            this.Player3MoneyAmountLabel.Text = "0";
            this.Player3MoneyAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player3MoneyAmountLabel.UseMnemonic = false;
            // 
            // Player3EpicAmountLabel
            // 
            this.Player3EpicAmountLabel.AutoSize = true;
            this.Player3EpicAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3EpicAmountLabel.Location = new System.Drawing.Point(132, 279);
            this.Player3EpicAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3EpicAmountLabel.Name = "Player3EpicAmountLabel";
            this.Player3EpicAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player3EpicAmountLabel.TabIndex = 66;
            this.Player3EpicAmountLabel.Text = "0";
            this.Player3EpicAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player3EpicAmountLabel.UseMnemonic = false;
            // 
            // Player3BackEndDevelopersLabel
            // 
            this.Player3BackEndDevelopersLabel.AutoSize = true;
            this.Player3BackEndDevelopersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3BackEndDevelopersLabel.Location = new System.Drawing.Point(215, 327);
            this.Player3BackEndDevelopersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3BackEndDevelopersLabel.Name = "Player3BackEndDevelopersLabel";
            this.Player3BackEndDevelopersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player3BackEndDevelopersLabel.TabIndex = 43;
            this.Player3BackEndDevelopersLabel.Text = "0";
            this.Player3BackEndDevelopersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player3BackEndDevelopersLabel.UseMnemonic = false;
            // 
            // Player3FeatureAmountLabel
            // 
            this.Player3FeatureAmountLabel.AutoSize = true;
            this.Player3FeatureAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3FeatureAmountLabel.Location = new System.Drawing.Point(132, 255);
            this.Player3FeatureAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3FeatureAmountLabel.Name = "Player3FeatureAmountLabel";
            this.Player3FeatureAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player3FeatureAmountLabel.TabIndex = 65;
            this.Player3FeatureAmountLabel.Text = "0";
            this.Player3FeatureAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player3FeatureAmountLabel.UseMnemonic = false;
            // 
            // Player3FrontEndDevelopersLabel
            // 
            this.Player3FrontEndDevelopersLabel.AutoSize = true;
            this.Player3FrontEndDevelopersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3FrontEndDevelopersLabel.Location = new System.Drawing.Point(153, 327);
            this.Player3FrontEndDevelopersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3FrontEndDevelopersLabel.Name = "Player3FrontEndDevelopersLabel";
            this.Player3FrontEndDevelopersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player3FrontEndDevelopersLabel.TabIndex = 42;
            this.Player3FrontEndDevelopersLabel.Text = "0";
            this.Player3FrontEndDevelopersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player3FrontEndDevelopersLabel.UseMnemonic = false;
            // 
            // Player3StoryAmountLabel
            // 
            this.Player3StoryAmountLabel.AutoSize = true;
            this.Player3StoryAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3StoryAmountLabel.Location = new System.Drawing.Point(132, 230);
            this.Player3StoryAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3StoryAmountLabel.Name = "Player3StoryAmountLabel";
            this.Player3StoryAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player3StoryAmountLabel.TabIndex = 64;
            this.Player3StoryAmountLabel.Text = "0";
            this.Player3StoryAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player3StoryAmountLabel.UseMnemonic = false;
            // 
            // Player3ScrumMastersLabel
            // 
            this.Player3ScrumMastersLabel.AutoSize = true;
            this.Player3ScrumMastersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3ScrumMastersLabel.Location = new System.Drawing.Point(92, 327);
            this.Player3ScrumMastersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3ScrumMastersLabel.Name = "Player3ScrumMastersLabel";
            this.Player3ScrumMastersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player3ScrumMastersLabel.TabIndex = 41;
            this.Player3ScrumMastersLabel.Text = "0";
            this.Player3ScrumMastersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player3ScrumMastersLabel.UseMnemonic = false;
            // 
            // Player3TaskAmountLabel
            // 
            this.Player3TaskAmountLabel.AutoSize = true;
            this.Player3TaskAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3TaskAmountLabel.Location = new System.Drawing.Point(132, 206);
            this.Player3TaskAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3TaskAmountLabel.Name = "Player3TaskAmountLabel";
            this.Player3TaskAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player3TaskAmountLabel.TabIndex = 63;
            this.Player3TaskAmountLabel.Text = "0";
            this.Player3TaskAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player3TaskAmountLabel.UseMnemonic = false;
            // 
            // Player3ProductOwnersLabel
            // 
            this.Player3ProductOwnersLabel.AutoSize = true;
            this.Player3ProductOwnersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3ProductOwnersLabel.Location = new System.Drawing.Point(31, 327);
            this.Player3ProductOwnersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3ProductOwnersLabel.Name = "Player3ProductOwnersLabel";
            this.Player3ProductOwnersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player3ProductOwnersLabel.TabIndex = 40;
            this.Player3ProductOwnersLabel.Text = "0";
            this.Player3ProductOwnersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player3ProductOwnersLabel.UseMnemonic = false;
            // 
            // Player3FullStackDeveloperHomePictureBox
            // 
            this.Player3FullStackDeveloperHomePictureBox.Location = new System.Drawing.Point(260, 356);
            this.Player3FullStackDeveloperHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player3FullStackDeveloperHomePictureBox.Name = "Player3FullStackDeveloperHomePictureBox";
            this.Player3FullStackDeveloperHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player3FullStackDeveloperHomePictureBox.TabIndex = 39;
            this.Player3FullStackDeveloperHomePictureBox.TabStop = false;
            // 
            // Player3BackEndDeveloperHomePictureBox
            // 
            this.Player3BackEndDeveloperHomePictureBox.Location = new System.Drawing.Point(199, 356);
            this.Player3BackEndDeveloperHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player3BackEndDeveloperHomePictureBox.Name = "Player3BackEndDeveloperHomePictureBox";
            this.Player3BackEndDeveloperHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player3BackEndDeveloperHomePictureBox.TabIndex = 38;
            this.Player3BackEndDeveloperHomePictureBox.TabStop = false;
            // 
            // Player3FrontEndDeveloperHomePictureBox
            // 
            this.Player3FrontEndDeveloperHomePictureBox.Location = new System.Drawing.Point(137, 356);
            this.Player3FrontEndDeveloperHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player3FrontEndDeveloperHomePictureBox.Name = "Player3FrontEndDeveloperHomePictureBox";
            this.Player3FrontEndDeveloperHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player3FrontEndDeveloperHomePictureBox.TabIndex = 37;
            this.Player3FrontEndDeveloperHomePictureBox.TabStop = false;
            // 
            // Player3ScrumMasterHomePictureBox
            // 
            this.Player3ScrumMasterHomePictureBox.Location = new System.Drawing.Point(76, 356);
            this.Player3ScrumMasterHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player3ScrumMasterHomePictureBox.Name = "Player3ScrumMasterHomePictureBox";
            this.Player3ScrumMasterHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player3ScrumMasterHomePictureBox.TabIndex = 36;
            this.Player3ScrumMasterHomePictureBox.TabStop = false;
            // 
            // Player3ProductOwnerHomePictureBox
            // 
            this.Player3ProductOwnerHomePictureBox.Location = new System.Drawing.Point(15, 356);
            this.Player3ProductOwnerHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player3ProductOwnerHomePictureBox.Name = "Player3ProductOwnerHomePictureBox";
            this.Player3ProductOwnerHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player3ProductOwnerHomePictureBox.TabIndex = 35;
            this.Player3ProductOwnerHomePictureBox.TabStop = false;
            // 
            // Player3EpicsLabel
            // 
            this.Player3EpicsLabel.AutoSize = true;
            this.Player3EpicsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3EpicsLabel.Location = new System.Drawing.Point(13, 279);
            this.Player3EpicsLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3EpicsLabel.Name = "Player3EpicsLabel";
            this.Player3EpicsLabel.Size = new System.Drawing.Size(66, 25);
            this.Player3EpicsLabel.TabIndex = 34;
            this.Player3EpicsLabel.Text = "Epics:";
            this.Player3EpicsLabel.UseMnemonic = false;
            // 
            // Player3FeaturesLabel
            // 
            this.Player3FeaturesLabel.AutoSize = true;
            this.Player3FeaturesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3FeaturesLabel.Location = new System.Drawing.Point(13, 255);
            this.Player3FeaturesLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3FeaturesLabel.Name = "Player3FeaturesLabel";
            this.Player3FeaturesLabel.Size = new System.Drawing.Size(95, 25);
            this.Player3FeaturesLabel.TabIndex = 33;
            this.Player3FeaturesLabel.Text = "Features:";
            this.Player3FeaturesLabel.UseMnemonic = false;
            // 
            // Player3StoriesLabel
            // 
            this.Player3StoriesLabel.AutoSize = true;
            this.Player3StoriesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3StoriesLabel.Location = new System.Drawing.Point(13, 230);
            this.Player3StoriesLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3StoriesLabel.Name = "Player3StoriesLabel";
            this.Player3StoriesLabel.Size = new System.Drawing.Size(79, 25);
            this.Player3StoriesLabel.TabIndex = 32;
            this.Player3StoriesLabel.Text = "Stories:";
            this.Player3StoriesLabel.UseMnemonic = false;
            // 
            // Player3TasksLabel
            // 
            this.Player3TasksLabel.AutoSize = true;
            this.Player3TasksLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3TasksLabel.Location = new System.Drawing.Point(13, 206);
            this.Player3TasksLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3TasksLabel.Name = "Player3TasksLabel";
            this.Player3TasksLabel.Size = new System.Drawing.Size(72, 25);
            this.Player3TasksLabel.TabIndex = 31;
            this.Player3TasksLabel.Text = "Tasks:";
            this.Player3TasksLabel.UseMnemonic = false;
            // 
            // Player3ClientCardHomePictureBox
            // 
            this.Player3ClientCardHomePictureBox.BackColor = System.Drawing.Color.Transparent;
            this.Player3ClientCardHomePictureBox.Location = new System.Drawing.Point(217, 206);
            this.Player3ClientCardHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player3ClientCardHomePictureBox.Name = "Player3ClientCardHomePictureBox";
            this.Player3ClientCardHomePictureBox.Size = new System.Drawing.Size(80, 111);
            this.Player3ClientCardHomePictureBox.TabIndex = 18;
            this.Player3ClientCardHomePictureBox.TabStop = false;
            // 
            // Player3TechnologyCardHomePictureBox
            // 
            this.Player3TechnologyCardHomePictureBox.BackColor = System.Drawing.Color.Transparent;
            this.Player3TechnologyCardHomePictureBox.Image = global::ScrumGame.Properties.Resources.TechnologyCardBack;
            this.Player3TechnologyCardHomePictureBox.Location = new System.Drawing.Point(204, 50);
            this.Player3TechnologyCardHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player3TechnologyCardHomePictureBox.Name = "Player3TechnologyCardHomePictureBox";
            this.Player3TechnologyCardHomePictureBox.Size = new System.Drawing.Size(107, 148);
            this.Player3TechnologyCardHomePictureBox.TabIndex = 18;
            this.Player3TechnologyCardHomePictureBox.TabStop = false;
            // 
            // Player3ResearchLabel
            // 
            this.Player3ResearchLabel.AutoSize = true;
            this.Player3ResearchLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3ResearchLabel.Location = new System.Drawing.Point(13, 121);
            this.Player3ResearchLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3ResearchLabel.Name = "Player3ResearchLabel";
            this.Player3ResearchLabel.Size = new System.Drawing.Size(101, 25);
            this.Player3ResearchLabel.TabIndex = 30;
            this.Player3ResearchLabel.Text = "Research:";
            this.Player3ResearchLabel.UseMnemonic = false;
            // 
            // Player3Research3PictureBox
            // 
            this.Player3Research3PictureBox.Location = new System.Drawing.Point(140, 149);
            this.Player3Research3PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player3Research3PictureBox.Name = "Player3Research3PictureBox";
            this.Player3Research3PictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player3Research3PictureBox.TabIndex = 29;
            this.Player3Research3PictureBox.TabStop = false;
            this.Player3Research3PictureBox.Click += new System.EventHandler(this.Player3Research3PictureBox_Click);
            // 
            // Player3Research2PictureBox
            // 
            this.Player3Research2PictureBox.Location = new System.Drawing.Point(79, 149);
            this.Player3Research2PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player3Research2PictureBox.Name = "Player3Research2PictureBox";
            this.Player3Research2PictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player3Research2PictureBox.TabIndex = 28;
            this.Player3Research2PictureBox.TabStop = false;
            this.Player3Research2PictureBox.Click += new System.EventHandler(this.Player3Research2PictureBox_Click);
            // 
            // Player3Research1PictureBox
            // 
            this.Player3Research1PictureBox.Location = new System.Drawing.Point(17, 149);
            this.Player3Research1PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player3Research1PictureBox.Name = "Player3Research1PictureBox";
            this.Player3Research1PictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player3Research1PictureBox.TabIndex = 27;
            this.Player3Research1PictureBox.TabStop = false;
            this.Player3Research1PictureBox.Click += new System.EventHandler(this.Player3Research1PictureBox_Click);
            // 
            // Player3ScoreLabel
            // 
            this.Player3ScoreLabel.AutoSize = true;
            this.Player3ScoreLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3ScoreLabel.Location = new System.Drawing.Point(12, 71);
            this.Player3ScoreLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3ScoreLabel.Name = "Player3ScoreLabel";
            this.Player3ScoreLabel.Size = new System.Drawing.Size(75, 25);
            this.Player3ScoreLabel.TabIndex = 2;
            this.Player3ScoreLabel.Text = "Score: ";
            this.Player3ScoreLabel.UseMnemonic = false;
            // 
            // Player3MoneyLabel
            // 
            this.Player3MoneyLabel.AutoSize = true;
            this.Player3MoneyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3MoneyLabel.Location = new System.Drawing.Point(12, 47);
            this.Player3MoneyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player3MoneyLabel.Name = "Player3MoneyLabel";
            this.Player3MoneyLabel.Size = new System.Drawing.Size(78, 25);
            this.Player3MoneyLabel.TabIndex = 1;
            this.Player3MoneyLabel.Text = "Money:";
            this.Player3MoneyLabel.UseMnemonic = false;
            // 
            // Player3TitleTextBox
            // 
            this.Player3TitleTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player3TitleTextBox.Location = new System.Drawing.Point(17, 11);
            this.Player3TitleTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player3TitleTextBox.Name = "Player3TitleTextBox";
            this.Player3TitleTextBox.Size = new System.Drawing.Size(292, 30);
            this.Player3TitleTextBox.TabIndex = 0;
            this.Player3TitleTextBox.Text = "Player 3";
            this.Player3TitleTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Player3TitleTextBox.TextChanged += new System.EventHandler(this.Player3TitleTextBox_TextChanged);
            // 
            // Player2Panel
            // 
            this.Player2Panel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Player2Panel.BackColor = System.Drawing.Color.White;
            this.Player2Panel.Controls.Add(this.Player2BudgetAmountLabel);
            this.Player2Panel.Controls.Add(this.Player2ScoreAmountLabel);
            this.Player2Panel.Controls.Add(this.Player2BudgetLabel);
            this.Player2Panel.Controls.Add(this.Player2FullStackDevelopersLabel);
            this.Player2Panel.Controls.Add(this.Player2MoneyAmountLabel);
            this.Player2Panel.Controls.Add(this.Player2EpicAmountLabel);
            this.Player2Panel.Controls.Add(this.Player2BackEndDevelopersLabel);
            this.Player2Panel.Controls.Add(this.Player2FeatureAmountLabel);
            this.Player2Panel.Controls.Add(this.Player2FrontEndDevelopersLabel);
            this.Player2Panel.Controls.Add(this.Player2StoryAmountLabel);
            this.Player2Panel.Controls.Add(this.Player2ScrumMastersLabel);
            this.Player2Panel.Controls.Add(this.Player2TaskAmountLabel);
            this.Player2Panel.Controls.Add(this.Player2ProductOwnersLabel);
            this.Player2Panel.Controls.Add(this.Player2FullStackDeveloperHomePictureBox);
            this.Player2Panel.Controls.Add(this.Player2BackEndDeveloperHomePictureBox);
            this.Player2Panel.Controls.Add(this.Player2FrontEndDeveloperHomePictureBox);
            this.Player2Panel.Controls.Add(this.Player2ScrumMasterHomePictureBox);
            this.Player2Panel.Controls.Add(this.Player2ProductOwnerHomePictureBox);
            this.Player2Panel.Controls.Add(this.Player2EpicsLabel);
            this.Player2Panel.Controls.Add(this.Player2FeaturesLabel);
            this.Player2Panel.Controls.Add(this.Player2StoriesLabel);
            this.Player2Panel.Controls.Add(this.Player2TasksLabel);
            this.Player2Panel.Controls.Add(this.Player2ClientCardHomePictureBox);
            this.Player2Panel.Controls.Add(this.Player2TechnologyCardHomePictureBox);
            this.Player2Panel.Controls.Add(this.Player2ResearchLabel);
            this.Player2Panel.Controls.Add(this.Player2Research3PictureBox);
            this.Player2Panel.Controls.Add(this.Player2Research2PictureBox);
            this.Player2Panel.Controls.Add(this.Player2Research1PictureBox);
            this.Player2Panel.Controls.Add(this.Player2ScoreLabel);
            this.Player2Panel.Controls.Add(this.Player2MoneyLabel);
            this.Player2Panel.Controls.Add(this.Player2TitleTextBox);
            this.Player2Panel.Location = new System.Drawing.Point(1352, 4);
            this.Player2Panel.Margin = new System.Windows.Forms.Padding(4);
            this.Player2Panel.Name = "Player2Panel";
            this.Player2Panel.Size = new System.Drawing.Size(328, 426);
            this.Player2Panel.TabIndex = 9;
            // 
            // Player2BudgetAmountLabel
            // 
            this.Player2BudgetAmountLabel.AutoSize = true;
            this.Player2BudgetAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2BudgetAmountLabel.Location = new System.Drawing.Point(129, 96);
            this.Player2BudgetAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2BudgetAmountLabel.Name = "Player2BudgetAmountLabel";
            this.Player2BudgetAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player2BudgetAmountLabel.TabIndex = 58;
            this.Player2BudgetAmountLabel.Text = "0";
            this.Player2BudgetAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player2BudgetAmountLabel.UseMnemonic = false;
            // 
            // Player2ScoreAmountLabel
            // 
            this.Player2ScoreAmountLabel.AutoSize = true;
            this.Player2ScoreAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2ScoreAmountLabel.Location = new System.Drawing.Point(129, 71);
            this.Player2ScoreAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2ScoreAmountLabel.Name = "Player2ScoreAmountLabel";
            this.Player2ScoreAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player2ScoreAmountLabel.TabIndex = 62;
            this.Player2ScoreAmountLabel.Text = "0";
            this.Player2ScoreAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player2ScoreAmountLabel.UseMnemonic = false;
            // 
            // Player2BudgetLabel
            // 
            this.Player2BudgetLabel.AutoSize = true;
            this.Player2BudgetLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2BudgetLabel.Location = new System.Drawing.Point(13, 96);
            this.Player2BudgetLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2BudgetLabel.Name = "Player2BudgetLabel";
            this.Player2BudgetLabel.Size = new System.Drawing.Size(80, 25);
            this.Player2BudgetLabel.TabIndex = 57;
            this.Player2BudgetLabel.Text = "Budget:";
            this.Player2BudgetLabel.UseMnemonic = false;
            // 
            // Player2FullStackDevelopersLabel
            // 
            this.Player2FullStackDevelopersLabel.AutoSize = true;
            this.Player2FullStackDevelopersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2FullStackDevelopersLabel.Location = new System.Drawing.Point(276, 327);
            this.Player2FullStackDevelopersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2FullStackDevelopersLabel.Name = "Player2FullStackDevelopersLabel";
            this.Player2FullStackDevelopersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player2FullStackDevelopersLabel.TabIndex = 44;
            this.Player2FullStackDevelopersLabel.Text = "0";
            this.Player2FullStackDevelopersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player2FullStackDevelopersLabel.UseMnemonic = false;
            // 
            // Player2MoneyAmountLabel
            // 
            this.Player2MoneyAmountLabel.AutoSize = true;
            this.Player2MoneyAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2MoneyAmountLabel.Location = new System.Drawing.Point(129, 47);
            this.Player2MoneyAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2MoneyAmountLabel.Name = "Player2MoneyAmountLabel";
            this.Player2MoneyAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player2MoneyAmountLabel.TabIndex = 61;
            this.Player2MoneyAmountLabel.Text = "0";
            this.Player2MoneyAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player2MoneyAmountLabel.UseMnemonic = false;
            // 
            // Player2EpicAmountLabel
            // 
            this.Player2EpicAmountLabel.AutoSize = true;
            this.Player2EpicAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2EpicAmountLabel.Location = new System.Drawing.Point(132, 279);
            this.Player2EpicAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2EpicAmountLabel.Name = "Player2EpicAmountLabel";
            this.Player2EpicAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player2EpicAmountLabel.TabIndex = 60;
            this.Player2EpicAmountLabel.Text = "0";
            this.Player2EpicAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player2EpicAmountLabel.UseMnemonic = false;
            // 
            // Player2BackEndDevelopersLabel
            // 
            this.Player2BackEndDevelopersLabel.AutoSize = true;
            this.Player2BackEndDevelopersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2BackEndDevelopersLabel.Location = new System.Drawing.Point(215, 327);
            this.Player2BackEndDevelopersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2BackEndDevelopersLabel.Name = "Player2BackEndDevelopersLabel";
            this.Player2BackEndDevelopersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player2BackEndDevelopersLabel.TabIndex = 43;
            this.Player2BackEndDevelopersLabel.Text = "0";
            this.Player2BackEndDevelopersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player2BackEndDevelopersLabel.UseMnemonic = false;
            // 
            // Player2FeatureAmountLabel
            // 
            this.Player2FeatureAmountLabel.AutoSize = true;
            this.Player2FeatureAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2FeatureAmountLabel.Location = new System.Drawing.Point(132, 255);
            this.Player2FeatureAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2FeatureAmountLabel.Name = "Player2FeatureAmountLabel";
            this.Player2FeatureAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player2FeatureAmountLabel.TabIndex = 59;
            this.Player2FeatureAmountLabel.Text = "0";
            this.Player2FeatureAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player2FeatureAmountLabel.UseMnemonic = false;
            // 
            // Player2FrontEndDevelopersLabel
            // 
            this.Player2FrontEndDevelopersLabel.AutoSize = true;
            this.Player2FrontEndDevelopersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2FrontEndDevelopersLabel.Location = new System.Drawing.Point(153, 327);
            this.Player2FrontEndDevelopersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2FrontEndDevelopersLabel.Name = "Player2FrontEndDevelopersLabel";
            this.Player2FrontEndDevelopersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player2FrontEndDevelopersLabel.TabIndex = 42;
            this.Player2FrontEndDevelopersLabel.Text = "0";
            this.Player2FrontEndDevelopersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player2FrontEndDevelopersLabel.UseMnemonic = false;
            // 
            // Player2StoryAmountLabel
            // 
            this.Player2StoryAmountLabel.AutoSize = true;
            this.Player2StoryAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2StoryAmountLabel.Location = new System.Drawing.Point(132, 230);
            this.Player2StoryAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2StoryAmountLabel.Name = "Player2StoryAmountLabel";
            this.Player2StoryAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player2StoryAmountLabel.TabIndex = 58;
            this.Player2StoryAmountLabel.Text = "0";
            this.Player2StoryAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player2StoryAmountLabel.UseMnemonic = false;
            // 
            // Player2ScrumMastersLabel
            // 
            this.Player2ScrumMastersLabel.AutoSize = true;
            this.Player2ScrumMastersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2ScrumMastersLabel.Location = new System.Drawing.Point(92, 327);
            this.Player2ScrumMastersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2ScrumMastersLabel.Name = "Player2ScrumMastersLabel";
            this.Player2ScrumMastersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player2ScrumMastersLabel.TabIndex = 41;
            this.Player2ScrumMastersLabel.Text = "0";
            this.Player2ScrumMastersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player2ScrumMastersLabel.UseMnemonic = false;
            // 
            // Player2TaskAmountLabel
            // 
            this.Player2TaskAmountLabel.AutoSize = true;
            this.Player2TaskAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2TaskAmountLabel.Location = new System.Drawing.Point(132, 206);
            this.Player2TaskAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2TaskAmountLabel.Name = "Player2TaskAmountLabel";
            this.Player2TaskAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player2TaskAmountLabel.TabIndex = 57;
            this.Player2TaskAmountLabel.Text = "0";
            this.Player2TaskAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player2TaskAmountLabel.UseMnemonic = false;
            // 
            // Player2ProductOwnersLabel
            // 
            this.Player2ProductOwnersLabel.AutoSize = true;
            this.Player2ProductOwnersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2ProductOwnersLabel.Location = new System.Drawing.Point(31, 327);
            this.Player2ProductOwnersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2ProductOwnersLabel.Name = "Player2ProductOwnersLabel";
            this.Player2ProductOwnersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player2ProductOwnersLabel.TabIndex = 40;
            this.Player2ProductOwnersLabel.Text = "0";
            this.Player2ProductOwnersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player2ProductOwnersLabel.UseMnemonic = false;
            // 
            // Player2FullStackDeveloperHomePictureBox
            // 
            this.Player2FullStackDeveloperHomePictureBox.Location = new System.Drawing.Point(260, 356);
            this.Player2FullStackDeveloperHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player2FullStackDeveloperHomePictureBox.Name = "Player2FullStackDeveloperHomePictureBox";
            this.Player2FullStackDeveloperHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player2FullStackDeveloperHomePictureBox.TabIndex = 39;
            this.Player2FullStackDeveloperHomePictureBox.TabStop = false;
            // 
            // Player2BackEndDeveloperHomePictureBox
            // 
            this.Player2BackEndDeveloperHomePictureBox.Location = new System.Drawing.Point(199, 356);
            this.Player2BackEndDeveloperHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player2BackEndDeveloperHomePictureBox.Name = "Player2BackEndDeveloperHomePictureBox";
            this.Player2BackEndDeveloperHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player2BackEndDeveloperHomePictureBox.TabIndex = 38;
            this.Player2BackEndDeveloperHomePictureBox.TabStop = false;
            // 
            // Player2FrontEndDeveloperHomePictureBox
            // 
            this.Player2FrontEndDeveloperHomePictureBox.Location = new System.Drawing.Point(137, 356);
            this.Player2FrontEndDeveloperHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player2FrontEndDeveloperHomePictureBox.Name = "Player2FrontEndDeveloperHomePictureBox";
            this.Player2FrontEndDeveloperHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player2FrontEndDeveloperHomePictureBox.TabIndex = 37;
            this.Player2FrontEndDeveloperHomePictureBox.TabStop = false;
            // 
            // Player2ScrumMasterHomePictureBox
            // 
            this.Player2ScrumMasterHomePictureBox.Location = new System.Drawing.Point(76, 356);
            this.Player2ScrumMasterHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player2ScrumMasterHomePictureBox.Name = "Player2ScrumMasterHomePictureBox";
            this.Player2ScrumMasterHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player2ScrumMasterHomePictureBox.TabIndex = 36;
            this.Player2ScrumMasterHomePictureBox.TabStop = false;
            // 
            // Player2ProductOwnerHomePictureBox
            // 
            this.Player2ProductOwnerHomePictureBox.Location = new System.Drawing.Point(15, 356);
            this.Player2ProductOwnerHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player2ProductOwnerHomePictureBox.Name = "Player2ProductOwnerHomePictureBox";
            this.Player2ProductOwnerHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player2ProductOwnerHomePictureBox.TabIndex = 35;
            this.Player2ProductOwnerHomePictureBox.TabStop = false;
            // 
            // Player2EpicsLabel
            // 
            this.Player2EpicsLabel.AutoSize = true;
            this.Player2EpicsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2EpicsLabel.Location = new System.Drawing.Point(13, 279);
            this.Player2EpicsLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2EpicsLabel.Name = "Player2EpicsLabel";
            this.Player2EpicsLabel.Size = new System.Drawing.Size(66, 25);
            this.Player2EpicsLabel.TabIndex = 34;
            this.Player2EpicsLabel.Text = "Epics:";
            this.Player2EpicsLabel.UseMnemonic = false;
            // 
            // Player2FeaturesLabel
            // 
            this.Player2FeaturesLabel.AutoSize = true;
            this.Player2FeaturesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2FeaturesLabel.Location = new System.Drawing.Point(13, 255);
            this.Player2FeaturesLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2FeaturesLabel.Name = "Player2FeaturesLabel";
            this.Player2FeaturesLabel.Size = new System.Drawing.Size(95, 25);
            this.Player2FeaturesLabel.TabIndex = 33;
            this.Player2FeaturesLabel.Text = "Features:";
            this.Player2FeaturesLabel.UseMnemonic = false;
            // 
            // Player2StoriesLabel
            // 
            this.Player2StoriesLabel.AutoSize = true;
            this.Player2StoriesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2StoriesLabel.Location = new System.Drawing.Point(13, 230);
            this.Player2StoriesLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2StoriesLabel.Name = "Player2StoriesLabel";
            this.Player2StoriesLabel.Size = new System.Drawing.Size(79, 25);
            this.Player2StoriesLabel.TabIndex = 32;
            this.Player2StoriesLabel.Text = "Stories:";
            this.Player2StoriesLabel.UseMnemonic = false;
            // 
            // Player2TasksLabel
            // 
            this.Player2TasksLabel.AutoSize = true;
            this.Player2TasksLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2TasksLabel.Location = new System.Drawing.Point(13, 206);
            this.Player2TasksLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2TasksLabel.Name = "Player2TasksLabel";
            this.Player2TasksLabel.Size = new System.Drawing.Size(72, 25);
            this.Player2TasksLabel.TabIndex = 31;
            this.Player2TasksLabel.Text = "Tasks:";
            this.Player2TasksLabel.UseMnemonic = false;
            // 
            // Player2ClientCardHomePictureBox
            // 
            this.Player2ClientCardHomePictureBox.BackColor = System.Drawing.Color.Transparent;
            this.Player2ClientCardHomePictureBox.Location = new System.Drawing.Point(217, 206);
            this.Player2ClientCardHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player2ClientCardHomePictureBox.Name = "Player2ClientCardHomePictureBox";
            this.Player2ClientCardHomePictureBox.Size = new System.Drawing.Size(80, 111);
            this.Player2ClientCardHomePictureBox.TabIndex = 18;
            this.Player2ClientCardHomePictureBox.TabStop = false;
            // 
            // Player2TechnologyCardHomePictureBox
            // 
            this.Player2TechnologyCardHomePictureBox.BackColor = System.Drawing.Color.Transparent;
            this.Player2TechnologyCardHomePictureBox.Image = global::ScrumGame.Properties.Resources.TechnologyCardBack;
            this.Player2TechnologyCardHomePictureBox.Location = new System.Drawing.Point(204, 50);
            this.Player2TechnologyCardHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player2TechnologyCardHomePictureBox.Name = "Player2TechnologyCardHomePictureBox";
            this.Player2TechnologyCardHomePictureBox.Size = new System.Drawing.Size(107, 148);
            this.Player2TechnologyCardHomePictureBox.TabIndex = 18;
            this.Player2TechnologyCardHomePictureBox.TabStop = false;
            // 
            // Player2ResearchLabel
            // 
            this.Player2ResearchLabel.AutoSize = true;
            this.Player2ResearchLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2ResearchLabel.Location = new System.Drawing.Point(13, 121);
            this.Player2ResearchLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2ResearchLabel.Name = "Player2ResearchLabel";
            this.Player2ResearchLabel.Size = new System.Drawing.Size(101, 25);
            this.Player2ResearchLabel.TabIndex = 30;
            this.Player2ResearchLabel.Text = "Research:";
            this.Player2ResearchLabel.UseMnemonic = false;
            // 
            // Player2Research3PictureBox
            // 
            this.Player2Research3PictureBox.Location = new System.Drawing.Point(140, 149);
            this.Player2Research3PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player2Research3PictureBox.Name = "Player2Research3PictureBox";
            this.Player2Research3PictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player2Research3PictureBox.TabIndex = 29;
            this.Player2Research3PictureBox.TabStop = false;
            this.Player2Research3PictureBox.Click += new System.EventHandler(this.Player2Research3PictureBox_Click);
            // 
            // Player2Research2PictureBox
            // 
            this.Player2Research2PictureBox.Location = new System.Drawing.Point(79, 149);
            this.Player2Research2PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player2Research2PictureBox.Name = "Player2Research2PictureBox";
            this.Player2Research2PictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player2Research2PictureBox.TabIndex = 28;
            this.Player2Research2PictureBox.TabStop = false;
            this.Player2Research2PictureBox.Click += new System.EventHandler(this.Player2Research2PictureBox_Click);
            // 
            // Player2Research1PictureBox
            // 
            this.Player2Research1PictureBox.Location = new System.Drawing.Point(17, 149);
            this.Player2Research1PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player2Research1PictureBox.Name = "Player2Research1PictureBox";
            this.Player2Research1PictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player2Research1PictureBox.TabIndex = 27;
            this.Player2Research1PictureBox.TabStop = false;
            this.Player2Research1PictureBox.Click += new System.EventHandler(this.Player2Research1PictureBox_Click);
            // 
            // Player2ScoreLabel
            // 
            this.Player2ScoreLabel.AutoSize = true;
            this.Player2ScoreLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2ScoreLabel.Location = new System.Drawing.Point(12, 71);
            this.Player2ScoreLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2ScoreLabel.Name = "Player2ScoreLabel";
            this.Player2ScoreLabel.Size = new System.Drawing.Size(75, 25);
            this.Player2ScoreLabel.TabIndex = 2;
            this.Player2ScoreLabel.Text = "Score: ";
            this.Player2ScoreLabel.UseMnemonic = false;
            // 
            // Player2MoneyLabel
            // 
            this.Player2MoneyLabel.AutoSize = true;
            this.Player2MoneyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2MoneyLabel.Location = new System.Drawing.Point(12, 47);
            this.Player2MoneyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player2MoneyLabel.Name = "Player2MoneyLabel";
            this.Player2MoneyLabel.Size = new System.Drawing.Size(78, 25);
            this.Player2MoneyLabel.TabIndex = 1;
            this.Player2MoneyLabel.Text = "Money:";
            this.Player2MoneyLabel.UseMnemonic = false;
            // 
            // Player2TitleTextBox
            // 
            this.Player2TitleTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player2TitleTextBox.Location = new System.Drawing.Point(17, 11);
            this.Player2TitleTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player2TitleTextBox.Name = "Player2TitleTextBox";
            this.Player2TitleTextBox.Size = new System.Drawing.Size(292, 30);
            this.Player2TitleTextBox.TabIndex = 0;
            this.Player2TitleTextBox.Text = "Player 2";
            this.Player2TitleTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Player2TitleTextBox.TextChanged += new System.EventHandler(this.Player2TitleTextBox_TextChanged);
            // 
            // CenterPanel
            // 
            this.CenterPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CenterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.CenterPanel.Controls.Add(this.button2);
            this.CenterPanel.Controls.Add(this.ScrumMasterSupplyLabel);
            this.CenterPanel.Controls.Add(this.ProductOwnerSupplyLabel);
            this.CenterPanel.Controls.Add(this.ScrumMasterSupplyPictureBox);
            this.CenterPanel.Controls.Add(this.ProductOwnerSupplyPictureBox);
            this.CenterPanel.Controls.Add(this.TextLabel);
            this.CenterPanel.Controls.Add(this.DiceAmount);
            this.CenterPanel.Controls.Add(this.diceImage6);
            this.CenterPanel.Controls.Add(this.diceImage5);
            this.CenterPanel.Controls.Add(this.diceImage4);
            this.CenterPanel.Controls.Add(this.diceImage3);
            this.CenterPanel.Controls.Add(this.diceImage2);
            this.CenterPanel.Controls.Add(this.diceImage1);
            this.CenterPanel.Controls.Add(this.RollDiceBtn);
            this.CenterPanel.Controls.Add(this.FullStackDeveloperSupplyLabel);
            this.CenterPanel.Controls.Add(this.Dice7PictureBox);
            this.CenterPanel.Controls.Add(this.BackEndDeveloperSupplyLabel);
            this.CenterPanel.Controls.Add(this.Dice6PictureBox);
            this.CenterPanel.Controls.Add(this.FrontEndDeveloperSupplyLabel);
            this.CenterPanel.Controls.Add(this.Dice5PictureBox);
            this.CenterPanel.Controls.Add(this.FullStackDeveloperSupplyPictureBox);
            this.CenterPanel.Controls.Add(this.Dice4PictureBox);
            this.CenterPanel.Controls.Add(this.BackEndDeveloperSupplyPictureBox);
            this.CenterPanel.Controls.Add(this.FrontEndDeveloperSupplyPictureBox);
            this.CenterPanel.Controls.Add(this.Dice3PictureBox);
            this.CenterPanel.Controls.Add(this.Dice2PictureBox);
            this.CenterPanel.Controls.Add(this.Dice1PictureBox);
            this.CenterPanel.Controls.Add(this.WorkerSupplyPictureBox);
            this.CenterPanel.Controls.Add(this.GameBoardPanel);
            this.CenterPanel.Controls.Add(this.TechnologyCardSupplyPictureBox);
            this.CenterPanel.Location = new System.Drawing.Point(342, 10);
            this.CenterPanel.Margin = new System.Windows.Forms.Padding(4);
            this.CenterPanel.Name = "CenterPanel";
            this.MainTableLayoutPanel.SetRowSpan(this.CenterPanel, 2);
            this.CenterPanel.Size = new System.Drawing.Size(1000, 849);
            this.CenterPanel.TabIndex = 6;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(649, 788);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(123, 26);
            this.button2.TabIndex = 7;
            this.button2.Text = "Task List";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ScrumMasterSupplyLabel
            // 
            this.ScrumMasterSupplyLabel.AutoSize = true;
            this.ScrumMasterSupplyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScrumMasterSupplyLabel.Location = new System.Drawing.Point(183, 758);
            this.ScrumMasterSupplyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ScrumMasterSupplyLabel.Name = "ScrumMasterSupplyLabel";
            this.ScrumMasterSupplyLabel.Size = new System.Drawing.Size(34, 25);
            this.ScrumMasterSupplyLabel.TabIndex = 63;
            this.ScrumMasterSupplyLabel.Text = "00";
            this.ScrumMasterSupplyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ScrumMasterSupplyLabel.UseMnemonic = false;
            // 
            // ProductOwnerSupplyLabel
            // 
            this.ProductOwnerSupplyLabel.AutoSize = true;
            this.ProductOwnerSupplyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductOwnerSupplyLabel.Location = new System.Drawing.Point(121, 758);
            this.ProductOwnerSupplyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ProductOwnerSupplyLabel.Name = "ProductOwnerSupplyLabel";
            this.ProductOwnerSupplyLabel.Size = new System.Drawing.Size(34, 25);
            this.ProductOwnerSupplyLabel.TabIndex = 62;
            this.ProductOwnerSupplyLabel.Text = "00";
            this.ProductOwnerSupplyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ProductOwnerSupplyLabel.UseMnemonic = false;
            // 
            // ScrumMasterSupplyPictureBox
            // 
            this.ScrumMasterSupplyPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.ScrumMasterSupplyPictureBox.Location = new System.Drawing.Point(173, 786);
            this.ScrumMasterSupplyPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.ScrumMasterSupplyPictureBox.Name = "ScrumMasterSupplyPictureBox";
            this.ScrumMasterSupplyPictureBox.Size = new System.Drawing.Size(53, 49);
            this.ScrumMasterSupplyPictureBox.TabIndex = 61;
            this.ScrumMasterSupplyPictureBox.TabStop = false;
            // 
            // ProductOwnerSupplyPictureBox
            // 
            this.ProductOwnerSupplyPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.ProductOwnerSupplyPictureBox.Location = new System.Drawing.Point(112, 786);
            this.ProductOwnerSupplyPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.ProductOwnerSupplyPictureBox.Name = "ProductOwnerSupplyPictureBox";
            this.ProductOwnerSupplyPictureBox.Size = new System.Drawing.Size(53, 49);
            this.ProductOwnerSupplyPictureBox.TabIndex = 60;
            this.ProductOwnerSupplyPictureBox.TabStop = false;
            // 
            // TextLabel
            // 
            this.TextLabel.AutoSize = true;
            this.TextLabel.Location = new System.Drawing.Point(340, 818);
            this.TextLabel.Name = "TextLabel";
            this.TextLabel.Size = new System.Drawing.Size(158, 17);
            this.TextLabel.TabIndex = 59;
            this.TextLabel.Text = "Number Of Dice To Roll";
            this.TextLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // DiceAmount
            // 
            this.DiceAmount.Location = new System.Drawing.Point(384, 788);
            this.DiceAmount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DiceAmount.Name = "DiceAmount";
            this.DiceAmount.Size = new System.Drawing.Size(100, 22);
            this.DiceAmount.TabIndex = 58;
            this.DiceAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // diceImage6
            // 
            this.diceImage6.Image = ((System.Drawing.Image)(resources.GetObject("diceImage6.Image")));
            this.diceImage6.Location = new System.Drawing.Point(709, 674);
            this.diceImage6.Margin = new System.Windows.Forms.Padding(4);
            this.diceImage6.Name = "diceImage6";
            this.diceImage6.Size = new System.Drawing.Size(53, 49);
            this.diceImage6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.diceImage6.TabIndex = 57;
            this.diceImage6.TabStop = false;
            this.diceImage6.Visible = false;
            // 
            // diceImage5
            // 
            this.diceImage5.Image = ((System.Drawing.Image)(resources.GetObject("diceImage5.Image")));
            this.diceImage5.Location = new System.Drawing.Point(649, 674);
            this.diceImage5.Margin = new System.Windows.Forms.Padding(4);
            this.diceImage5.Name = "diceImage5";
            this.diceImage5.Size = new System.Drawing.Size(53, 49);
            this.diceImage5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.diceImage5.TabIndex = 56;
            this.diceImage5.TabStop = false;
            this.diceImage5.Visible = false;
            // 
            // diceImage4
            // 
            this.diceImage4.Image = ((System.Drawing.Image)(resources.GetObject("diceImage4.Image")));
            this.diceImage4.Location = new System.Drawing.Point(588, 674);
            this.diceImage4.Margin = new System.Windows.Forms.Padding(4);
            this.diceImage4.Name = "diceImage4";
            this.diceImage4.Size = new System.Drawing.Size(53, 49);
            this.diceImage4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.diceImage4.TabIndex = 55;
            this.diceImage4.TabStop = false;
            this.diceImage4.Visible = false;
            // 
            // diceImage3
            // 
            this.diceImage3.Image = ((System.Drawing.Image)(resources.GetObject("diceImage3.Image")));
            this.diceImage3.Location = new System.Drawing.Point(527, 674);
            this.diceImage3.Margin = new System.Windows.Forms.Padding(4);
            this.diceImage3.Name = "diceImage3";
            this.diceImage3.Size = new System.Drawing.Size(53, 49);
            this.diceImage3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.diceImage3.TabIndex = 54;
            this.diceImage3.TabStop = false;
            this.diceImage3.Visible = false;
            // 
            // diceImage2
            // 
            this.diceImage2.Image = ((System.Drawing.Image)(resources.GetObject("diceImage2.Image")));
            this.diceImage2.Location = new System.Drawing.Point(467, 674);
            this.diceImage2.Margin = new System.Windows.Forms.Padding(4);
            this.diceImage2.Name = "diceImage2";
            this.diceImage2.Size = new System.Drawing.Size(53, 49);
            this.diceImage2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.diceImage2.TabIndex = 53;
            this.diceImage2.TabStop = false;
            this.diceImage2.Visible = false;
            // 
            // diceImage1
            // 
            this.diceImage1.Image = ((System.Drawing.Image)(resources.GetObject("diceImage1.Image")));
            this.diceImage1.Location = new System.Drawing.Point(405, 674);
            this.diceImage1.Margin = new System.Windows.Forms.Padding(4);
            this.diceImage1.Name = "diceImage1";
            this.diceImage1.Size = new System.Drawing.Size(53, 49);
            this.diceImage1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.diceImage1.TabIndex = 52;
            this.diceImage1.TabStop = false;
            this.diceImage1.Visible = false;
            // 

            // RollDiceBtn
            // 
            this.RollDiceBtn.Location = new System.Drawing.Point(517, 788);
            this.RollDiceBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.RollDiceBtn.Name = "RollDiceBtn";
            this.RollDiceBtn.Size = new System.Drawing.Size(123, 26);
            this.RollDiceBtn.TabIndex = 51;
            this.RollDiceBtn.Text = "Roll Dice";
            this.RollDiceBtn.UseVisualStyleBackColor = true;
            this.RollDiceBtn.Click += new System.EventHandler(this.cmdRoll_Click);

            // button1
            // 
            this.button1.Location = new System.Drawing.Point(517, 788);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 26);
            this.button1.TabIndex = 51;
            this.button1.Text = "Roll Dice";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.cmdRoll_Click);

            // 
            // FullStackDeveloperSupplyLabel
            // 
            this.FullStackDeveloperSupplyLabel.AutoSize = true;
            this.FullStackDeveloperSupplyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullStackDeveloperSupplyLabel.Location = new System.Drawing.Point(213, 681);
            this.FullStackDeveloperSupplyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.FullStackDeveloperSupplyLabel.Name = "FullStackDeveloperSupplyLabel";
            this.FullStackDeveloperSupplyLabel.Size = new System.Drawing.Size(34, 25);
            this.FullStackDeveloperSupplyLabel.TabIndex = 50;
            this.FullStackDeveloperSupplyLabel.Text = "00";
            this.FullStackDeveloperSupplyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.FullStackDeveloperSupplyLabel.UseMnemonic = false;
            // 
            // Dice7PictureBox
            // 
            this.Dice7PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Dice7PictureBox.Location = new System.Drawing.Point(737, 732);
            this.Dice7PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Dice7PictureBox.Name = "Dice7PictureBox";
            this.Dice7PictureBox.Size = new System.Drawing.Size(53, 50);
            this.Dice7PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Dice7PictureBox.TabIndex = 26;
            this.Dice7PictureBox.TabStop = false;
            // 
            // BackEndDeveloperSupplyLabel
            // 
            this.BackEndDeveloperSupplyLabel.AutoSize = true;
            this.BackEndDeveloperSupplyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackEndDeveloperSupplyLabel.Location = new System.Drawing.Point(152, 681);
            this.BackEndDeveloperSupplyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.BackEndDeveloperSupplyLabel.Name = "BackEndDeveloperSupplyLabel";
            this.BackEndDeveloperSupplyLabel.Size = new System.Drawing.Size(34, 25);
            this.BackEndDeveloperSupplyLabel.TabIndex = 49;
            this.BackEndDeveloperSupplyLabel.Text = "00";
            this.BackEndDeveloperSupplyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.BackEndDeveloperSupplyLabel.UseMnemonic = false;
            // 
            // Dice6PictureBox
            // 
            this.Dice6PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Dice6PictureBox.Location = new System.Drawing.Point(676, 732);
            this.Dice6PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Dice6PictureBox.Name = "Dice6PictureBox";
            this.Dice6PictureBox.Size = new System.Drawing.Size(53, 50);
            this.Dice6PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Dice6PictureBox.TabIndex = 25;
            this.Dice6PictureBox.TabStop = false;
            // 
            // FrontEndDeveloperSupplyLabel
            // 
            this.FrontEndDeveloperSupplyLabel.AutoSize = true;
            this.FrontEndDeveloperSupplyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FrontEndDeveloperSupplyLabel.Location = new System.Drawing.Point(91, 681);
            this.FrontEndDeveloperSupplyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.FrontEndDeveloperSupplyLabel.Name = "FrontEndDeveloperSupplyLabel";
            this.FrontEndDeveloperSupplyLabel.Size = new System.Drawing.Size(34, 25);
            this.FrontEndDeveloperSupplyLabel.TabIndex = 48;
            this.FrontEndDeveloperSupplyLabel.Text = "00";
            this.FrontEndDeveloperSupplyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.FrontEndDeveloperSupplyLabel.UseMnemonic = false;
            // 
            // Dice5PictureBox
            // 
            this.Dice5PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Dice5PictureBox.Location = new System.Drawing.Point(615, 732);
            this.Dice5PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Dice5PictureBox.Name = "Dice5PictureBox";
            this.Dice5PictureBox.Size = new System.Drawing.Size(53, 50);
            this.Dice5PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Dice5PictureBox.TabIndex = 24;
            this.Dice5PictureBox.TabStop = false;
            // 
            // FullStackDeveloperSupplyPictureBox
            // 
            this.FullStackDeveloperSupplyPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.FullStackDeveloperSupplyPictureBox.Location = new System.Drawing.Point(204, 709);
            this.FullStackDeveloperSupplyPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.FullStackDeveloperSupplyPictureBox.Name = "FullStackDeveloperSupplyPictureBox";
            this.FullStackDeveloperSupplyPictureBox.Size = new System.Drawing.Size(53, 49);
            this.FullStackDeveloperSupplyPictureBox.TabIndex = 47;
            this.FullStackDeveloperSupplyPictureBox.TabStop = false;
            // 
            // Dice4PictureBox
            // 
            this.Dice4PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Dice4PictureBox.Location = new System.Drawing.Point(553, 732);
            this.Dice4PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Dice4PictureBox.Name = "Dice4PictureBox";
            this.Dice4PictureBox.Size = new System.Drawing.Size(53, 50);
            this.Dice4PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Dice4PictureBox.TabIndex = 23;
            this.Dice4PictureBox.TabStop = false;
            // 
            // BackEndDeveloperSupplyPictureBox
            // 
            this.BackEndDeveloperSupplyPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.BackEndDeveloperSupplyPictureBox.Location = new System.Drawing.Point(143, 709);
            this.BackEndDeveloperSupplyPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.BackEndDeveloperSupplyPictureBox.Name = "BackEndDeveloperSupplyPictureBox";
            this.BackEndDeveloperSupplyPictureBox.Size = new System.Drawing.Size(53, 49);
            this.BackEndDeveloperSupplyPictureBox.TabIndex = 46;
            this.BackEndDeveloperSupplyPictureBox.TabStop = false;
            // 
            // FrontEndDeveloperSupplyPictureBox
            // 
            this.FrontEndDeveloperSupplyPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.FrontEndDeveloperSupplyPictureBox.Location = new System.Drawing.Point(81, 709);
            this.FrontEndDeveloperSupplyPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.FrontEndDeveloperSupplyPictureBox.Name = "FrontEndDeveloperSupplyPictureBox";
            this.FrontEndDeveloperSupplyPictureBox.Size = new System.Drawing.Size(53, 49);
            this.FrontEndDeveloperSupplyPictureBox.TabIndex = 45;
            this.FrontEndDeveloperSupplyPictureBox.TabStop = false;
            // 
            // Dice3PictureBox
            // 
            this.Dice3PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Dice3PictureBox.Location = new System.Drawing.Point(492, 732);
            this.Dice3PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Dice3PictureBox.Name = "Dice3PictureBox";
            this.Dice3PictureBox.Size = new System.Drawing.Size(53, 50);
            this.Dice3PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Dice3PictureBox.TabIndex = 22;
            this.Dice3PictureBox.TabStop = false;
            // 
            // Dice2PictureBox
            // 
            this.Dice2PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Dice2PictureBox.Location = new System.Drawing.Point(431, 732);
            this.Dice2PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Dice2PictureBox.Name = "Dice2PictureBox";
            this.Dice2PictureBox.Size = new System.Drawing.Size(53, 50);
            this.Dice2PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Dice2PictureBox.TabIndex = 21;
            this.Dice2PictureBox.TabStop = false;
            // 
            // Dice1PictureBox
            // 
            this.Dice1PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Dice1PictureBox.Location = new System.Drawing.Point(369, 732);
            this.Dice1PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Dice1PictureBox.Name = "Dice1PictureBox";
            this.Dice1PictureBox.Size = new System.Drawing.Size(53, 50);
            this.Dice1PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Dice1PictureBox.TabIndex = 20;
            this.Dice1PictureBox.TabStop = false;
            // 
            // WorkerSupplyPictureBox
            // 
            this.WorkerSupplyPictureBox.Location = new System.Drawing.Point(45, 674);
            this.WorkerSupplyPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.WorkerSupplyPictureBox.Name = "WorkerSupplyPictureBox";
            this.WorkerSupplyPictureBox.Size = new System.Drawing.Size(237, 170);
            this.WorkerSupplyPictureBox.TabIndex = 19;
            this.WorkerSupplyPictureBox.TabStop = false;
            // 
            // GameBoardPanel
            // 
            this.GameBoardPanel.BackgroundImage = global::ScrumGame.Properties.Resources.GameBoard;
            this.GameBoardPanel.Controls.Add(this.Player4BudgetPictureBox);
            this.GameBoardPanel.Controls.Add(this.Player3BudgetPictureBox);
            this.GameBoardPanel.Controls.Add(this.Player2BudgetPictureBox);
            this.GameBoardPanel.Controls.Add(this.Player1BudgetPictureBox);
            this.GameBoardPanel.Controls.Add(this.TechnologyCardSlotsPictureBox);
            this.GameBoardPanel.Controls.Add(this.EpicSupplyLabel);
            this.GameBoardPanel.Controls.Add(this.FeatureSupplyLabel);
            this.GameBoardPanel.Controls.Add(this.ContractOutPanel);
            this.GameBoardPanel.Controls.Add(this.BudgetStripPictureBox);
            this.GameBoardPanel.Controls.Add(this.TechnologyCard4PictureBox);
            this.GameBoardPanel.Controls.Add(this.BigWigPanel);
            this.GameBoardPanel.Controls.Add(this.TechnologyCard3PictureBox);
            this.GameBoardPanel.Controls.Add(this.JobFairPanel);
            this.GameBoardPanel.Controls.Add(this.TechnologyCard2PictureBox);
            this.GameBoardPanel.Controls.Add(this.GooglePanel);
            this.GameBoardPanel.Controls.Add(this.TechnologyCard1PictureBox);
            this.GameBoardPanel.Controls.Add(this.Client4PictureBox);
            this.GameBoardPanel.Controls.Add(this.TasklandPanel);
            this.GameBoardPanel.Controls.Add(this.Client3PictureBox);
            this.GameBoardPanel.Controls.Add(this.StorylandPanel);
            this.GameBoardPanel.Controls.Add(this.Client2PictureBox);
            this.GameBoardPanel.Controls.Add(this.EpiclandPanel);
            this.GameBoardPanel.Controls.Add(this.Client1PictureBox);
            this.GameBoardPanel.Controls.Add(this.FeaturelandPanel);
            this.GameBoardPanel.Location = new System.Drawing.Point(7, 6);
            this.GameBoardPanel.Margin = new System.Windows.Forms.Padding(4);
            this.GameBoardPanel.Name = "GameBoardPanel";
            this.GameBoardPanel.Size = new System.Drawing.Size(987, 665);
            this.GameBoardPanel.TabIndex = 18;
            // 
            // Player4BudgetPictureBox
            // 
            this.Player4BudgetPictureBox.BackColor = System.Drawing.Color.Blue;
            this.Player4BudgetPictureBox.Location = new System.Drawing.Point(73, 617);
            this.Player4BudgetPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player4BudgetPictureBox.Name = "Player4BudgetPictureBox";
            this.Player4BudgetPictureBox.Size = new System.Drawing.Size(7, 25);
            this.Player4BudgetPictureBox.TabIndex = 23;
            this.Player4BudgetPictureBox.TabStop = false;
            // 
            // Player3BudgetPictureBox
            // 
            this.Player3BudgetPictureBox.BackColor = System.Drawing.Color.Yellow;
            this.Player3BudgetPictureBox.Location = new System.Drawing.Point(63, 617);
            this.Player3BudgetPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player3BudgetPictureBox.Name = "Player3BudgetPictureBox";
            this.Player3BudgetPictureBox.Size = new System.Drawing.Size(7, 25);
            this.Player3BudgetPictureBox.TabIndex = 22;
            this.Player3BudgetPictureBox.TabStop = false;
            // 
            // Player2BudgetPictureBox
            // 
            this.Player2BudgetPictureBox.BackColor = System.Drawing.Color.Turquoise;
            this.Player2BudgetPictureBox.Location = new System.Drawing.Point(43, 617);
            this.Player2BudgetPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player2BudgetPictureBox.Name = "Player2BudgetPictureBox";
            this.Player2BudgetPictureBox.Size = new System.Drawing.Size(7, 25);
            this.Player2BudgetPictureBox.TabIndex = 21;
            this.Player2BudgetPictureBox.TabStop = false;
            // 
            // Player1BudgetPictureBox
            // 
            this.Player1BudgetPictureBox.BackColor = System.Drawing.Color.Fuchsia;
            this.Player1BudgetPictureBox.Location = new System.Drawing.Point(32, 617);
            this.Player1BudgetPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player1BudgetPictureBox.Name = "Player1BudgetPictureBox";
            this.Player1BudgetPictureBox.Size = new System.Drawing.Size(7, 25);
            this.Player1BudgetPictureBox.TabIndex = 20;
            this.Player1BudgetPictureBox.TabStop = false;
            // 
            // TechnologyCardSlotsPictureBox
            // 
            this.TechnologyCardSlotsPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.TechnologyCardSlotsPictureBox.BackgroundImage = global::ScrumGame.Properties.Resources.TechnologyCardSlots;
            this.TechnologyCardSlotsPictureBox.Location = new System.Drawing.Point(499, 486);
            this.TechnologyCardSlotsPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.TechnologyCardSlotsPictureBox.Name = "TechnologyCardSlotsPictureBox";
            this.TechnologyCardSlotsPictureBox.Size = new System.Drawing.Size(456, 20);
            this.TechnologyCardSlotsPictureBox.TabIndex = 19;
            this.TechnologyCardSlotsPictureBox.TabStop = false;
            // 
            // EpicSupplyLabel
            // 
            this.EpicSupplyLabel.AutoSize = true;
            this.EpicSupplyLabel.BackColor = System.Drawing.Color.Transparent;
            this.EpicSupplyLabel.ForeColor = System.Drawing.Color.White;
            this.EpicSupplyLabel.Location = new System.Drawing.Point(743, 334);
            this.EpicSupplyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.EpicSupplyLabel.Name = "EpicSupplyLabel";
            this.EpicSupplyLabel.Size = new System.Drawing.Size(16, 17);
            this.EpicSupplyLabel.TabIndex = 18;
            this.EpicSupplyLabel.Text = "0";
            this.EpicSupplyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FeatureSupplyLabel
            // 
            this.FeatureSupplyLabel.AutoSize = true;
            this.FeatureSupplyLabel.BackColor = System.Drawing.Color.Transparent;
            this.FeatureSupplyLabel.ForeColor = System.Drawing.Color.White;
            this.FeatureSupplyLabel.Location = new System.Drawing.Point(756, 142);
            this.FeatureSupplyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.FeatureSupplyLabel.Name = "FeatureSupplyLabel";
            this.FeatureSupplyLabel.Size = new System.Drawing.Size(16, 17);
            this.FeatureSupplyLabel.TabIndex = 2;
            this.FeatureSupplyLabel.Text = "0";
            this.FeatureSupplyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ContractOutPanel
            // 
            this.ContractOutPanel.BackColor = System.Drawing.Color.Transparent;
            this.ContractOutPanel.Location = new System.Drawing.Point(100, 73);
            this.ContractOutPanel.Margin = new System.Windows.Forms.Padding(4);
            this.ContractOutPanel.Name = "ContractOutPanel";
            this.ContractOutPanel.Size = new System.Drawing.Size(200, 185);
            this.ContractOutPanel.TabIndex = 15;
            this.ContractOutPanel.MouseHover += new System.EventHandler(this.ContractOutPanel_MouseHover);
            // 
            // BudgetStripPictureBox
            // 
            this.BudgetStripPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.BudgetStripPictureBox.Location = new System.Drawing.Point(29, 101);
            this.BudgetStripPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.BudgetStripPictureBox.Name = "BudgetStripPictureBox";
            this.BudgetStripPictureBox.Size = new System.Drawing.Size(53, 542);
            this.BudgetStripPictureBox.TabIndex = 17;
            this.BudgetStripPictureBox.TabStop = false;
            this.BudgetStripPictureBox.MouseHover += new System.EventHandler(this.BudgetStripPictureBox_MouseHover);
            // 
            // TechnologyCard4PictureBox
            // 
            this.TechnologyCard4PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.TechnologyCard4PictureBox.Location = new System.Drawing.Point(845, 495);
            this.TechnologyCard4PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.TechnologyCard4PictureBox.Name = "TechnologyCard4PictureBox";
            this.TechnologyCard4PictureBox.Size = new System.Drawing.Size(107, 148);
            this.TechnologyCard4PictureBox.TabIndex = 0;
            this.TechnologyCard4PictureBox.TabStop = false;
            this.TechnologyCard4PictureBox.Click += new System.EventHandler(this.TechnologyCard4PictureBox_Click);
            // 
            // BigWigPanel
            // 
            this.BigWigPanel.BackColor = System.Drawing.Color.Transparent;
            this.BigWigPanel.Location = new System.Drawing.Point(175, 308);
            this.BigWigPanel.Margin = new System.Windows.Forms.Padding(4);
            this.BigWigPanel.Name = "BigWigPanel";
            this.BigWigPanel.Size = new System.Drawing.Size(133, 123);
            this.BigWigPanel.TabIndex = 16;
            this.BigWigPanel.MouseHover += new System.EventHandler(this.BigWigPanel_MouseHover);
            // 
            // TechnologyCard3PictureBox
            // 
            this.TechnologyCard3PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.TechnologyCard3PictureBox.Location = new System.Drawing.Point(731, 495);
            this.TechnologyCard3PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.TechnologyCard3PictureBox.Name = "TechnologyCard3PictureBox";
            this.TechnologyCard3PictureBox.Size = new System.Drawing.Size(107, 148);
            this.TechnologyCard3PictureBox.TabIndex = 1;
            this.TechnologyCard3PictureBox.TabStop = false;
            this.TechnologyCard3PictureBox.Click += new System.EventHandler(this.TechnologyCard3PictureBox_Click);
            // 
            // JobFairPanel
            // 
            this.JobFairPanel.BackColor = System.Drawing.Color.Transparent;
            this.JobFairPanel.Location = new System.Drawing.Point(316, 345);
            this.JobFairPanel.Margin = new System.Windows.Forms.Padding(4);
            this.JobFairPanel.Name = "JobFairPanel";
            this.JobFairPanel.Size = new System.Drawing.Size(163, 154);
            this.JobFairPanel.TabIndex = 16;
            this.JobFairPanel.MouseHover += new System.EventHandler(this.JobFairPanel_MouseHover);
            // 
            // TechnologyCard2PictureBox
            // 
            this.TechnologyCard2PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.TechnologyCard2PictureBox.Location = new System.Drawing.Point(616, 495);
            this.TechnologyCard2PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.TechnologyCard2PictureBox.Name = "TechnologyCard2PictureBox";
            this.TechnologyCard2PictureBox.Size = new System.Drawing.Size(107, 148);
            this.TechnologyCard2PictureBox.TabIndex = 2;
            this.TechnologyCard2PictureBox.TabStop = false;
            this.TechnologyCard2PictureBox.Click += new System.EventHandler(this.TechnologyCard2PictureBox_Click);
            // 
            // GooglePanel
            // 
            this.GooglePanel.BackColor = System.Drawing.Color.Transparent;
            this.GooglePanel.Location = new System.Drawing.Point(501, 308);
            this.GooglePanel.Margin = new System.Windows.Forms.Padding(4);
            this.GooglePanel.Name = "GooglePanel";
            this.GooglePanel.Size = new System.Drawing.Size(133, 123);
            this.GooglePanel.TabIndex = 15;
            this.GooglePanel.MouseHover += new System.EventHandler(this.GooglePanel_MouseHover);
            // 
            // TechnologyCard1PictureBox
            // 
            this.TechnologyCard1PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.TechnologyCard1PictureBox.Location = new System.Drawing.Point(501, 495);
            this.TechnologyCard1PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.TechnologyCard1PictureBox.Name = "TechnologyCard1PictureBox";
            this.TechnologyCard1PictureBox.Size = new System.Drawing.Size(107, 148);
            this.TechnologyCard1PictureBox.TabIndex = 3;
            this.TechnologyCard1PictureBox.TabStop = false;
            this.TechnologyCard1PictureBox.Click += new System.EventHandler(this.TechnologyCard1PictureBox_Click);
            // 
            // Client4PictureBox
            // 
            this.Client4PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.Client4PictureBox.Location = new System.Drawing.Point(388, 532);
            this.Client4PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Client4PictureBox.Name = "Client4PictureBox";
            this.Client4PictureBox.Size = new System.Drawing.Size(80, 111);
            this.Client4PictureBox.TabIndex = 5;
            this.Client4PictureBox.TabStop = false;
            this.Client4PictureBox.MouseHover += new System.EventHandler(this.Client4PictureBox_MouseHover);
            // 
            // TasklandPanel
            // 
            this.TasklandPanel.BackColor = System.Drawing.Color.Transparent;
            this.TasklandPanel.Controls.Add(this.TaskSupplyLabel);
            this.TasklandPanel.Location = new System.Drawing.Point(336, 73);
            this.TasklandPanel.Margin = new System.Windows.Forms.Padding(4);
            this.TasklandPanel.Name = "TasklandPanel";
            this.TasklandPanel.Size = new System.Drawing.Size(200, 185);
            this.TasklandPanel.TabIndex = 15;
            this.TasklandPanel.MouseHover += new System.EventHandler(this.TasklandPanel_MouseHover);
            // 
            // TaskSupplyLabel
            // 
            this.TaskSupplyLabel.AutoSize = true;
            this.TaskSupplyLabel.ForeColor = System.Drawing.Color.White;
            this.TaskSupplyLabel.Location = new System.Drawing.Point(24, 69);
            this.TaskSupplyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TaskSupplyLabel.Name = "TaskSupplyLabel";
            this.TaskSupplyLabel.Size = new System.Drawing.Size(16, 17);
            this.TaskSupplyLabel.TabIndex = 0;
            this.TaskSupplyLabel.Text = "0";
            this.TaskSupplyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Client3PictureBox
            // 
            this.Client3PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.Client3PictureBox.Location = new System.Drawing.Point(300, 532);
            this.Client3PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Client3PictureBox.Name = "Client3PictureBox";
            this.Client3PictureBox.Size = new System.Drawing.Size(80, 111);
            this.Client3PictureBox.TabIndex = 6;
            this.Client3PictureBox.TabStop = false;
            this.Client3PictureBox.MouseHover += new System.EventHandler(this.Client3PictureBox_MouseHover);
            // 
            // StorylandPanel
            // 
            this.StorylandPanel.BackColor = System.Drawing.Color.Transparent;
            this.StorylandPanel.Controls.Add(this.StorySupplyLabel);
            this.StorylandPanel.Location = new System.Drawing.Point(544, 73);
            this.StorylandPanel.Margin = new System.Windows.Forms.Padding(4);
            this.StorylandPanel.Name = "StorylandPanel";
            this.StorylandPanel.Size = new System.Drawing.Size(200, 185);
            this.StorylandPanel.TabIndex = 15;
            this.StorylandPanel.MouseHover += new System.EventHandler(this.StorylandPanel_MouseHover);
            // 
            // StorySupplyLabel
            // 
            this.StorySupplyLabel.AutoSize = true;
            this.StorySupplyLabel.ForeColor = System.Drawing.Color.White;
            this.StorySupplyLabel.Location = new System.Drawing.Point(4, 69);
            this.StorySupplyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.StorySupplyLabel.Name = "StorySupplyLabel";
            this.StorySupplyLabel.Size = new System.Drawing.Size(16, 17);
            this.StorySupplyLabel.TabIndex = 1;
            this.StorySupplyLabel.Text = "0";
            this.StorySupplyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Client2PictureBox
            // 
            this.Client2PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.Client2PictureBox.Location = new System.Drawing.Point(212, 532);
            this.Client2PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Client2PictureBox.Name = "Client2PictureBox";
            this.Client2PictureBox.Size = new System.Drawing.Size(80, 111);
            this.Client2PictureBox.TabIndex = 7;
            this.Client2PictureBox.TabStop = false;
            this.Client2PictureBox.MouseHover += new System.EventHandler(this.Client2PictureBox_MouseHover);
            // 
            // EpiclandPanel
            // 
            this.EpiclandPanel.BackColor = System.Drawing.Color.Transparent;
            this.EpiclandPanel.Location = new System.Drawing.Point(765, 265);
            this.EpiclandPanel.Margin = new System.Windows.Forms.Padding(4);
            this.EpiclandPanel.Name = "EpiclandPanel";
            this.EpiclandPanel.Size = new System.Drawing.Size(200, 185);
            this.EpiclandPanel.TabIndex = 15;
            this.EpiclandPanel.MouseHover += new System.EventHandler(this.EpiclandPanel_MouseHover);
            // 
            // Client1PictureBox
            // 
            this.Client1PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.Client1PictureBox.Location = new System.Drawing.Point(124, 532);
            this.Client1PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Client1PictureBox.Name = "Client1PictureBox";
            this.Client1PictureBox.Size = new System.Drawing.Size(80, 111);
            this.Client1PictureBox.TabIndex = 8;
            this.Client1PictureBox.TabStop = false;
            this.Client1PictureBox.MouseHover += new System.EventHandler(this.Client1PictureBox_MouseHover);
            // 
            // FeaturelandPanel
            // 
            this.FeaturelandPanel.BackColor = System.Drawing.Color.Transparent;
            this.FeaturelandPanel.Location = new System.Drawing.Point(765, 73);
            this.FeaturelandPanel.Margin = new System.Windows.Forms.Padding(4);
            this.FeaturelandPanel.Name = "FeaturelandPanel";
            this.FeaturelandPanel.Size = new System.Drawing.Size(200, 185);
            this.FeaturelandPanel.TabIndex = 14;
            this.FeaturelandPanel.MouseHover += new System.EventHandler(this.FeaturelandPanel_MouseHover);
            // 
            // TechnologyCardSupplyPictureBox
            // 
            this.TechnologyCardSupplyPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.TechnologyCardSupplyPictureBox.Image = global::ScrumGame.Properties.Resources.TechnologyCardBack;
            this.TechnologyCardSupplyPictureBox.Location = new System.Drawing.Point(848, 682);
            this.TechnologyCardSupplyPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.TechnologyCardSupplyPictureBox.Name = "TechnologyCardSupplyPictureBox";
            this.TechnologyCardSupplyPictureBox.Size = new System.Drawing.Size(107, 148);
            this.TechnologyCardSupplyPictureBox.TabIndex = 4;
            this.TechnologyCardSupplyPictureBox.TabStop = false;
            // 
            // Player1Panel
            // 
            this.Player1Panel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Player1Panel.BackColor = System.Drawing.Color.White;
            this.Player1Panel.Controls.Add(this.Player1BudgetAmountLabel);
            this.Player1Panel.Controls.Add(this.Player1BudgetLabel);
            this.Player1Panel.Controls.Add(this.Player1ScoreAmountLabel);
            this.Player1Panel.Controls.Add(this.Player1MoneyAmountLabel);
            this.Player1Panel.Controls.Add(this.Player1EpicAmountLabel);
            this.Player1Panel.Controls.Add(this.Player1FeatureAmountLabel);
            this.Player1Panel.Controls.Add(this.Player1StoryAmountLabel);
            this.Player1Panel.Controls.Add(this.Player1TaskAmountLabel);
            this.Player1Panel.Controls.Add(this.Player1FullStackDevelopersLabel);
            this.Player1Panel.Controls.Add(this.Player1BackEndDevelopersLabel);
            this.Player1Panel.Controls.Add(this.Player1FrontEndDevelopersLabel);
            this.Player1Panel.Controls.Add(this.Player1ScrumMastersLabel);
            this.Player1Panel.Controls.Add(this.Player1ProductOwnersLabel);
            this.Player1Panel.Controls.Add(this.Player1FullStackDeveloperHomePictureBox);
            this.Player1Panel.Controls.Add(this.Player1BackEndDeveloperHomePictureBox);
            this.Player1Panel.Controls.Add(this.Player1FrontEndDeveloperHomePictureBox);
            this.Player1Panel.Controls.Add(this.Player1ScrumMasterHomePictureBox);
            this.Player1Panel.Controls.Add(this.Player1ProductOwnerHomePictureBox);
            this.Player1Panel.Controls.Add(this.Player1EpicsLabel);
            this.Player1Panel.Controls.Add(this.Player1FeaturesLabel);
            this.Player1Panel.Controls.Add(this.Player1StoriesLabel);
            this.Player1Panel.Controls.Add(this.Player1TasksLabel);
            this.Player1Panel.Controls.Add(this.Player1ClientCardHomePictureBox);
            this.Player1Panel.Controls.Add(this.Player1TechnologyCardHomePictureBox);
            this.Player1Panel.Controls.Add(this.Player1ResearchLabel);
            this.Player1Panel.Controls.Add(this.Player1Research3PictureBox);
            this.Player1Panel.Controls.Add(this.Player1Research2PictureBox);
            this.Player1Panel.Controls.Add(this.Player1Research1PictureBox);
            this.Player1Panel.Controls.Add(this.Player1ScoreLabel);
            this.Player1Panel.Controls.Add(this.Player1MoneyLabel);
            this.Player1Panel.Controls.Add(this.Player1TitleTextBox);
            this.Player1Panel.Location = new System.Drawing.Point(4, 4);
            this.Player1Panel.Margin = new System.Windows.Forms.Padding(4);
            this.Player1Panel.Name = "Player1Panel";
            this.Player1Panel.Size = new System.Drawing.Size(328, 426);
            this.Player1Panel.TabIndex = 7;
            // 
            // Player1BudgetAmountLabel
            // 
            this.Player1BudgetAmountLabel.AutoSize = true;
            this.Player1BudgetAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1BudgetAmountLabel.Location = new System.Drawing.Point(128, 96);
            this.Player1BudgetAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1BudgetAmountLabel.Name = "Player1BudgetAmountLabel";
            this.Player1BudgetAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player1BudgetAmountLabel.TabIndex = 52;
            this.Player1BudgetAmountLabel.Text = "0";
            this.Player1BudgetAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player1BudgetAmountLabel.UseMnemonic = false;
            // 
            // Player1BudgetLabel
            // 
            this.Player1BudgetLabel.AutoSize = true;
            this.Player1BudgetLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1BudgetLabel.Location = new System.Drawing.Point(12, 96);
            this.Player1BudgetLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1BudgetLabel.Name = "Player1BudgetLabel";
            this.Player1BudgetLabel.Size = new System.Drawing.Size(80, 25);
            this.Player1BudgetLabel.TabIndex = 51;
            this.Player1BudgetLabel.Text = "Budget:";
            this.Player1BudgetLabel.UseMnemonic = false;
            // 
            // Player1ScoreAmountLabel
            // 
            this.Player1ScoreAmountLabel.AutoSize = true;
            this.Player1ScoreAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1ScoreAmountLabel.Location = new System.Drawing.Point(128, 71);
            this.Player1ScoreAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1ScoreAmountLabel.Name = "Player1ScoreAmountLabel";
            this.Player1ScoreAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player1ScoreAmountLabel.TabIndex = 50;
            this.Player1ScoreAmountLabel.Text = "0";
            this.Player1ScoreAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player1ScoreAmountLabel.UseMnemonic = false;
            // 
            // Player1MoneyAmountLabel
            // 
            this.Player1MoneyAmountLabel.AutoSize = true;
            this.Player1MoneyAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1MoneyAmountLabel.Location = new System.Drawing.Point(128, 47);
            this.Player1MoneyAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1MoneyAmountLabel.Name = "Player1MoneyAmountLabel";
            this.Player1MoneyAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player1MoneyAmountLabel.TabIndex = 49;
            this.Player1MoneyAmountLabel.Text = "0";
            this.Player1MoneyAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player1MoneyAmountLabel.UseMnemonic = false;
            // 
            // Player1EpicAmountLabel
            // 
            this.Player1EpicAmountLabel.AutoSize = true;
            this.Player1EpicAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1EpicAmountLabel.Location = new System.Drawing.Point(128, 279);
            this.Player1EpicAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1EpicAmountLabel.Name = "Player1EpicAmountLabel";
            this.Player1EpicAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player1EpicAmountLabel.TabIndex = 48;
            this.Player1EpicAmountLabel.Text = "0";
            this.Player1EpicAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player1EpicAmountLabel.UseMnemonic = false;
            // 
            // Player1FeatureAmountLabel
            // 
            this.Player1FeatureAmountLabel.AutoSize = true;
            this.Player1FeatureAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1FeatureAmountLabel.Location = new System.Drawing.Point(128, 255);
            this.Player1FeatureAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1FeatureAmountLabel.Name = "Player1FeatureAmountLabel";
            this.Player1FeatureAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player1FeatureAmountLabel.TabIndex = 47;
            this.Player1FeatureAmountLabel.Text = "0";
            this.Player1FeatureAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player1FeatureAmountLabel.UseMnemonic = false;
            // 
            // Player1StoryAmountLabel
            // 
            this.Player1StoryAmountLabel.AutoSize = true;
            this.Player1StoryAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1StoryAmountLabel.Location = new System.Drawing.Point(128, 230);
            this.Player1StoryAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1StoryAmountLabel.Name = "Player1StoryAmountLabel";
            this.Player1StoryAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player1StoryAmountLabel.TabIndex = 46;
            this.Player1StoryAmountLabel.Text = "0";
            this.Player1StoryAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player1StoryAmountLabel.UseMnemonic = false;
            // 
            // Player1TaskAmountLabel
            // 
            this.Player1TaskAmountLabel.AutoSize = true;
            this.Player1TaskAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1TaskAmountLabel.Location = new System.Drawing.Point(128, 206);
            this.Player1TaskAmountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1TaskAmountLabel.Name = "Player1TaskAmountLabel";
            this.Player1TaskAmountLabel.Size = new System.Drawing.Size(23, 25);
            this.Player1TaskAmountLabel.TabIndex = 45;
            this.Player1TaskAmountLabel.Text = "0";
            this.Player1TaskAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player1TaskAmountLabel.UseMnemonic = false;
            // 
            // Player1FullStackDevelopersLabel
            // 
            this.Player1FullStackDevelopersLabel.AutoSize = true;
            this.Player1FullStackDevelopersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1FullStackDevelopersLabel.Location = new System.Drawing.Point(276, 327);
            this.Player1FullStackDevelopersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1FullStackDevelopersLabel.Name = "Player1FullStackDevelopersLabel";
            this.Player1FullStackDevelopersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player1FullStackDevelopersLabel.TabIndex = 44;
            this.Player1FullStackDevelopersLabel.Text = "0";
            this.Player1FullStackDevelopersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player1FullStackDevelopersLabel.UseMnemonic = false;
            // 
            // Player1BackEndDevelopersLabel
            // 
            this.Player1BackEndDevelopersLabel.AutoSize = true;
            this.Player1BackEndDevelopersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1BackEndDevelopersLabel.Location = new System.Drawing.Point(215, 327);
            this.Player1BackEndDevelopersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1BackEndDevelopersLabel.Name = "Player1BackEndDevelopersLabel";
            this.Player1BackEndDevelopersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player1BackEndDevelopersLabel.TabIndex = 43;
            this.Player1BackEndDevelopersLabel.Text = "0";
            this.Player1BackEndDevelopersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player1BackEndDevelopersLabel.UseMnemonic = false;
            // 
            // Player1FrontEndDevelopersLabel
            // 
            this.Player1FrontEndDevelopersLabel.AutoSize = true;
            this.Player1FrontEndDevelopersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1FrontEndDevelopersLabel.Location = new System.Drawing.Point(153, 327);
            this.Player1FrontEndDevelopersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1FrontEndDevelopersLabel.Name = "Player1FrontEndDevelopersLabel";
            this.Player1FrontEndDevelopersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player1FrontEndDevelopersLabel.TabIndex = 42;
            this.Player1FrontEndDevelopersLabel.Text = "0";
            this.Player1FrontEndDevelopersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player1FrontEndDevelopersLabel.UseMnemonic = false;
            // 
            // Player1ScrumMastersLabel
            // 
            this.Player1ScrumMastersLabel.AutoSize = true;
            this.Player1ScrumMastersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1ScrumMastersLabel.Location = new System.Drawing.Point(92, 327);
            this.Player1ScrumMastersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1ScrumMastersLabel.Name = "Player1ScrumMastersLabel";
            this.Player1ScrumMastersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player1ScrumMastersLabel.TabIndex = 41;
            this.Player1ScrumMastersLabel.Text = "0";
            this.Player1ScrumMastersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player1ScrumMastersLabel.UseMnemonic = false;
            // 
            // Player1ProductOwnersLabel
            // 
            this.Player1ProductOwnersLabel.AutoSize = true;
            this.Player1ProductOwnersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1ProductOwnersLabel.Location = new System.Drawing.Point(31, 327);
            this.Player1ProductOwnersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1ProductOwnersLabel.Name = "Player1ProductOwnersLabel";
            this.Player1ProductOwnersLabel.Size = new System.Drawing.Size(23, 25);
            this.Player1ProductOwnersLabel.TabIndex = 40;
            this.Player1ProductOwnersLabel.Text = "0";
            this.Player1ProductOwnersLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Player1ProductOwnersLabel.UseMnemonic = false;
            // 
            // Player1FullStackDeveloperHomePictureBox
            // 
            this.Player1FullStackDeveloperHomePictureBox.Location = new System.Drawing.Point(260, 356);
            this.Player1FullStackDeveloperHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player1FullStackDeveloperHomePictureBox.Name = "Player1FullStackDeveloperHomePictureBox";
            this.Player1FullStackDeveloperHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player1FullStackDeveloperHomePictureBox.TabIndex = 39;
            this.Player1FullStackDeveloperHomePictureBox.TabStop = false;
            // 
            // Player1BackEndDeveloperHomePictureBox
            // 
            this.Player1BackEndDeveloperHomePictureBox.Location = new System.Drawing.Point(199, 356);
            this.Player1BackEndDeveloperHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player1BackEndDeveloperHomePictureBox.Name = "Player1BackEndDeveloperHomePictureBox";
            this.Player1BackEndDeveloperHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player1BackEndDeveloperHomePictureBox.TabIndex = 38;
            this.Player1BackEndDeveloperHomePictureBox.TabStop = false;
            // 
            // Player1FrontEndDeveloperHomePictureBox
            // 
            this.Player1FrontEndDeveloperHomePictureBox.Location = new System.Drawing.Point(137, 356);
            this.Player1FrontEndDeveloperHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player1FrontEndDeveloperHomePictureBox.Name = "Player1FrontEndDeveloperHomePictureBox";
            this.Player1FrontEndDeveloperHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player1FrontEndDeveloperHomePictureBox.TabIndex = 37;
            this.Player1FrontEndDeveloperHomePictureBox.TabStop = false;
            // 
            // Player1ScrumMasterHomePictureBox
            // 
            this.Player1ScrumMasterHomePictureBox.Location = new System.Drawing.Point(76, 356);
            this.Player1ScrumMasterHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player1ScrumMasterHomePictureBox.Name = "Player1ScrumMasterHomePictureBox";
            this.Player1ScrumMasterHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player1ScrumMasterHomePictureBox.TabIndex = 36;
            this.Player1ScrumMasterHomePictureBox.TabStop = false;
            // 
            // Player1ProductOwnerHomePictureBox
            // 
            this.Player1ProductOwnerHomePictureBox.Location = new System.Drawing.Point(15, 356);
            this.Player1ProductOwnerHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player1ProductOwnerHomePictureBox.Name = "Player1ProductOwnerHomePictureBox";
            this.Player1ProductOwnerHomePictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player1ProductOwnerHomePictureBox.TabIndex = 35;
            this.Player1ProductOwnerHomePictureBox.TabStop = false;
            // 
            // Player1EpicsLabel
            // 
            this.Player1EpicsLabel.AutoSize = true;
            this.Player1EpicsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1EpicsLabel.Location = new System.Drawing.Point(13, 279);
            this.Player1EpicsLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1EpicsLabel.Name = "Player1EpicsLabel";
            this.Player1EpicsLabel.Size = new System.Drawing.Size(66, 25);
            this.Player1EpicsLabel.TabIndex = 34;
            this.Player1EpicsLabel.Text = "Epics:";
            this.Player1EpicsLabel.UseMnemonic = false;
            // 
            // Player1FeaturesLabel
            // 
            this.Player1FeaturesLabel.AutoSize = true;
            this.Player1FeaturesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1FeaturesLabel.Location = new System.Drawing.Point(13, 255);
            this.Player1FeaturesLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1FeaturesLabel.Name = "Player1FeaturesLabel";
            this.Player1FeaturesLabel.Size = new System.Drawing.Size(95, 25);
            this.Player1FeaturesLabel.TabIndex = 33;
            this.Player1FeaturesLabel.Text = "Features:";
            this.Player1FeaturesLabel.UseMnemonic = false;
            // 
            // Player1StoriesLabel
            // 
            this.Player1StoriesLabel.AutoSize = true;
            this.Player1StoriesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1StoriesLabel.Location = new System.Drawing.Point(13, 230);
            this.Player1StoriesLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1StoriesLabel.Name = "Player1StoriesLabel";
            this.Player1StoriesLabel.Size = new System.Drawing.Size(79, 25);
            this.Player1StoriesLabel.TabIndex = 32;
            this.Player1StoriesLabel.Text = "Stories:";
            this.Player1StoriesLabel.UseMnemonic = false;
            // 
            // Player1TasksLabel
            // 
            this.Player1TasksLabel.AutoSize = true;
            this.Player1TasksLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1TasksLabel.Location = new System.Drawing.Point(13, 206);
            this.Player1TasksLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1TasksLabel.Name = "Player1TasksLabel";
            this.Player1TasksLabel.Size = new System.Drawing.Size(72, 25);
            this.Player1TasksLabel.TabIndex = 31;
            this.Player1TasksLabel.Text = "Tasks:";
            this.Player1TasksLabel.UseMnemonic = false;
            // 
            // Player1ClientCardHomePictureBox
            // 
            this.Player1ClientCardHomePictureBox.BackColor = System.Drawing.Color.Transparent;
            this.Player1ClientCardHomePictureBox.Location = new System.Drawing.Point(217, 206);
            this.Player1ClientCardHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player1ClientCardHomePictureBox.Name = "Player1ClientCardHomePictureBox";
            this.Player1ClientCardHomePictureBox.Size = new System.Drawing.Size(80, 111);
            this.Player1ClientCardHomePictureBox.TabIndex = 18;
            this.Player1ClientCardHomePictureBox.TabStop = false;
            // 
            // Player1TechnologyCardHomePictureBox
            // 
            this.Player1TechnologyCardHomePictureBox.BackColor = System.Drawing.Color.Transparent;
            this.Player1TechnologyCardHomePictureBox.Image = global::ScrumGame.Properties.Resources.TechnologyCardBack;
            this.Player1TechnologyCardHomePictureBox.Location = new System.Drawing.Point(204, 50);
            this.Player1TechnologyCardHomePictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player1TechnologyCardHomePictureBox.Name = "Player1TechnologyCardHomePictureBox";
            this.Player1TechnologyCardHomePictureBox.Size = new System.Drawing.Size(107, 148);
            this.Player1TechnologyCardHomePictureBox.TabIndex = 18;
            this.Player1TechnologyCardHomePictureBox.TabStop = false;
            this.Player1TechnologyCardHomePictureBox.Click += new System.EventHandler(this.Player1TechnologyCardHomePictureBox_Click);
            // 
            // Player1ResearchLabel
            // 
            this.Player1ResearchLabel.AutoSize = true;
            this.Player1ResearchLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1ResearchLabel.Location = new System.Drawing.Point(13, 121);
            this.Player1ResearchLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1ResearchLabel.Name = "Player1ResearchLabel";
            this.Player1ResearchLabel.Size = new System.Drawing.Size(101, 25);
            this.Player1ResearchLabel.TabIndex = 30;
            this.Player1ResearchLabel.Text = "Research:";
            this.Player1ResearchLabel.UseMnemonic = false;
            // 
            // Player1Research3PictureBox
            // 
            this.Player1Research3PictureBox.Location = new System.Drawing.Point(140, 149);
            this.Player1Research3PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player1Research3PictureBox.Name = "Player1Research3PictureBox";
            this.Player1Research3PictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player1Research3PictureBox.TabIndex = 29;
            this.Player1Research3PictureBox.TabStop = false;
            this.Player1Research3PictureBox.Click += new System.EventHandler(this.Player1Research3PictureBox_Click);
            // 
            // Player1Research2PictureBox
            // 
            this.Player1Research2PictureBox.Location = new System.Drawing.Point(79, 149);
            this.Player1Research2PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player1Research2PictureBox.Name = "Player1Research2PictureBox";
            this.Player1Research2PictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player1Research2PictureBox.TabIndex = 28;
            this.Player1Research2PictureBox.TabStop = false;
            this.Player1Research2PictureBox.Click += new System.EventHandler(this.Player1Research2PictureBox_Click);
            // 
            // Player1Research1PictureBox
            // 
            this.Player1Research1PictureBox.Location = new System.Drawing.Point(17, 149);
            this.Player1Research1PictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player1Research1PictureBox.Name = "Player1Research1PictureBox";
            this.Player1Research1PictureBox.Size = new System.Drawing.Size(53, 49);
            this.Player1Research1PictureBox.TabIndex = 27;
            this.Player1Research1PictureBox.TabStop = false;
            this.Player1Research1PictureBox.Click += new System.EventHandler(this.Player1Research1PictureBox_Click);
            // 
            // Player1ScoreLabel
            // 
            this.Player1ScoreLabel.AutoSize = true;
            this.Player1ScoreLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1ScoreLabel.Location = new System.Drawing.Point(12, 71);
            this.Player1ScoreLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1ScoreLabel.Name = "Player1ScoreLabel";
            this.Player1ScoreLabel.Size = new System.Drawing.Size(75, 25);
            this.Player1ScoreLabel.TabIndex = 2;
            this.Player1ScoreLabel.Text = "Score: ";
            this.Player1ScoreLabel.UseMnemonic = false;
            // 
            // Player1MoneyLabel
            // 
            this.Player1MoneyLabel.AutoSize = true;
            this.Player1MoneyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1MoneyLabel.Location = new System.Drawing.Point(12, 47);
            this.Player1MoneyLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Player1MoneyLabel.Name = "Player1MoneyLabel";
            this.Player1MoneyLabel.Size = new System.Drawing.Size(78, 25);
            this.Player1MoneyLabel.TabIndex = 1;
            this.Player1MoneyLabel.Text = "Money:";
            this.Player1MoneyLabel.UseMnemonic = false;
            // 
            // Player1TitleTextBox
            // 
            this.Player1TitleTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Player1TitleTextBox.Location = new System.Drawing.Point(17, 11);
            this.Player1TitleTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.Player1TitleTextBox.Name = "Player1TitleTextBox";
            this.Player1TitleTextBox.Size = new System.Drawing.Size(292, 30);
            this.Player1TitleTextBox.TabIndex = 0;
            this.Player1TitleTextBox.Text = "Player 1";
            this.Player1TitleTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Player1TitleTextBox.TextChanged += new System.EventHandler(this.Player1TitleTextBox_TextChanged);
            // 
            // ToolTip
            // 
            this.ToolTip.AutomaticDelay = 250;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1685, 897);
            this.Controls.Add(this.MainTableLayoutPanel);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1701, 931);
            this.Name = "MainForm";
            this.Text = "ScrumGame";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.MainTableLayoutPanel.ResumeLayout(false);
            this.Player4Panel.ResumeLayout(false);
            this.Player4Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Player4FullStackDeveloperHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4BackEndDeveloperHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4FrontEndDeveloperHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4ScrumMasterHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4ProductOwnerHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4ClientCardHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4TechnologyCardHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4Research3PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4Research2PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player4Research1PictureBox)).EndInit();
            this.Player3Panel.ResumeLayout(false);
            this.Player3Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Player3FullStackDeveloperHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3BackEndDeveloperHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3FrontEndDeveloperHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3ScrumMasterHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3ProductOwnerHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3ClientCardHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3TechnologyCardHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3Research3PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3Research2PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3Research1PictureBox)).EndInit();
            this.Player2Panel.ResumeLayout(false);
            this.Player2Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Player2FullStackDeveloperHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2BackEndDeveloperHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2FrontEndDeveloperHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2ScrumMasterHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2ProductOwnerHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2ClientCardHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2TechnologyCardHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2Research3PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2Research2PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2Research1PictureBox)).EndInit();
            this.CenterPanel.ResumeLayout(false);
            this.CenterPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScrumMasterSupplyPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProductOwnerSupplyPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceImage6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceImage5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceImage4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceImage3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceImage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceImage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice7PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice6PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice5PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FullStackDeveloperSupplyPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice4PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackEndDeveloperSupplyPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrontEndDeveloperSupplyPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice3PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice2PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice1PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkerSupplyPictureBox)).EndInit();
            this.GameBoardPanel.ResumeLayout(false);
            this.GameBoardPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Player4BudgetPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player3BudgetPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player2BudgetPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1BudgetPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TechnologyCardSlotsPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BudgetStripPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TechnologyCard4PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TechnologyCard3PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TechnologyCard2PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TechnologyCard1PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Client4PictureBox)).EndInit();
            this.TasklandPanel.ResumeLayout(false);
            this.TasklandPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Client3PictureBox)).EndInit();
            this.StorylandPanel.ResumeLayout(false);
            this.StorylandPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Client2PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Client1PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TechnologyCardSupplyPictureBox)).EndInit();
            this.Player1Panel.ResumeLayout(false);
            this.Player1Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Player1FullStackDeveloperHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1BackEndDeveloperHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1FrontEndDeveloperHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1ScrumMasterHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1ProductOwnerHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1ClientCardHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1TechnologyCardHomePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1Research3PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1Research2PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Player1Research1PictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem gameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rulesToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel MainTableLayoutPanel;
        private System.Windows.Forms.Panel CenterPanel;
        private System.Windows.Forms.Panel Player1Panel;
        private System.Windows.Forms.PictureBox Client1PictureBox;
        private System.Windows.Forms.PictureBox Client2PictureBox;
        private System.Windows.Forms.PictureBox Client3PictureBox;
        private System.Windows.Forms.PictureBox Client4PictureBox;
        private System.Windows.Forms.PictureBox TechnologyCard1PictureBox;
        private System.Windows.Forms.PictureBox TechnologyCard2PictureBox;
        private System.Windows.Forms.PictureBox TechnologyCard3PictureBox;
        private System.Windows.Forms.PictureBox TechnologyCard4PictureBox;
        private System.Windows.Forms.PictureBox BudgetStripPictureBox;
        private System.Windows.Forms.Panel BigWigPanel;
        private System.Windows.Forms.Panel JobFairPanel;
        private System.Windows.Forms.Panel GooglePanel;
        private System.Windows.Forms.Panel ContractOutPanel;
        private System.Windows.Forms.Panel TasklandPanel;
        private System.Windows.Forms.Panel StorylandPanel;
        private System.Windows.Forms.Panel EpiclandPanel;
        private System.Windows.Forms.Panel FeaturelandPanel;
        public System.Windows.Forms.PictureBox TechnologyCardSupplyPictureBox;
        private System.Windows.Forms.PictureBox Dice7PictureBox;
        private System.Windows.Forms.PictureBox Dice6PictureBox;
        private System.Windows.Forms.PictureBox Dice5PictureBox;
        private System.Windows.Forms.PictureBox Dice4PictureBox;
        private System.Windows.Forms.PictureBox Dice3PictureBox;
        private System.Windows.Forms.PictureBox Dice2PictureBox;
        private System.Windows.Forms.PictureBox Dice1PictureBox;
        private System.Windows.Forms.PictureBox WorkerSupplyPictureBox;
        private System.Windows.Forms.Panel GameBoardPanel;
        private System.Windows.Forms.Label Player1MoneyLabel;
        private System.Windows.Forms.TextBox Player1TitleTextBox;
        private System.Windows.Forms.Label Player1FullStackDevelopersLabel;
        private System.Windows.Forms.Label Player1BackEndDevelopersLabel;
        private System.Windows.Forms.Label Player1FrontEndDevelopersLabel;
        private System.Windows.Forms.Label Player1ScrumMastersLabel;
        private System.Windows.Forms.Label Player1ProductOwnersLabel;
        private System.Windows.Forms.PictureBox Player1FullStackDeveloperHomePictureBox;
        private System.Windows.Forms.PictureBox Player1BackEndDeveloperHomePictureBox;
        private System.Windows.Forms.PictureBox Player1FrontEndDeveloperHomePictureBox;
        private System.Windows.Forms.PictureBox Player1ScrumMasterHomePictureBox;
        private System.Windows.Forms.PictureBox Player1ProductOwnerHomePictureBox;
        private System.Windows.Forms.Label Player1EpicsLabel;
        private System.Windows.Forms.Label Player1FeaturesLabel;
        private System.Windows.Forms.Label Player1StoriesLabel;
        private System.Windows.Forms.Label Player1TasksLabel;
        private System.Windows.Forms.PictureBox Player1ClientCardHomePictureBox;
        private System.Windows.Forms.PictureBox Player1TechnologyCardHomePictureBox;
        private System.Windows.Forms.Label Player1ResearchLabel;
        private System.Windows.Forms.PictureBox Player1Research3PictureBox;
        private System.Windows.Forms.PictureBox Player1Research2PictureBox;
        private System.Windows.Forms.PictureBox Player1Research1PictureBox;
        private System.Windows.Forms.Label Player1ScoreLabel;
        private System.Windows.Forms.Panel Player4Panel;
        private System.Windows.Forms.Label Player4FullStackDevelopersLabel;
        private System.Windows.Forms.Label Player4BackEndDevelopersLabel;
        private System.Windows.Forms.Label Player4FrontEndDevelopersLabel;
        private System.Windows.Forms.Label Player4ScrumMastersLabel;
        private System.Windows.Forms.Label Player4ProductOwnersLabel;
        private System.Windows.Forms.PictureBox Player4FullStackDeveloperHomePictureBox;
        private System.Windows.Forms.PictureBox Player4BackEndDeveloperHomePictureBox;
        private System.Windows.Forms.PictureBox Player4FrontEndDeveloperHomePictureBox;
        private System.Windows.Forms.PictureBox Player4ScrumMasterHomePictureBox;
        private System.Windows.Forms.PictureBox Player4ProductOwnerHomePictureBox;
        private System.Windows.Forms.Label Player4EpicsLabel;
        private System.Windows.Forms.Label Player4FeaturesLabel;
        private System.Windows.Forms.Label Player4StoriesLabel;
        private System.Windows.Forms.Label Player4TasksLabel;
        private System.Windows.Forms.PictureBox Player4ClientCardHomePictureBox;
        private System.Windows.Forms.PictureBox Player4TechnologyCardHomePictureBox;
        private System.Windows.Forms.Label Player4ResearchLabel;
        private System.Windows.Forms.PictureBox Player4Research3PictureBox;
        private System.Windows.Forms.PictureBox Player4Research2PictureBox;
        private System.Windows.Forms.PictureBox Player4Research1PictureBox;
        private System.Windows.Forms.Label Player4ScoreLabel;
        private System.Windows.Forms.Label Player4MoneyLabel;
        private System.Windows.Forms.TextBox Player4TitleTextBox;
        private System.Windows.Forms.Panel Player3Panel;
        private System.Windows.Forms.Label Player3FullStackDevelopersLabel;
        private System.Windows.Forms.Label Player3BackEndDevelopersLabel;
        private System.Windows.Forms.Label Player3FrontEndDevelopersLabel;
        private System.Windows.Forms.Label Player3ScrumMastersLabel;
        private System.Windows.Forms.Label Player3ProductOwnersLabel;
        private System.Windows.Forms.PictureBox Player3FullStackDeveloperHomePictureBox;
        private System.Windows.Forms.PictureBox Player3BackEndDeveloperHomePictureBox;
        private System.Windows.Forms.PictureBox Player3FrontEndDeveloperHomePictureBox;
        private System.Windows.Forms.PictureBox Player3ScrumMasterHomePictureBox;
        private System.Windows.Forms.PictureBox Player3ProductOwnerHomePictureBox;
        private System.Windows.Forms.Label Player3EpicsLabel;
        private System.Windows.Forms.Label Player3FeaturesLabel;
        private System.Windows.Forms.Label Player3StoriesLabel;
        private System.Windows.Forms.Label Player3TasksLabel;
        private System.Windows.Forms.PictureBox Player3ClientCardHomePictureBox;
        private System.Windows.Forms.PictureBox Player3TechnologyCardHomePictureBox;
        private System.Windows.Forms.Label Player3ResearchLabel;
        private System.Windows.Forms.PictureBox Player3Research3PictureBox;
        private System.Windows.Forms.PictureBox Player3Research2PictureBox;
        private System.Windows.Forms.PictureBox Player3Research1PictureBox;
        private System.Windows.Forms.Label Player3ScoreLabel;
        private System.Windows.Forms.Label Player3MoneyLabel;
        private System.Windows.Forms.TextBox Player3TitleTextBox;
        private System.Windows.Forms.Panel Player2Panel;
        private System.Windows.Forms.Label Player2FullStackDevelopersLabel;
        private System.Windows.Forms.Label Player2BackEndDevelopersLabel;
        private System.Windows.Forms.Label Player2FrontEndDevelopersLabel;
        private System.Windows.Forms.Label Player2ScrumMastersLabel;
        private System.Windows.Forms.Label Player2ProductOwnersLabel;
        private System.Windows.Forms.PictureBox Player2FullStackDeveloperHomePictureBox;
        private System.Windows.Forms.PictureBox Player2BackEndDeveloperHomePictureBox;
        private System.Windows.Forms.PictureBox Player2FrontEndDeveloperHomePictureBox;
        private System.Windows.Forms.PictureBox Player2ScrumMasterHomePictureBox;
        private System.Windows.Forms.PictureBox Player2ProductOwnerHomePictureBox;
        private System.Windows.Forms.Label Player2EpicsLabel;
        private System.Windows.Forms.Label Player2FeaturesLabel;
        private System.Windows.Forms.Label Player2StoriesLabel;
        private System.Windows.Forms.Label Player2TasksLabel;
        private System.Windows.Forms.PictureBox Player2ClientCardHomePictureBox;
        private System.Windows.Forms.PictureBox Player2TechnologyCardHomePictureBox;
        private System.Windows.Forms.Label Player2ResearchLabel;
        private System.Windows.Forms.PictureBox Player2Research3PictureBox;
        private System.Windows.Forms.PictureBox Player2Research2PictureBox;
        private System.Windows.Forms.PictureBox Player2Research1PictureBox;
        private System.Windows.Forms.Label Player2ScoreLabel;
        private System.Windows.Forms.Label Player2MoneyLabel;
        private System.Windows.Forms.TextBox Player2TitleTextBox;
        private System.Windows.Forms.Label FullStackDeveloperSupplyLabel;
        private System.Windows.Forms.Label BackEndDeveloperSupplyLabel;
        private System.Windows.Forms.Label FrontEndDeveloperSupplyLabel;
        private System.Windows.Forms.PictureBox FullStackDeveloperSupplyPictureBox;
        private System.Windows.Forms.PictureBox BackEndDeveloperSupplyPictureBox;
        private System.Windows.Forms.PictureBox FrontEndDeveloperSupplyPictureBox;
        private System.Windows.Forms.Button RollDiceBtn;
        private Button button1;
        private System.Windows.Forms.PictureBox diceImage6;
        private System.Windows.Forms.PictureBox diceImage5;
        private System.Windows.Forms.PictureBox diceImage4;
        private System.Windows.Forms.PictureBox diceImage3;
        private System.Windows.Forms.PictureBox diceImage2;
        private System.Windows.Forms.PictureBox diceImage1;
        public System.Windows.Forms.TextBox DiceAmount;
        private System.Windows.Forms.Label TextLabel;
        private System.Windows.Forms.Label ScrumMasterSupplyLabel;
        private System.Windows.Forms.Label ProductOwnerSupplyLabel;
        private System.Windows.Forms.PictureBox ScrumMasterSupplyPictureBox;
        private System.Windows.Forms.PictureBox ProductOwnerSupplyPictureBox;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolTip ToolTip;
        private System.Windows.Forms.Label EpicSupplyLabel;
        private System.Windows.Forms.Label FeatureSupplyLabel;
        private System.Windows.Forms.Label TaskSupplyLabel;
        private System.Windows.Forms.Label StorySupplyLabel;
        private System.Windows.Forms.PictureBox TechnologyCardSlotsPictureBox;
        private System.Windows.Forms.Label Player4ScoreAmountLabel;
        private System.Windows.Forms.Label Player4MoneyAmountLabel;
        private System.Windows.Forms.Label Player4EpicAmountLabel;
        private System.Windows.Forms.Label Player4FeatureAmountLabel;
        private System.Windows.Forms.Label Player4StoryAmountLabel;
        private System.Windows.Forms.Label Player4TaskAmountLabel;
        private System.Windows.Forms.Label Player3ScoreAmountLabel;
        private System.Windows.Forms.Label Player3MoneyAmountLabel;
        private System.Windows.Forms.Label Player3EpicAmountLabel;
        private System.Windows.Forms.Label Player3FeatureAmountLabel;
        private System.Windows.Forms.Label Player3StoryAmountLabel;
        private System.Windows.Forms.Label Player3TaskAmountLabel;
        private System.Windows.Forms.Label Player2ScoreAmountLabel;
        private System.Windows.Forms.Label Player2MoneyAmountLabel;
        private System.Windows.Forms.Label Player2EpicAmountLabel;
        private System.Windows.Forms.Label Player2FeatureAmountLabel;
        private System.Windows.Forms.Label Player2StoryAmountLabel;
        private System.Windows.Forms.Label Player2TaskAmountLabel;
        private System.Windows.Forms.Label Player1ScoreAmountLabel;
        private System.Windows.Forms.Label Player1MoneyAmountLabel;
        private System.Windows.Forms.Label Player1EpicAmountLabel;
        private System.Windows.Forms.Label Player1FeatureAmountLabel;
        private System.Windows.Forms.Label Player1StoryAmountLabel;
        private System.Windows.Forms.Label Player1TaskAmountLabel;
        private System.Windows.Forms.Label Player4BudgetAmountLabel;
        private System.Windows.Forms.Label Player4BudgetLabel;
        private System.Windows.Forms.Label Player3BudgetAmountLabel;
        private System.Windows.Forms.Label Player3BudgetLabel;
        private System.Windows.Forms.Label Player2BudgetAmountLabel;
        private System.Windows.Forms.Label Player2BudgetLabel;
        private System.Windows.Forms.Label Player1BudgetAmountLabel;
        private System.Windows.Forms.Label Player1BudgetLabel;
        private System.Windows.Forms.PictureBox Player4BudgetPictureBox;
        private System.Windows.Forms.PictureBox Player3BudgetPictureBox;
        private System.Windows.Forms.PictureBox Player2BudgetPictureBox;
        private System.Windows.Forms.PictureBox Player1BudgetPictureBox;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

