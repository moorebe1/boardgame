﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumGame
{
    /// <summary>
    /// Load image based on this class type and the owner (player color)
    /// </summary>
    class ProductOwner : Worker
    {
        protected override void LoadImage()
        {
            if (Owner == null)
                Control.BackgroundImage = global::ScrumGame.Properties.Resources.ProductOwnerNeutral;
            else
            {
                switch (Owner.PlayerNumber)
                {
                    case 1:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.ProductOwnerPlayer1;
                        break;
                    case 2:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.ProductOwnerPlayer2;
                        break;
                    case 3:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.ProductOwnerPlayer3;
                        break;
                    case 4:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.ProductOwnerPlayer4;
                        break;
                }
            }
        }
    }
}
