﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Resources;
using System.IO;
using System.Media;
using ScrumGame.Properties;

namespace ScrumGame
{
    public partial class MainForm : Form
    {
        #region Properties

        Form2 frm2 = new Form2(); //Make form 2 a global variable so the task list items persist closing

        //-----------------------------------------------------------Properties-------------------------------------------------------------------------------
        /// <summary>
        /// Custom color progress bar for player1
        /// </summary>
        CustomProgressBar Player1PointProgressBar = new CustomProgressBar();

        /// <summary>
        /// Custom color progress bar for player2
        /// </summary>
        CustomProgressBar Player2PointProgressBar = new CustomProgressBar();

        /// <summary>
        /// Custom color progress bar for player3
        /// </summary>
        CustomProgressBar Player3PointProgressBar = new CustomProgressBar();

        /// <summary>
        /// Custom color progress bar for player4
        /// </summary>
        /// 
        CustomProgressBar Player4PointProgressBar = new CustomProgressBar();

        /// <summary>
        /// Custom color progress bar for player1
        /// </summary>
        CustomProgressBar Player1BudgetProgressBar = new CustomProgressBar();

        /// <summary>
        /// Custom color progress bar for player2
        /// </summary>
        CustomProgressBar Player2BudgetProgressBar = new CustomProgressBar();

        /// <summary>
        /// Custom color progress bar for player3
        /// </summary>
        CustomProgressBar Player3BudgetProgressBar = new CustomProgressBar();

        /// <summary>
        /// Custom color progress bar for player4
        /// </summary>
        /// 
        CustomProgressBar Player4BudgetProgressBar = new CustomProgressBar();

        /// <summary>
        /// Player objects to keep track of points and resources
        /// </summary>
        public Player[] Players { get; set; }
        /// <summary>
        /// Vertical offset to account for border and menu bar
        /// </summary>
        private const int Y_OFFSET = 23;

        /// <summary>
        /// Horizontal offset between resource control and the first location to place a worker
        /// </summary>
        private const int RESOURCE_X_OFFSET = 10;

        /// <summary>
        /// Vertical offset between resource control and the first location to place a worker
        /// </summary>
        private const int RESOURCE_Y_OFFSET = 6;

        /// <summary>
        /// List of all workers in game
        /// </summary>
        private List<Worker> WorkersList { get; set; }

        /// <summary>
        /// List of all front-end developers in supply
        /// </summary>
        private List<Worker> FrontEndDeveloperSupplyList { get; set; }

        /// <summary>
        /// list of all back-end developers in supply
        /// </summary>
        private List<Worker> BackEndDeveloperSupplyList { get; set; }

        /// <summary>
        /// list of all full-stack developers in supply
        /// </summary>
        private List<Worker> FullStackDeveloperSupplyList { get; set; }

        /// <summary>
        /// list of all product owners in supply
        /// </summary>
        private List<Worker> ProductOwnerSupplyList { get; set; }

        /// <summary>
        /// list of all scrum masters in supply
        /// </summary>
        private List<Worker> ScrumMasterSupplyList { get; set; }

        /// <summary>
        /// list of all workers on Countract Out spot
        /// </summary>
        private List<Worker> ContractOutWorkersList { get; set; }

        /// <summary>
        /// list of all workers on Taskland
        /// </summary>
        private List<Worker> TaskWorkersList { get; set; }

        /// <summary>
        /// list of all workers on Storyland
        /// </summary>
        private List<Worker> StoryWorkersList { get; set; }

        /// <summary>
        /// list of all workers on Featureland
        /// </summary>
        private List<Worker> FeatureWorkersList { get; set; }

        /// <summary>
        /// list of all workers on Epicland
        /// </summary>
        private List<Worker> EpicWorkersList { get; set; }

        /// <summary>
        /// Worker on Google spot
        /// </summary>
        private Worker GoogleWorker { get; set; }

        /// <summary>
        /// Workers on Job fair spot
        /// </summary>
        private List<Worker> JobFairWorkersList { get; set; }

        /// <summary>
        /// Worker on BigWigs Spot
        /// </summary>
        private Worker BigWigsWorker { get; set; }

        /// <summary>
        /// The pointer is dragging a control if true.
        /// </summary>
        public bool IsDragging { get; set; }

        /// <summary>
        /// The point at which dragging began.
        /// </summary>
        public Point DragPoint { get; set; }

        public int[] ResourceSupply { get; set; }
        public Label[] ResourceLabels { get; set; }

        public Stack<TechnologyCard> TechnologyCardStack { get; set; }


        public int DiceTotal { get; set; }
        public bool AllowDiceRoll { get; set; }


        private int ResearchSupply { get; set; }
        public TechnologyCard[] TechnologyCardMasterArray { get; set; }

        public Player ActivePlayer { get; set; }

        public TechnologyCard[] TechnologyCardSlots {get; set;}
        public PictureBox[] TechnologyCardPictureBoxes { get; set; }

        public Worker[] TechnologyCardWorkerArray { get; set; }
        #endregion
        //-----------------------------------------------------------------------Methods------------------------------------------------------------------------------------
        /// <summary>
        /// Default Constructor
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            InitializeCustomeComponent();

        }

        /// <summary>
        /// Declare and Initialize Controls that can be customized by the developer
        /// </summary>
        private void InitializeCustomeComponent()
        {
            DiceTotal = 0;
            AllowDiceRoll = true;
            
            //
            //Players
            //
            Players = new Player[4];
            for (int i = 1; i < 5; i++)
            {
                Players[i - 1] = new Player("Player " + i);
                Players[i - 1].PlayerNumber = i;
            }
            ActivePlayer = Players[0];

            // 
            // Player1PointProgressBar
            // 
            Player1PointProgressBar.ForeColor = System.Drawing.Color.Fuchsia;
            Player1PointProgressBar.Location = new System.Drawing.Point(22, 7);
            Player1PointProgressBar.Name = "progressBar1";
            Player1PointProgressBar.Size = new System.Drawing.Size(700, 9);
            Player1PointProgressBar.TabIndex = 18;
            Player1PointProgressBar.MouseHover += ProgressBar1_MouseHover;
            this.GameBoardPanel.Controls.Add(Player1PointProgressBar);

            // 
            // Player2PointProgressBar
            // 
            Player2PointProgressBar.ForeColor = System.Drawing.Color.Turquoise;
            Player2PointProgressBar.Location = new System.Drawing.Point(22, 16);
            Player2PointProgressBar.Name = "progressBar1";
            Player2PointProgressBar.Size = new System.Drawing.Size(700, 9);
            Player2PointProgressBar.TabIndex = 18;
            Player2PointProgressBar.MouseHover += ProgressBar2_MouseHover;
            this.GameBoardPanel.Controls.Add(Player2PointProgressBar);

            // 
            // Player3PointProgressBar
            // 
            Player3PointProgressBar.ForeColor = System.Drawing.Color.Yellow;
            Player3PointProgressBar.Location = new System.Drawing.Point(22, 25);
            Player3PointProgressBar.Name = "progressBar1";
            Player3PointProgressBar.Size = new System.Drawing.Size(700, 9);
            Player3PointProgressBar.TabIndex = 18;
            Player3PointProgressBar.MouseHover += ProgressBar3_MouseHover;
            this.GameBoardPanel.Controls.Add(Player3PointProgressBar);

            // 
            // Player4PointProgressBar
            // 
            Player4PointProgressBar.ForeColor = System.Drawing.Color.Blue;
            Player4PointProgressBar.Location = new System.Drawing.Point(22, 34);
            Player4PointProgressBar.Name = "progressBar1";
            Player4PointProgressBar.Size = new System.Drawing.Size(700, 9);
            Player4PointProgressBar.TabIndex = 18;
            Player4PointProgressBar.MouseHover += ProgressBar4_MouseHover;
            this.GameBoardPanel.Controls.Add(Player4PointProgressBar);

           

            //
            //WorkersList(s)
            //
            WorkersList = new List<Worker>();
            ContractOutWorkersList = new List<Worker>();
            TaskWorkersList = new List<Worker>();
            StoryWorkersList = new List<Worker>();
            FeatureWorkersList = new List<Worker>();
            EpicWorkersList = new List<Worker>();
            JobFairWorkersList = new List<Worker>();
            GoogleWorker = null;
            BigWigsWorker = null;


            //
            //FrontEndDeveloper Tokens
            //
            FrontEndDeveloperSupplyList = new List<Worker>();
            for (int i = 0; i < 12; i++)
            {
                FrontEndDeveloperSupplyList.Add(new FrontEndDeveloper());
                Point location = new Point(this.CenterPanel.Location.X + FrontEndDeveloperSupplyPictureBox.Location.X, this.CenterPanel.Location.Y + FrontEndDeveloperSupplyPictureBox.Location.Y + Y_OFFSET);
                FrontEndDeveloperSupplyList[i].Control.Location = location;
                FrontEndDeveloperSupplyList[i].Control.Name = "FrontEndDeveloperPictureBox" + (i + 1);
                this.Controls.Add(FrontEndDeveloperSupplyList[i].Control);
                FrontEndDeveloperSupplyList[i].Control.BringToFront();
                FrontEndDeveloperSupplyLabel.Text = FrontEndDeveloperSupplyList.Count.ToString();
            }
            foreach (Worker w in FrontEndDeveloperSupplyList)
            {
                WorkersList.Add(w);
            }
            //
            //BackEndDeveloper Tokens
            //
            BackEndDeveloperSupplyList = new List<Worker>();
            for (int i = 0; i < 12; i++)
            {
                BackEndDeveloperSupplyList.Add(new BackEndDeveloper());
                Point location = new Point(this.CenterPanel.Location.X + BackEndDeveloperSupplyPictureBox.Location.X, this.CenterPanel.Location.Y + BackEndDeveloperSupplyPictureBox.Location.Y + Y_OFFSET);
                BackEndDeveloperSupplyList[i].Control.Location = location;
                BackEndDeveloperSupplyList[i].Control.Name = "BackEndDeveloperPictureBox" + (i + 1);
                this.Controls.Add(BackEndDeveloperSupplyList[i].Control);
                BackEndDeveloperSupplyList[i].Control.BringToFront();
                BackEndDeveloperSupplyLabel.Text = BackEndDeveloperSupplyList.Count.ToString();
            }
            foreach (Worker w in BackEndDeveloperSupplyList)
            {
                WorkersList.Add(w);
            }


            //
            //FullStackDeveloper Tokens
            //
            FullStackDeveloperSupplyList = new List<Worker>();
            for (int i = 0; i < 12; i++)
            {
                FullStackDeveloperSupplyList.Add(new FullStackDeveloper());
                Point location = new Point(this.CenterPanel.Location.X + FullStackDeveloperSupplyPictureBox.Location.X, this.CenterPanel.Location.Y + FullStackDeveloperSupplyPictureBox.Location.Y + Y_OFFSET);
                FullStackDeveloperSupplyList[i].Control.Location = location;
                FullStackDeveloperSupplyList[i].Control.Name = "FullStackDeveloperPictureBox" + (i + 1);
                this.Controls.Add(FullStackDeveloperSupplyList[i].Control);
                FullStackDeveloperSupplyList[i].Control.BringToFront();
                FullStackDeveloperSupplyLabel.Text = FullStackDeveloperSupplyList.Count.ToString();
            }
            foreach (Worker w in FullStackDeveloperSupplyList)
            {
                WorkersList.Add(w);
            }

            //
            //ProductOwner Tokens
            //
            ProductOwnerSupplyList = new List<Worker>();
            for (int i = 0; i < 4; i++)
            {
                ProductOwnerSupplyList.Add(new ProductOwner());
                Point location = new Point(this.CenterPanel.Location.X + ProductOwnerSupplyPictureBox.Location.X, this.CenterPanel.Location.Y + ProductOwnerSupplyPictureBox.Location.Y + Y_OFFSET);
                ProductOwnerSupplyList[i].Control.Location = location;
                ProductOwnerSupplyList[i].Control.Name = "ProductOwnerPictureBox" + (i + 1);
                this.Controls.Add(ProductOwnerSupplyList[i].Control);
                ProductOwnerSupplyList[i].Control.BringToFront();
                ProductOwnerSupplyLabel.Text = ProductOwnerSupplyList.Count.ToString();
            }
            foreach (Worker w in ProductOwnerSupplyList)
            {
                WorkersList.Add(w);
            }

            //
            //ScrumMaster
            //
            ScrumMasterSupplyList = new List<Worker>();
            for (int i = 0; i < 4; i++)
            {
                ScrumMasterSupplyList.Add(new ScrumMaster());
                Point location = new Point(this.CenterPanel.Location.X + ScrumMasterSupplyPictureBox.Location.X, this.CenterPanel.Location.Y + ScrumMasterSupplyPictureBox.Location.Y + Y_OFFSET);
                ScrumMasterSupplyList[i].Control.Location = location;
                ScrumMasterSupplyList[i].Control.Name = "ScrumMasterPictureBox" + (i + 1);
                this.Controls.Add(ScrumMasterSupplyList[i].Control);
                ScrumMasterSupplyList[i].Control.BringToFront();
                ScrumMasterSupplyLabel.Text = ScrumMasterSupplyList.Count.ToString();
            }
            foreach (Worker w in ScrumMasterSupplyList)
            {
                WorkersList.Add(w);
            }

            //
            //Add drag and drop functionality to game pieces
            //
            foreach (Worker w in FrontEndDeveloperSupplyList)
            {
                w.Control.MouseDown += new MouseEventHandler(GamePiecePictureBox_MouseDown);
                w.Control.MouseMove += new MouseEventHandler(GamePiecePictureBox_MouseMove);
                w.Control.MouseUp += new MouseEventHandler(GamePiecePictureBox_MouseUp);
                w.Control.MouseHover += new System.EventHandler(FrontEndDeveloper_MouseHover);

            }
            foreach (Worker w in BackEndDeveloperSupplyList)
            {
                w.Control.MouseDown += new MouseEventHandler(GamePiecePictureBox_MouseDown);
                w.Control.MouseMove += new MouseEventHandler(GamePiecePictureBox_MouseMove);
                w.Control.MouseUp += new MouseEventHandler(GamePiecePictureBox_MouseUp);
                w.Control.MouseHover += new System.EventHandler(BackEndDeveloper_MouseHover);
            }
            foreach (Worker w in FullStackDeveloperSupplyList)
            {
                w.Control.MouseDown += new MouseEventHandler(GamePiecePictureBox_MouseDown);
                w.Control.MouseMove += new MouseEventHandler(GamePiecePictureBox_MouseMove);
                w.Control.MouseUp += new MouseEventHandler(GamePiecePictureBox_MouseUp);
                w.Control.MouseHover += new System.EventHandler(FullStackDeveloper_MouseHover);
            }
            foreach (Worker w in ProductOwnerSupplyList)
            {
                w.Control.MouseDown += new MouseEventHandler(GamePiecePictureBox_MouseDown);
                w.Control.MouseMove += new MouseEventHandler(GamePiecePictureBox_MouseMove);
                w.Control.MouseUp += new MouseEventHandler(GamePiecePictureBox_MouseUp);
                w.Control.MouseHover += new System.EventHandler(ProductOwner_MouseHover);
            }
            foreach (Worker w in ScrumMasterSupplyList)
            {
                w.Control.MouseDown += new MouseEventHandler(GamePiecePictureBox_MouseDown);
                w.Control.MouseMove += new MouseEventHandler(GamePiecePictureBox_MouseMove);
                w.Control.MouseUp += new MouseEventHandler(GamePiecePictureBox_MouseUp);
                w.Control.MouseHover += new System.EventHandler(ScrumMaster_MouseHover);
            }

            //
            // Resources
            //
            ResourceSupply = new int[] { 20, 16, 12, 10 };
            ResourceLabels = new Label[] { TaskSupplyLabel, StorySupplyLabel, FeatureSupplyLabel, EpicSupplyLabel };
            for (int i = 0; i < 4; i++)
            {
                ResourceLabels[i].Text = ResourceSupply[i].ToString();
            }
            ResearchSupply = 90;

            //
            // Technology Cards
            //
            TechnologyCardStack = new Stack<TechnologyCard>();
            TechnologyCardMasterArray = new TechnologyCard[36];
            TechnologyCardPictureBoxes = new PictureBox[] { TechnologyCard1PictureBox, TechnologyCard2PictureBox, TechnologyCard3PictureBox, TechnologyCard4PictureBox };
            TechnologyCardWorkerArray = new Worker[4];

            TechnologyCardMasterArray[0] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard1)));
            TechnologyCardMasterArray[0].CardEvent = new DiceEvent(ref TechnologyCardMasterArray[0]);
            TechnologyCardMasterArray[0].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[0]);

            TechnologyCardMasterArray[1] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard2)));
            TechnologyCardMasterArray[1].CardEvent = new DiceEvent(ref TechnologyCardMasterArray[1]);
            TechnologyCardMasterArray[1].CardPoints = new ClientCardPoints(ref TechnologyCardMasterArray[1], 1);

            TechnologyCardMasterArray[2] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard3)));
            TechnologyCardMasterArray[2].CardEvent = new DiceEvent(ref TechnologyCardMasterArray[2]);
            TechnologyCardMasterArray[2].CardPoints = new ClientCardPoints(ref TechnologyCardMasterArray[2], 2);

            TechnologyCardMasterArray[3] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard4)));
            TechnologyCardMasterArray[3].CardEvent = new DiceEvent(ref TechnologyCardMasterArray[3]);
            TechnologyCardMasterArray[3].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[3]);

            TechnologyCardMasterArray[4] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard5)));
            TechnologyCardMasterArray[4].CardEvent = new DiceEvent(ref TechnologyCardMasterArray[4]);
            TechnologyCardMasterArray[4].CardPoints = new ResearchTokenPoints(ref TechnologyCardMasterArray[4], 2);

            TechnologyCardMasterArray[5] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard6)));
            TechnologyCardMasterArray[5].CardEvent = new DiceEvent(ref TechnologyCardMasterArray[5]);
            TechnologyCardMasterArray[5].CardPoints = new BudgetPoints(ref TechnologyCardMasterArray[5], 1);

            TechnologyCardMasterArray[6] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard7)));
            TechnologyCardMasterArray[6].CardEvent = new DiceEvent(ref TechnologyCardMasterArray[6]);
            TechnologyCardMasterArray[6].CardPoints = new BudgetPoints(ref TechnologyCardMasterArray[6], 2);

            TechnologyCardMasterArray[7] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard8)));
            TechnologyCardMasterArray[7].CardEvent = new DiceEvent(ref TechnologyCardMasterArray[7]);
            TechnologyCardMasterArray[7].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[7]);

            TechnologyCardMasterArray[8] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard9)));
            TechnologyCardMasterArray[8].CardEvent = new DiceEvent(ref TechnologyCardMasterArray[8]);
            TechnologyCardMasterArray[8].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[8]);

            TechnologyCardMasterArray[9] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard10)));
            TechnologyCardMasterArray[9].CardEvent = new DiceEvent(ref TechnologyCardMasterArray[9]);
            TechnologyCardMasterArray[9].CardPoints = new ResearchTokenPoints(ref TechnologyCardMasterArray[9], 1);

            TechnologyCardMasterArray[10] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard11)));
            TechnologyCardMasterArray[10].CardEvent = new MoneyEvent(ref TechnologyCardMasterArray[10], 7);
            TechnologyCardMasterArray[10].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[10]);

            TechnologyCardMasterArray[11] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard12)));
            TechnologyCardMasterArray[11].CardEvent = new MoneyEvent(ref TechnologyCardMasterArray[11], 2);
            TechnologyCardMasterArray[11].CardPoints = new ClientCardPoints(ref TechnologyCardMasterArray[11], 2);

            TechnologyCardMasterArray[12] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard13)));
            TechnologyCardMasterArray[12].CardEvent = new MoneyEvent(ref TechnologyCardMasterArray[12], 4);
            TechnologyCardMasterArray[12].CardPoints = new ClientCardPoints(ref TechnologyCardMasterArray[12], 1);

            TechnologyCardMasterArray[13] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard14)));
            TechnologyCardMasterArray[13].CardEvent = new MoneyEvent(ref TechnologyCardMasterArray[13], 5);
            TechnologyCardMasterArray[13].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[13]);

            TechnologyCardMasterArray[14] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard15)));
            TechnologyCardMasterArray[14].CardEvent = new MoneyEvent(ref TechnologyCardMasterArray[14], 3);
            TechnologyCardMasterArray[14].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[14]);

            TechnologyCardMasterArray[15] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard16)));
            TechnologyCardMasterArray[15].CardEvent = new MoneyEvent(ref TechnologyCardMasterArray[15], 1);
            TechnologyCardMasterArray[15].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[15]);

            TechnologyCardMasterArray[16] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard17)));
            TechnologyCardMasterArray[16].CardEvent = new MoneyEvent(ref TechnologyCardMasterArray[16], 3);
            TechnologyCardMasterArray[16].CardPoints = new BudgetPoints(ref TechnologyCardMasterArray[16], 2);

            TechnologyCardMasterArray[17] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard18)));
            TechnologyCardMasterArray[17].CardEvent = new ResourceEvent(ref TechnologyCardMasterArray[17],0,0,1,0);
            TechnologyCardMasterArray[17].CardPoints = new BudgetPoints(ref TechnologyCardMasterArray[17], 1);

            TechnologyCardMasterArray[18] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard19)));
            TechnologyCardMasterArray[18].CardEvent = new ResourceEvent(ref TechnologyCardMasterArray[18],0,0,2,0);
            TechnologyCardMasterArray[18].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[18]);

            TechnologyCardMasterArray[19] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard20)));
            TechnologyCardMasterArray[19].CardEvent = new ResourceEvent(ref TechnologyCardMasterArray[19],0,0,1,0);
            TechnologyCardMasterArray[19].CardPoints = new WorkerPoints(ref TechnologyCardMasterArray[19], 1);

            TechnologyCardMasterArray[20] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard21)));
            TechnologyCardMasterArray[20].CardEvent = new ResourceEvent(ref TechnologyCardMasterArray[20],0,0,0,1);
            TechnologyCardMasterArray[20].CardPoints = new WorkerPoints(ref TechnologyCardMasterArray[20], 1);

            TechnologyCardMasterArray[21] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard22)));
            TechnologyCardMasterArray[21].CardEvent = new ResourceEvent(ref TechnologyCardMasterArray[21],0,1,0,0);
            TechnologyCardMasterArray[21].CardPoints = new WorkerPoints(ref TechnologyCardMasterArray[21], 2);

            TechnologyCardMasterArray[22] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard23)));
            TechnologyCardMasterArray[22].CardEvent = new DiceResourceEvent(ref TechnologyCardMasterArray[22], 3);
            TechnologyCardMasterArray[22].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[22]);

            TechnologyCardMasterArray[23] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard24)));
            TechnologyCardMasterArray[23].CardEvent = new DiceResourceEvent(ref TechnologyCardMasterArray[23], 0);
            TechnologyCardMasterArray[23].CardPoints = new WorkerPoints(ref TechnologyCardMasterArray[23], 2);

            TechnologyCardMasterArray[24] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard25)));
            TechnologyCardMasterArray[24].CardEvent = new DiceResourceEvent(ref TechnologyCardMasterArray[24], 2);
            TechnologyCardMasterArray[24].CardPoints = new WorkerPoints(ref TechnologyCardMasterArray[24], 1);

            TechnologyCardMasterArray[25] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard26)));
            TechnologyCardMasterArray[25].CardEvent = new PointsEvent(ref TechnologyCardMasterArray[25],3);
            TechnologyCardMasterArray[25].CardPoints = new ClientCardPoints(ref TechnologyCardMasterArray[25], 3);

            TechnologyCardMasterArray[26] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard27)));
            TechnologyCardMasterArray[26].CardEvent = new PointsEvent(ref TechnologyCardMasterArray[26],3);
            TechnologyCardMasterArray[26].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[26]);

            TechnologyCardMasterArray[27] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard28)));
            TechnologyCardMasterArray[27].CardEvent = new PointsEvent(ref TechnologyCardMasterArray[27],3);
            TechnologyCardMasterArray[27].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[27]);

            TechnologyCardMasterArray[28] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard29)));
            TechnologyCardMasterArray[28].CardEvent = new ResearchEvent(ref TechnologyCardMasterArray[28]);
            TechnologyCardMasterArray[28].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[28]);

            TechnologyCardMasterArray[29] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard30)));
            TechnologyCardMasterArray[29].CardEvent = new BudgetEvent(ref TechnologyCardMasterArray[29]);
            TechnologyCardMasterArray[29].CardPoints = new BudgetPoints(ref TechnologyCardMasterArray[29], 1);

            TechnologyCardMasterArray[30] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard31)));
            TechnologyCardMasterArray[30].CardEvent = new BudgetEvent(ref TechnologyCardMasterArray[30]);
            TechnologyCardMasterArray[30].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[30]);

            TechnologyCardMasterArray[31] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard32)));
            TechnologyCardMasterArray[31].CardEvent = new DrawCardEvent(ref TechnologyCardMasterArray[31]);
            TechnologyCardMasterArray[31].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[31]);

            TechnologyCardMasterArray[32] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard33)));
            TechnologyCardMasterArray[32].CardEvent = new TempResearchEvent(ref TechnologyCardMasterArray[32],4);
            TechnologyCardMasterArray[32].CardPoints = new ResearchTokenPoints(ref TechnologyCardMasterArray[32], 1);

            TechnologyCardMasterArray[33] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard34)));
            TechnologyCardMasterArray[33].CardEvent = new TempResearchEvent(ref TechnologyCardMasterArray[33],3);
            TechnologyCardMasterArray[33].CardPoints = new ResearchTokenPoints(ref TechnologyCardMasterArray[33], 1);

            TechnologyCardMasterArray[34] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard35)));
            TechnologyCardMasterArray[34].CardEvent = new TempResearchEvent(ref TechnologyCardMasterArray[34],2);
            TechnologyCardMasterArray[34].CardPoints = new ResearchTokenPoints(ref TechnologyCardMasterArray[34], 2);

            TechnologyCardMasterArray[35] = new TechnologyCard(((System.Drawing.Image)(ScrumGame.Properties.Resources.TechnologyCard36)));
            TechnologyCardMasterArray[35].CardEvent = new WildResourceEvent(ref TechnologyCardMasterArray[35]);
            TechnologyCardMasterArray[35].CardPoints = new GreenCardPoints(ref TechnologyCardMasterArray[35]);

            List<TechnologyCard> RandomBuffer = new List<TechnologyCard>(TechnologyCardMasterArray.ToList());
            Random rand = new Random();
            int index = 0;
            for (int i = 0; i < TechnologyCardMasterArray.Length; i++)
            {
                index = rand.Next(RandomBuffer.Count);
                TechnologyCardStack.Push(RandomBuffer[index]);
                RandomBuffer.RemoveAt(index);
            }
            TechnologyCardSlots = new TechnologyCard[4];
            FillTechnologyCardSlots();
            UpdateLabels();
        }

        public void FillTechnologyCardSlots()
        {
            if (TechnologyCardStack.Count > 0)
            {
                if (TechnologyCardSlots[0] == null)
                {
                    TechnologyCardSlots[0] = TechnologyCardStack.Pop();
                    TechnologyCard1PictureBox.Image = TechnologyCardSlots[0].Image;
                }
                if (TechnologyCardSlots[1] == null)
                {
                    TechnologyCardSlots[1] = TechnologyCardStack.Pop();
                    TechnologyCard2PictureBox.Image = TechnologyCardSlots[1].Image;
                }
                if (TechnologyCardSlots[2] == null)
                {
                    TechnologyCardSlots[2] = TechnologyCardStack.Pop();
                    TechnologyCard3PictureBox.Image = TechnologyCardSlots[2].Image;
                }
                if (TechnologyCardSlots[3] == null)
                {
                    TechnologyCardSlots[3] = TechnologyCardStack.Pop();
                    TechnologyCard4PictureBox.Image = TechnologyCardSlots[3].Image;
                }
            }
            else
            {
                TechnologyCardSupplyPictureBox.Image = null;
                for (int i = 0; i < 4; i++)
                {
                    if (TechnologyCardSlots[i] == null)
                    {
                        TechnologyCardPictureBoxes[i].Image = null;
                    }
                }
            }
            
            

        }

        public void UpdateLabels()
        {
            if (Players[0] != null)
            {
                Player1MoneyAmountLabel.Text = Players[0].Money.ToString();
                Player1ScoreAmountLabel.Text = Players[0].Points.ToString();
                Player1BudgetAmountLabel.Text = Players[0].Budget.ToString();
                Player1TaskAmountLabel.Text = Players[0].Resources[0].ToString();
                Player1StoryAmountLabel.Text = Players[0].Resources[1].ToString();
                Player1FeatureAmountLabel.Text = Players[0].Resources[2].ToString();
                Player1EpicAmountLabel.Text = Players[0].Resources[3].ToString();
                Player1Research1PictureBox.Image = Players[0].CurrentResearchImages[0];
                Player1Research2PictureBox.Image = Players[0].CurrentResearchImages[1];
                Player1Research3PictureBox.Image = Players[0].CurrentResearchImages[2];
                Player1PointProgressBar.Value = Players[0].Points;
                Player1BudgetPictureBox.Location = new System.Drawing.Point(24, 501 - (Players[0].Budget * 40));
                Player1BudgetPictureBox.Height = 20 + (40 * Players[0].Budget);

            }
            if (Players[1] != null)
            {
                Player2MoneyAmountLabel.Text = Players[1].Money.ToString();
                Player2ScoreAmountLabel.Text = Players[1].Points.ToString();
                Player2BudgetAmountLabel.Text = Players[1].Budget.ToString();
                Player2TaskAmountLabel.Text = Players[1].Resources[0].ToString();
                Player2StoryAmountLabel.Text = Players[1].Resources[1].ToString();
                Player2FeatureAmountLabel.Text = Players[1].Resources[2].ToString();
                Player2EpicAmountLabel.Text = Players[1].Resources[3].ToString();
                Player2Research1PictureBox.Image = Players[1].CurrentResearchImages[0];
                Player2Research2PictureBox.Image = Players[1].CurrentResearchImages[1];
                Player2Research3PictureBox.Image = Players[1].CurrentResearchImages[2];
                Player2PointProgressBar.Value = Players[1].Points;
                Player2BudgetPictureBox.Location = new System.Drawing.Point(32, 501 - (Players[1].Budget * 40));
                Player2BudgetPictureBox.Height = 20 + (40 * Players[1].Budget);
            }
            if (Players[2] != null)
            {
                Player3MoneyAmountLabel.Text = Players[2].Money.ToString();
                Player3ScoreAmountLabel.Text = Players[2].Points.ToString();
                Player3BudgetAmountLabel.Text = Players[2].Budget.ToString();
                Player3TaskAmountLabel.Text = Players[2].Resources[0].ToString();
                Player3StoryAmountLabel.Text = Players[2].Resources[1].ToString();
                Player3FeatureAmountLabel.Text = Players[2].Resources[2].ToString();
                Player3EpicAmountLabel.Text = Players[2].Resources[3].ToString();
                Player3Research1PictureBox.Image = Players[2].CurrentResearchImages[0];
                Player3Research2PictureBox.Image = Players[2].CurrentResearchImages[1];
                Player3Research3PictureBox.Image = Players[2].CurrentResearchImages[2];
                Player3PointProgressBar.Value = Players[2].Points;
                Player3BudgetPictureBox.Location = new System.Drawing.Point(47, 501 - (Players[2].Budget * 40));
                Player3BudgetPictureBox.Height = 20 + (40 * Players[2].Budget);
            }
            if (Players[3] != null)
            {
                Player4MoneyAmountLabel.Text = Players[3].Money.ToString();
                Player4ScoreAmountLabel.Text = Players[3].Points.ToString();
                Player4BudgetAmountLabel.Text = Players[3].Budget.ToString();
                Player4TaskAmountLabel.Text = Players[3].Resources[0].ToString();
                Player4StoryAmountLabel.Text = Players[3].Resources[1].ToString();
                Player4FeatureAmountLabel.Text = Players[3].Resources[2].ToString();
                Player4EpicAmountLabel.Text = Players[3].Resources[3].ToString();
                Player4Research1PictureBox.Image = Players[3].CurrentResearchImages[0];
                Player4Research2PictureBox.Image = Players[3].CurrentResearchImages[1];
                Player4Research3PictureBox.Image = Players[3].CurrentResearchImages[2];
                Player4PointProgressBar.Value = Players[3].Points;
                Player4BudgetPictureBox.Location = new System.Drawing.Point(55, 501 - (Players[3].Budget * 40));
                Player4BudgetPictureBox.Height = 20 + (40 * Players[3].Budget);
            }
            TaskSupplyLabel.Text = ResourceSupply[0].ToString();
            StorySupplyLabel.Text = ResourceSupply[1].ToString();
            FeatureSupplyLabel.Text = ResourceSupply[2].ToString();
            EpicSupplyLabel.Text = ResourceSupply[3].ToString();

            
        }

        

        private void cmdRoll_Click(object sender, EventArgs e)
        {
            if (AllowDiceRoll)
            {
                RollDice();
            }
        }

        public void RollDice()
        {
            int outParse;

            //Checks if DiceAmount text is empty or not and is an integer number between 1 and 7
            if (!string.IsNullOrWhiteSpace(DiceAmount.Text) && int.TryParse(DiceAmount.Text, out outParse) && int.Parse(DiceAmount.Text) <= 7 && int.Parse(DiceAmount.Text) >= 1)
            {
                DiceTotal = 0;
                
                UseResearch ResearchForm = new UseResearch(ActivePlayer);
                this.Enabled = false;
                ResearchForm.ShowDialog();
                //Creates a list for the diceImages and adds to list
                List<PictureBox> diceImageList = new List<PictureBox>();
                for (int i = 1; i < 7; i++)
                {
                    diceImageList.Add((PictureBox)Controls.Find("diceImage" + i, true)[0]);
                }

                //Creates list of DiceBoxes and adds to list
                List<PictureBox> DicePictureBoxList = new List<PictureBox>();
                for (int i = 1; i <= 7; i++)
                {
                    DicePictureBoxList.Add((PictureBox)Controls.Find("Dice" + i + "PictureBox", true)[0]);
                }

                //Creates a list of random numbers
                Random r = new Random();
                List<int> RandomInts = new List<int>();
                for (int i = 1; i <= 7; i++)
                {
                    RandomInts.Add(r.Next(0, 6));
                }

                //Resets the images in dicepicturebox
                Dice1PictureBox.Image = null;
                Dice2PictureBox.Image = null;
                Dice3PictureBox.Image = null;
                Dice4PictureBox.Image = null;
                Dice5PictureBox.Image = null;
                Dice6PictureBox.Image = null;
                Dice7PictureBox.Image = null;


                //Places a random dice image into a dicebox 
                for (int i = 0; i <= int.Parse(DiceAmount.Text) - 1; i++)
                {
                    DicePictureBoxList[i].Image = diceImageList[RandomInts[i]].Image;
                    DiceTotal += RandomInts[i] + 1;
                }
            }
            else
            {
                TextLabel.Text = "Try a number between 1 and 7.";
                return;
            }
        }
        #region Dragging Events
        /// <summary>
        /// Changes the global IsDragging to true when a user clicks and holds on this control
        /// https://stackoverflow.com/questions/13245083/dragdrop-to-move-a-picture-box-at-run-time   Courtesy of Alan
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GamePiecePictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            IsDragging = true;
            DragPoint = new Point(e.X, e.Y);
            ((PictureBox)sender).BringToFront();
            LeaveAreaEvent(GetWorker((PictureBox)sender));
        }

        /// <summary>
        /// Updates the control's position when mouse is moving if being dragged.
        /// https://stackoverflow.com/questions/13245083/dragdrop-to-move-a-picture-box-at-run-time   Courtesy of Alan
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GamePiecePictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (IsDragging)
            {
                int xOffset = ((PictureBox)sender).Location.X + e.X - DragPoint.X;
                int yOffset = ((PictureBox)sender).Location.Y + e.Y - DragPoint.Y;
                if (xOffset >= 0 && (xOffset + ((PictureBox)sender).Size.Width <= this.Size.Width)) //horizontal boundaries
                {
                    if (yOffset >= 0 && (yOffset + ((PictureBox)sender).Size.Height <= this.Size.Height - 30)) //vertical boundaries (offset by 30 pixels to account for title bar)
                    {
                        ((PictureBox)sender).Location = new Point(xOffset, yOffset);
                    }

                }


            }
        }
        /// <summary>
        /// Changes the global IsDragging to false when the mouse is unclicked while dragging
        /// https://stackoverflow.com/questions/13245083/dragdrop-to-move-a-picture-box-at-run-time   Courtest of Alan
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GamePiecePictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            IsDragging = false;
            EnterAreaEvent(GetWorker((PictureBox)sender));
        }
        #endregion
        #region Worker Enter/Leave Events
        /// <summary>
        /// Searches for the area a gamepiece is in and performs an action based on the area. Use for leaving an area.
        /// </summary>
        /// <param name="input"></param>
        private void LeaveAreaEvent(Worker input)
        {
            if (IsInBoundaries(input.Control, Player1Panel))
            {
                if (input is FrontEndDeveloper && input.Owner == Players[0])
                {
                    
                    Players[0].NumFrontEndDevelopersOnHome--;
                    Player1FrontEndDevelopersLabel.Text = Players[0].NumFrontEndDevelopersOnHome.ToString();
                }
                else if (input is BackEndDeveloper && input.Owner == Players[0])
                {
                    Players[0].NumBackEndDevelopersOnHome--;
                    Player1BackEndDevelopersLabel.Text = Players[0].NumBackEndDevelopersOnHome.ToString();
                }
                else if (input is FullStackDeveloper && input.Owner == Players[0])
                {
                    Players[0].NumFullStackDevelopersOnHome--;
                    Player1FullStackDevelopersLabel.Text = Players[0].NumFullStackDevelopersOnHome.ToString();
                }
                else if (input is ProductOwner && input.Owner == Players[0])
                {
                    Players[0].NumProductOwnersOnHome--;
                    Player1ProductOwnersLabel.Text = Players[0].NumProductOwnersOnHome.ToString();
                }
                else if (input is ScrumMaster && input.Owner == Players[0])
                {
                    Players[0].NumScrumMastersOnHome--;
                    Player1ScrumMastersLabel.Text = Players[0].NumScrumMastersOnHome.ToString();
                }

            }
            else if (IsInBoundaries(input.Control, Player2Panel))
            {
                if (input is FrontEndDeveloper && input.Owner == Players[1])
                {

                    Players[1] .NumFrontEndDevelopersOnHome--;
                    Player2FrontEndDevelopersLabel.Text = Players[1] .NumFrontEndDevelopersOnHome.ToString();
                }
                else if (input is BackEndDeveloper && input.Owner == Players[1])
                {
                    Players[1] .NumBackEndDevelopersOnHome--;
                    Player2BackEndDevelopersLabel.Text = Players[1] .NumBackEndDevelopersOnHome.ToString();
                }
                else if (input is FullStackDeveloper && input.Owner == Players[1])
                {
                    Players[1] .NumFullStackDevelopersOnHome--;
                    Player2FullStackDevelopersLabel.Text = Players[1] .NumFullStackDevelopersOnHome.ToString();
                }
                else if (input is ProductOwner && input.Owner == Players[1])
                {
                    Players[1] .NumProductOwnersOnHome--;
                    Player2ProductOwnersLabel.Text = Players[1] .NumProductOwnersOnHome.ToString();
                }
                else if (input is ScrumMaster && input.Owner == Players[1])
                {
                    Players[1] .NumScrumMastersOnHome--;
                    Player2ScrumMastersLabel.Text = Players[1] .NumScrumMastersOnHome.ToString();
                }

            }
            else if (IsInBoundaries(input.Control, Player3Panel))
            {
                if (input is FrontEndDeveloper && input.Owner == Players[2])
                {

                    Players[2].NumFrontEndDevelopersOnHome--;
                    Player3FrontEndDevelopersLabel.Text = Players[2].NumFrontEndDevelopersOnHome.ToString();
                }
                else if (input is BackEndDeveloper && input.Owner == Players[2])
                {
                    Players[2].NumBackEndDevelopersOnHome--;
                    Player3BackEndDevelopersLabel.Text = Players[2].NumBackEndDevelopersOnHome.ToString();
                }
                else if (input is FullStackDeveloper && input.Owner == Players[2])
                {
                    Players[2].NumFullStackDevelopersOnHome--;
                    Player3FullStackDevelopersLabel.Text = Players[2].NumFullStackDevelopersOnHome.ToString();
                }
                else if (input is ProductOwner && input.Owner == Players[2])
                {
                    Players[2].NumProductOwnersOnHome--;
                    Player3ProductOwnersLabel.Text = Players[2].NumProductOwnersOnHome.ToString();
                }
                else if (input is ScrumMaster && input.Owner == Players[2])
                {
                    Players[2].NumScrumMastersOnHome--;
                    Player3ScrumMastersLabel.Text = Players[2].NumScrumMastersOnHome.ToString();
                }

            }
            else if (IsInBoundaries(input.Control, Player4Panel))
            {
                if (input is FrontEndDeveloper && input.Owner == Players[3])
                {

                    Players[3].NumFrontEndDevelopersOnHome--;
                    Player4FrontEndDevelopersLabel.Text = Players[3].NumFrontEndDevelopersOnHome.ToString();
                }
                else if (input is BackEndDeveloper && input.Owner == Players[3])
                {
                    Players[3].NumBackEndDevelopersOnHome--;
                    Player4BackEndDevelopersLabel.Text = Players[3].NumBackEndDevelopersOnHome.ToString();
                }
                else if (input is FullStackDeveloper && input.Owner == Players[3])
                {
                    Players[3].NumFullStackDevelopersOnHome--;
                    Player4FullStackDevelopersLabel.Text = Players[3].NumFullStackDevelopersOnHome.ToString();
                }
                else if (input is ProductOwner && input.Owner == Players[3])
                {
                    Players[3].NumProductOwnersOnHome--;
                    Player4ProductOwnersLabel.Text = Players[3].NumProductOwnersOnHome.ToString();
                }
                else if (input is ScrumMaster && input.Owner == Players[3])
                {
                    Players[3].NumScrumMastersOnHome--;
                    Player4ScrumMastersLabel.Text = Players[3].NumScrumMastersOnHome.ToString();
                }

            }
            else if (IsInBoundaries(input.Control, ContractOutPanel))
            {
                ContractOutWorkersList.Remove(input);
                UpdateContractOutWorkerLocations();
            }
            else if (IsInBoundaries(input.Control, WorkerSupplyPictureBox))
            {
                if (input is FrontEndDeveloper)
                {
                    FrontEndDeveloperSupplyList.Remove(input);
                    FrontEndDeveloperSupplyLabel.Text = FrontEndDeveloperSupplyList.Count.ToString();
                }
                else if (input is BackEndDeveloper)
                {
                    BackEndDeveloperSupplyList.Remove(input);
                    BackEndDeveloperSupplyLabel.Text = BackEndDeveloperSupplyList.Count.ToString();
                }
                else if (input is FullStackDeveloper)
                {
                    FullStackDeveloperSupplyList.Remove(input);
                    FullStackDeveloperSupplyLabel.Text = FullStackDeveloperSupplyList.Count.ToString();
                }
                else if (input is ProductOwner)
                {
                    ProductOwnerSupplyList.Remove(input);
                    ProductOwnerSupplyLabel.Text = ProductOwnerSupplyList.Count.ToString();
                }
                else if (input is ScrumMaster)
                {
                    ScrumMasterSupplyList.Remove(input);
                    ScrumMasterSupplyLabel.Text = ScrumMasterSupplyList.Count.ToString();
                }

            }
            else if (IsInBoundaries(input.Control, TasklandPanel))
            {
                TaskWorkersList.Remove(input);
                Point startPoint = GetRawLocation(TasklandPanel);
                startPoint.X += RESOURCE_X_OFFSET;
                startPoint.Y += RESOURCE_Y_OFFSET;
                UpdateResourceWorkerLocations(TaskWorkersList, startPoint);
            }
            else if (IsInBoundaries(input.Control, StorylandPanel))
            {
                StoryWorkersList.Remove(input);
                Point startPoint = GetRawLocation(StorylandPanel);
                startPoint.X += RESOURCE_X_OFFSET;
                startPoint.Y += RESOURCE_Y_OFFSET;
                UpdateResourceWorkerLocations(StoryWorkersList, startPoint);
            }
            else if (IsInBoundaries(input.Control, FeaturelandPanel))
            {
                FeatureWorkersList.Remove(input);
                Point startPoint = GetRawLocation(FeaturelandPanel);
                startPoint.X += RESOURCE_X_OFFSET;
                startPoint.Y += RESOURCE_Y_OFFSET;
                UpdateResourceWorkerLocations(FeatureWorkersList, startPoint);
            }
            else if (IsInBoundaries(input.Control, EpiclandPanel))
            {
                EpicWorkersList.Remove(input);
                Point startPoint = GetRawLocation(EpiclandPanel);
                startPoint.X += RESOURCE_X_OFFSET;
                startPoint.Y += RESOURCE_Y_OFFSET;
                UpdateResourceWorkerLocations(EpicWorkersList, startPoint);
            }
            else if (IsInBoundaries(input.Control, GooglePanel))
            {
                GoogleWorker = null;
            }
            else if (IsInBoundaries(input.Control, JobFairPanel))
            {
                JobFairWorkersList.Remove(input);
                UpdateJobFairWorkerLocations();
            }
            else if (IsInBoundaries(input.Control, BigWigPanel))
            {
                BigWigsWorker = null;
            }
            else if (IsInBoundaries(input.Control, TechnologyCard1PictureBox))
            {
                TechnologyCardWorkerArray[0] = null;
            }
            else if (IsInBoundaries(input.Control, TechnologyCard2PictureBox))
            {
                TechnologyCardWorkerArray[1] = null;
            }
            else if (IsInBoundaries(input.Control, TechnologyCard3PictureBox))
            {
                TechnologyCardWorkerArray[2] = null;
            }
            else if (IsInBoundaries(input.Control, TechnologyCard4PictureBox))
            {
                TechnologyCardWorkerArray[3] = null;
            }

        }

        /// <summary>
        /// Searches for the area a gamepiece is in and performs an action based on the area. Use for entering an area.
        /// </summary>
        /// <param name="input"></param>
        private void EnterAreaEvent(Worker input)
        {
            if (IsInBoundaries(input.Control, Player1Panel))
            {
                if (input.Owner == null)
                {
                    input.Owner = Players[0];
                    Players[0].WorkerList.Add(input);
                }
                if (input is FrontEndDeveloper && input.Owner == Players[0])
                {
                    input.Control.Location = GetRawLocation(Player1FrontEndDeveloperHomePictureBox);
                    Players[0].NumFrontEndDevelopersOnHome++;
                    Player1FrontEndDevelopersLabel.Text = Players[0].NumFrontEndDevelopersOnHome.ToString();
                }
                else if (input is BackEndDeveloper && input.Owner == Players[0])
                {
                    input.Control.Location = GetRawLocation(Player1BackEndDeveloperHomePictureBox);
                    Players[0].NumBackEndDevelopersOnHome++;
                    Player1BackEndDevelopersLabel.Text = Players[0].NumBackEndDevelopersOnHome.ToString();
                }
                else if (input is FullStackDeveloper && input.Owner == Players[0])
                {
                    input.Control.Location = GetRawLocation(Player1FullStackDeveloperHomePictureBox);
                    Players[0].NumFullStackDevelopersOnHome++;
                    Player1FullStackDevelopersLabel.Text = Players[0].NumFullStackDevelopersOnHome.ToString();
                }
                 else if (input is ProductOwner && input.Owner == Players[0])
                {
                    input.Control.Location = GetRawLocation(Player1ProductOwnerHomePictureBox);
                    Players[0].NumProductOwnersOnHome++;
                    Player1ProductOwnersLabel.Text = Players[0].NumProductOwnersOnHome.ToString();
                }
                 else if (input is ScrumMaster && input.Owner == Players[0])
                {
                    input.Control.Location = GetRawLocation(Player1ScrumMasterHomePictureBox);
                    Players[0].NumScrumMastersOnHome++;
                    Player1ScrumMastersLabel.Text = Players[0].NumScrumMastersOnHome.ToString();
                }
                
            }
            else if (IsInBoundaries(input.Control, Player2Panel))
            {
                if (input.Owner == null)
                {
                    input.Owner = Players[1];
                    Players[1].WorkerList.Add(input);
                }
                if (input is FrontEndDeveloper && input.Owner == Players[1])
                {
                    input.Control.Location = GetRawLocation(Player2FrontEndDeveloperHomePictureBox);
                    Players[1] .NumFrontEndDevelopersOnHome++;
                    Player2FrontEndDevelopersLabel.Text = Players[1].NumFrontEndDevelopersOnHome.ToString();
                }
                else if (input is BackEndDeveloper && input.Owner == Players[1])
                {
                    input.Control.Location = GetRawLocation(Player2BackEndDeveloperHomePictureBox);
                    Players[1] .NumBackEndDevelopersOnHome++;
                    Player2BackEndDevelopersLabel.Text = Players[1].NumBackEndDevelopersOnHome.ToString();
                }
                else if (input is FullStackDeveloper && input.Owner == Players[1])
                {
                    input.Control.Location = GetRawLocation(Player2FullStackDeveloperHomePictureBox);
                    Players[1] .NumFullStackDevelopersOnHome++;
                    Player2FullStackDevelopersLabel.Text = Players[1].NumFullStackDevelopersOnHome.ToString();
                }
                else if (input is ProductOwner && input.Owner == Players[1])
                {
                    input.Control.Location = GetRawLocation(Player2ProductOwnerHomePictureBox);
                    Players[1].NumProductOwnersOnHome++;
                    Player2ProductOwnersLabel.Text = Players[1].NumProductOwnersOnHome.ToString();
                }
                else if (input is ScrumMaster && input.Owner == Players[1])
                {
                    input.Control.Location = GetRawLocation(Player2ScrumMasterHomePictureBox);
                    Players[1].NumScrumMastersOnHome++;
                    Player2ScrumMastersLabel.Text = Players[1].NumScrumMastersOnHome.ToString();
                }

            }
            else if (IsInBoundaries(input.Control, Player3Panel))
            {
                if (input.Owner == null)
                {
                    input.Owner = Players[2];
                    Players[2].WorkerList.Add(input);
                }
                if (input is FrontEndDeveloper && input.Owner == Players[2])
                {
                    input.Control.Location = GetRawLocation(Player3FrontEndDeveloperHomePictureBox);
                    Players[2].NumFrontEndDevelopersOnHome++;
                    Player3FrontEndDevelopersLabel.Text = Players[2].NumFrontEndDevelopersOnHome.ToString();
                }
                else if (input is BackEndDeveloper && input.Owner == Players[2])
                {
                    input.Control.Location = GetRawLocation(Player3BackEndDeveloperHomePictureBox);
                    Players[2].NumBackEndDevelopersOnHome++;
                    Player3BackEndDevelopersLabel.Text = Players[2].NumBackEndDevelopersOnHome.ToString();
                }
                else if (input is FullStackDeveloper && input.Owner == Players[2])
                {
                    input.Control.Location = GetRawLocation(Player3FullStackDeveloperHomePictureBox);
                    Players[2].NumFullStackDevelopersOnHome++;
                    Player3FullStackDevelopersLabel.Text = Players[2].NumFullStackDevelopersOnHome.ToString();
                }
                else if (input is ProductOwner && input.Owner == Players[2])
                {
                    input.Control.Location = GetRawLocation(Player3ProductOwnerHomePictureBox);
                    Players[2].NumProductOwnersOnHome++;
                    Player3ProductOwnersLabel.Text = Players[2].NumProductOwnersOnHome.ToString();
                }
                else if (input is ScrumMaster && input.Owner == Players[2])
                {
                    input.Control.Location = GetRawLocation(Player3ScrumMasterHomePictureBox);
                    Players[2].NumScrumMastersOnHome++;
                    Player3ScrumMastersLabel.Text = Players[2].NumScrumMastersOnHome.ToString();
                }

            }
            else if (IsInBoundaries(input.Control, Player4Panel))
            {
                if (input.Owner == null)
                {
                    input.Owner = Players[3];
                    Players[3].WorkerList.Add(input);
                }
                if (input is FrontEndDeveloper && input.Owner == Players[3])
                {
                    input.Control.Location = GetRawLocation(Player4FrontEndDeveloperHomePictureBox);
                    Players[3].NumFrontEndDevelopersOnHome++;
                    Player4FrontEndDevelopersLabel.Text = Players[3].NumFrontEndDevelopersOnHome.ToString();
                }
                else if (input is BackEndDeveloper && input.Owner == Players[3])
                {
                    input.Control.Location = GetRawLocation(Player4BackEndDeveloperHomePictureBox);
                    Players[3].NumBackEndDevelopersOnHome++;
                    Player4BackEndDevelopersLabel.Text = Players[3].NumBackEndDevelopersOnHome.ToString();
                }
                else if (input is FullStackDeveloper && input.Owner == Players[3])
                {
                    input.Control.Location = GetRawLocation(Player4FullStackDeveloperHomePictureBox);
                    Players[3].NumFullStackDevelopersOnHome++;
                    Player4FullStackDevelopersLabel.Text = Players[3].NumFullStackDevelopersOnHome.ToString();
                }
                else if (input is ProductOwner && input.Owner == Players[3])
                {
                    input.Control.Location = GetRawLocation(Player4ProductOwnerHomePictureBox);
                    Players[3].NumProductOwnersOnHome++;
                    Player4ProductOwnersLabel.Text = Players[3].NumProductOwnersOnHome.ToString();
                }
                else if (input is ScrumMaster && input.Owner == Players[3])
                {
                    input.Control.Location = GetRawLocation(Player4ScrumMasterHomePictureBox);
                    Players[3].NumScrumMastersOnHome++;
                    Player4ScrumMastersLabel.Text = Players[3].NumScrumMastersOnHome.ToString();
                }
            }
            else if (IsInBoundaries(input.Control, ContractOutPanel))
            {
                ContractOutWorkersList.Add(input);
                UpdateContractOutWorkerLocations();
            }
            else if (IsInBoundaries(input.Control, WorkerSupplyPictureBox))
            {
                if (input is FrontEndDeveloper)
                {
                    input.Owner = null;
                    input.Control.Location = GetRawLocation(FrontEndDeveloperSupplyPictureBox);
                    FrontEndDeveloperSupplyList.Add(input);
                    FrontEndDeveloperSupplyLabel.Text = FrontEndDeveloperSupplyList.Count.ToString();

                }
                else if (input is BackEndDeveloper)
                {
                    input.Owner = null;
                    input.Control.Location = GetRawLocation(BackEndDeveloperSupplyPictureBox);
                    BackEndDeveloperSupplyList.Add(input);
                    BackEndDeveloperSupplyLabel.Text = BackEndDeveloperSupplyList.Count.ToString();
                }
                else if (input is FullStackDeveloper)
                {
                    input.Owner = null;
                    input.Control.Location = GetRawLocation(FullStackDeveloperSupplyPictureBox);
                    FullStackDeveloperSupplyList.Add(input);
                    FullStackDeveloperSupplyLabel.Text = FullStackDeveloperSupplyList.Count.ToString();
                }
                else if (input is ProductOwner)
                {
                    input.Owner = null;
                    input.Control.Location = GetRawLocation(ProductOwnerSupplyPictureBox);
                    ProductOwnerSupplyList.Add(input);
                    ProductOwnerSupplyLabel.Text = ProductOwnerSupplyList.Count.ToString();
                }
                else if (input is ScrumMaster)
                {
                    input.Owner = null;
                    input.Control.Location = GetRawLocation(ScrumMasterSupplyPictureBox);
                    ScrumMasterSupplyList.Add(input);
                    ScrumMasterSupplyLabel.Text = ScrumMasterSupplyList.Count.ToString();
                }

            }
            else if (IsInBoundaries(input.Control, TasklandPanel))
            {
                if (!(input is ProductOwner) && TaskWorkersList.Count < 7 && input.Owner != null)
                {
                    TaskWorkersList.Add(input);
                    Point startPoint = GetRawLocation(TasklandPanel);
                    startPoint.X += RESOURCE_X_OFFSET;
                    startPoint.Y += RESOURCE_Y_OFFSET;
                    UpdateResourceWorkerLocations(TaskWorkersList, startPoint);
                }
            }
            else if (IsInBoundaries(input.Control, StorylandPanel))
            {
                if (!(input is ProductOwner) && StoryWorkersList.Count < 7 && input.Owner != null)
                {
                    StoryWorkersList.Add(input);
                    Point startPoint = GetRawLocation(StorylandPanel);
                    startPoint.X += RESOURCE_X_OFFSET;
                    startPoint.Y += RESOURCE_Y_OFFSET;
                    UpdateResourceWorkerLocations(StoryWorkersList, startPoint);
                }
            }
            else if (IsInBoundaries(input.Control, FeaturelandPanel))
            {
                if (!(input is ProductOwner) && FeatureWorkersList.Count < 7 && input.Owner != null)
                {
                    FeatureWorkersList.Add(input);
                    Point startPoint = GetRawLocation(FeaturelandPanel);
                    startPoint.X += RESOURCE_X_OFFSET;
                    startPoint.Y += RESOURCE_Y_OFFSET;
                    UpdateResourceWorkerLocations(FeatureWorkersList, startPoint);
                }
            }
            else if (IsInBoundaries(input.Control, EpiclandPanel))
            {
                if (!(input is ProductOwner) && EpicWorkersList.Count < 7 && input.Owner != null)
                {
                    EpicWorkersList.Add(input);
                    Point startPoint = GetRawLocation(EpiclandPanel);
                    startPoint.X += RESOURCE_X_OFFSET;
                    startPoint.Y += RESOURCE_Y_OFFSET;
                    UpdateResourceWorkerLocations(EpicWorkersList, startPoint);
                }
            }
            else if (IsInBoundaries(input.Control, GooglePanel))
            {
                if (GoogleWorker == null && (input is ProductOwner || input is ScrumMaster) && input.Owner != null)
                {
                    GoogleWorker = input;
                    Point location = GetRawLocation(GooglePanel);
                    location.X += 31;
                    location.Y += 55;
                    input.Control.Location = location;
                }
            }
            else if (IsInBoundaries(input.Control, JobFairPanel))
            {
                if (JobFairWorkersList.Count < 2 && input.Owner != null)
                {
                    if ((JobFairWorkersList.Count == 1 && JobFairWorkersList[0].Owner == input.Owner ) || JobFairWorkersList.Count == 0)
                    {
                        JobFairWorkersList.Add(input);
                        UpdateJobFairWorkerLocations();
                    }
                }
            }
            else if (IsInBoundaries(input.Control, BigWigPanel))
            {
                if (BigWigsWorker == null && (input is ProductOwner || input is ScrumMaster) && input.Owner != null)
                {
                    BigWigsWorker = input;
                    Point location = GetRawLocation(BigWigPanel);
                    location.X += 29;
                    location.Y += 55;
                    input.Control.Location = location;
                }
            }
            else if (IsInBoundaries(input.Control, TechnologyCard1PictureBox))
            {
                if (TechnologyCardWorkerArray[0] == null && input.Owner != null)
                {
                    TechnologyCardWorkerArray[0] = input;
                    Point location = GetRawLocation(TechnologyCard1PictureBox);
                    location.X += 20;
                    location.Y += 21;
                    input.Control.Location = location;
                }
                
            }
            else if (IsInBoundaries(input.Control, TechnologyCard2PictureBox))
            {
                if (TechnologyCardWorkerArray[1] == null && input.Owner != null)
                {
                    TechnologyCardWorkerArray[1] = input;
                    Point location = GetRawLocation(TechnologyCard2PictureBox);
                    location.X += 20;
                    location.Y += 21;
                    input.Control.Location = location;
                }
            }
            else if (IsInBoundaries(input.Control, TechnologyCard3PictureBox))
            {
                if (TechnologyCardWorkerArray[2] == null && input.Owner != null)
                {
                    TechnologyCardWorkerArray[2] = input;
                    Point location = GetRawLocation(TechnologyCard3PictureBox);
                    location.X += 20;
                    location.Y += 21;
                    input.Control.Location = location;
                }
            }
            else if (IsInBoundaries(input.Control, TechnologyCard4PictureBox))
            {
                if (TechnologyCardWorkerArray[3] == null && input.Owner != null)
                {
                    TechnologyCardWorkerArray[3] = input;
                    Point location = GetRawLocation(TechnologyCard4PictureBox);
                    location.X += 20;
                    location.Y += 21;
                    input.Control.Location = location;
                }
            }
        }
        #endregion
        #region Helper Methods
        /// <summary>
        /// Test if a control 1 is within the area of control 2
        /// </summary>
        /// <param name="input"></param>
        /// <param name="area"></param>
        /// <returns></returns>
        private bool IsInBoundaries(Control input, Control area)
        {
            bool output = false;
            Point areaPoint = GetRawLocation(area);
            if (input.Location.X < (areaPoint.X + area.Size.Width) && input.Location.X > areaPoint.X) //check if within horizontal boundaries
            {
                if (input.Location.Y < (areaPoint.Y + area.Size.Height) && input.Location.Y > areaPoint.Y)
                {
                    output = true;
                }
            }
            return output;
        }

        /// <summary>
        /// Get the worker object associated with the picturebox
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private Worker GetWorker(PictureBox input)
        {
            Worker output = null;
            foreach (Worker w in WorkersList)
            {
                if (w.Control == input)
                {
                    output = w;
                    break;
                }
            }
                    
            return output;
        }

        /// <summary>
        /// Get location of control based on main form.
        /// </summary>
        /// <param name="referenceControl"></param>
        /// <returns></returns>
        private Point GetRawLocation(Control referenceControl)
        {
            Point output = referenceControl.FindForm().PointToClient(referenceControl.Parent.PointToScreen(referenceControl.Location));

            return output;
        }

        /// <summary>
        /// Set locations of worker PictureBoxes on Contract Out spot. Spreads them evenly in a limited area.
        /// </summary>
        private void UpdateContractOutWorkerLocations()
        {
            if (ContractOutWorkersList.Count > 0)
            {
                int VerticalOffset = 71;
                int HorizontalOffset = 4;
                int Width = 110;
                int Spacing = Width / ContractOutWorkersList.Count;
                Point StartPoint = GetRawLocation(ContractOutPanel);
                StartPoint.X += HorizontalOffset;
                StartPoint.Y += VerticalOffset + Y_OFFSET;
                for (int i = 0; i < ContractOutWorkersList.Count; i++)
                {
                    ContractOutWorkersList[i].Control.Location = StartPoint;
                    StartPoint.X += Spacing;
                }
            }
        }

        /// <summary>
        /// Set locations of worker PictureBoxes on a resource.
        /// </summary>
        /// <param name="workerList"></param>
        /// <param name="referencePoint"></param>
        private void UpdateResourceWorkerLocations( List<Worker> workerList, Point referencePoint)
        {
            Point[] points = new Point[7];
            points[0] = new Point(0, 0);
            points[1] = new Point(46, 0);
            points[2] = new Point(92, 0);
            points[3] = new Point(92, 48);
            points[4] = new Point(92, 97);
            points[5] = new Point(46, 97);
            points[6] = new Point(0, 97);
            for (int i = 0; i < workerList.Count; i++)
            {
                workerList[i].Control.Location = new Point(points[i].X + referencePoint.X, points[i].Y + referencePoint.Y);
            }

        }

        /// <summary>
        /// Set locations of worker PictureBoxes on Job Fair spot.
        /// </summary>
        private void UpdateJobFairWorkerLocations()
        {
            Point[] points = new Point[2];
            points[0] = GetRawLocation(JobFairPanel);
            points[1] = GetRawLocation(JobFairPanel);
            points[0].X += 22;
            points[0].Y += 80;
            points[1].X += 68;
            points[1].Y += 80;
            for (int i = 0; i < JobFairWorkersList.Count; i++)
            {
                JobFairWorkersList[i].Control.Location = points[i];
            }
        }

        /// When user clicks the "Task List" button, the form for the list is triggered and displayed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            frm2.ShowDialog();
        }

        /// <summary>
        /// Update images for player research tokens
        /// </summary>
        /// <param name="playerNum"></param>
        private void UpdatePlayerResearch(int playerNum)
        {
            if (playerNum >= 0 && playerNum <= 3)
            {
                switch (playerNum)
                {
                    case 0:
                        if (Players[0].CurrentResearchImages[0] != null) { Player1Research1PictureBox.Image = Players[0].CurrentResearchImages[0]; }
                        if (Players[0].CurrentResearchImages[1] != null) { Player1Research2PictureBox.Image = Players[0].CurrentResearchImages[1]; }
                        if (Players[0].CurrentResearchImages[2] != null) { Player1Research3PictureBox.Image = Players[0].CurrentResearchImages[2]; }
                        break;
                    case 1:
                        if (Players[1].CurrentResearchImages[0] != null) { Player2Research1PictureBox.Image = Players[1].CurrentResearchImages[0]; }
                        if (Players[1].CurrentResearchImages[1] != null) { Player2Research2PictureBox.Image = Players[1].CurrentResearchImages[1]; }
                        if (Players[1].CurrentResearchImages[2] != null) { Player2Research3PictureBox.Image = Players[1].CurrentResearchImages[2]; }
                        break;
                    case 2:
                        if (Players[2].CurrentResearchImages[0] != null) { Player3Research1PictureBox.Image = Players[2].CurrentResearchImages[0]; }
                        if (Players[2].CurrentResearchImages[1] != null) { Player3Research2PictureBox.Image = Players[2].CurrentResearchImages[1]; }
                        if (Players[2].CurrentResearchImages[2] != null) { Player3Research3PictureBox.Image = Players[2].CurrentResearchImages[2]; }
                        break;
                    case 3:
                        if (Players[3].CurrentResearchImages[0] != null) { Player4Research1PictureBox.Image = Players[3].CurrentResearchImages[0]; }
                        if (Players[3].CurrentResearchImages[1] != null) { Player4Research2PictureBox.Image = Players[3].CurrentResearchImages[1]; }
                        if (Players[3].CurrentResearchImages[2] != null) { Player4Research3PictureBox.Image = Players[3].CurrentResearchImages[2]; }
                        break;
                }
                
            }
            
        }
        #endregion
        #region ToolTip Hover Events
        /// <summary>
        /// Show tooltip for ContractOutPanel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContractOutPanel_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Contract Out Area", ContractOutPanel);
        }

        /// <summary>
        /// Show ToolTip for Points Progressbar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProgressBar1_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Player Points", Player1PointProgressBar);
        }

        /// <summary>
        /// Show ToolTip for Points Progressbar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProgressBar2_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Player Points", Player2PointProgressBar);
        }

        /// <summary>
        /// Show ToolTip for Points Progressbar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProgressBar3_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Player Points", Player3PointProgressBar);
        }

        /// <summary>
        /// Show ToolTip for Points Progressbar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProgressBar4_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Player Points", Player4PointProgressBar);
        }

        /// <summary>
        /// Show ToolTip for Taskland
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TasklandPanel_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Taskland Resource Spot", TasklandPanel);
        }

        /// <summary>
        /// Show ToolTip for Storyland
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StorylandPanel_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Storyland Resource Spot", StorylandPanel);
        }

        /// <summary>
        /// Show ToolTip for Featureland
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FeaturelandPanel_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Featureland Resource Spot", FeaturelandPanel);
        }

        /// <summary>
        /// Show ToolTip for Epicland
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EpiclandPanel_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Epicland Resource Spot", EpiclandPanel);
        }

        /// <summary>
        /// Show ToolTip for Google Spot
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GooglePanel_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Google Research Spot", GooglePanel);
        }

        /// <summary>
        /// Show ToolTip for job Fair Spot
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void JobFairPanel_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Job Fair Hiring Spot", JobFairPanel);
        }

        /// <summary>
        /// Show ToolTip for Big Wigs Spot
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BigWigPanel_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Big Wigs Budget Increase Spot", BigWigPanel);
        }

        /// <summary>
        /// Show ToolTip for Budget Bar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BudgetStripPictureBox_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Budget Bar", BudgetStripPictureBox);
        }

        /// <summary>
        /// Show ToolTip for Client Card Spot
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Client1PictureBox_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Client Card", Client1PictureBox);
        }

        /// <summary>
        /// Show ToolTip for Client Card Spot
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Client2PictureBox_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Client Card", Client2PictureBox);
        }

        /// <summary>
        /// Show ToolTip for Client Card Spot
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Client3PictureBox_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Client Card", Client3PictureBox);
        }

        /// <summary>
        /// Show ToolTip for Client Card Spot
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Client4PictureBox_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Client Card", Client4PictureBox);
        }

        

        /// <summary>
        /// Show ToolTip for Product Owner
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductOwner_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Product Owner", (PictureBox)sender);
        }

        /// <summary>
        /// Show ToolTip for Scrum Master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScrumMaster_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Scrum Master", (PictureBox)sender);
        }

        /// <summary>
        /// Show ToolTip for Front End Developer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrontEndDeveloper_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Front End Developer", (PictureBox)sender);
        }

        /// <summary>
        /// Show ToolTip for Back End Developer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackEndDeveloper_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Back End Developer", (PictureBox)sender);
        }

        /// <summary>
        /// Show ToolTip for Full Stack Developer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FullStackDeveloper_MouseHover(object sender, EventArgs e)
        {
            ToolTip.Show("Full Stack Developer", (PictureBox)sender);
        }
        #endregion
        #region ResearchClickEvents
        private void Player1Research1PictureBox_Click(object sender, EventArgs e)
        {
            Players[0].UseResearch(0);
            UpdatePlayerResearch(0);
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            Players[0].ResetResearchUsed();
            UpdatePlayerResearch(0);
        }

        private void Player1Research2PictureBox_Click(object sender, EventArgs e)
        {
            Players[0].UseResearch(1);
            UpdatePlayerResearch(0);
        }

        private void Player1Research3PictureBox_Click(object sender, EventArgs e)
        {
            Players[0].UseResearch(2);
            UpdatePlayerResearch(0);
        }

        private void Player2Research1PictureBox_Click(object sender, EventArgs e)
        {
            Players[1].UseResearch(0);
            UpdatePlayerResearch(1);
        }

        private void Player2Research2PictureBox_Click(object sender, EventArgs e)
        {
            Players[1].UseResearch(1);
            UpdatePlayerResearch(1);
        }

        private void Player2Research3PictureBox_Click(object sender, EventArgs e)
        {
            Players[1].UseResearch(2);
            UpdatePlayerResearch(1);
        }

        private void Player3Research1PictureBox_Click(object sender, EventArgs e)
        {
            Players[2].UseResearch(0);
            UpdatePlayerResearch(2);
        }

        private void Player3Research2PictureBox_Click(object sender, EventArgs e)
        {
            Players[2].UseResearch(1);
            UpdatePlayerResearch(2);
        }

        private void Player3Research3PictureBox_Click(object sender, EventArgs e)
        {
            Players[2].UseResearch(2);
            UpdatePlayerResearch(2);
        }

        private void Player4Research1PictureBox_Click(object sender, EventArgs e)
        {
            Players[3].UseResearch(0);
            UpdatePlayerResearch(3);
        }

        private void Player4Research2PictureBox_Click(object sender, EventArgs e)
        {
            Players[3].UseResearch(1);
            UpdatePlayerResearch(3);
        }

        private void Player4Research3PictureBox_Click(object sender, EventArgs e)
        {
            Players[3].UseResearch(2);
            UpdatePlayerResearch(3);
        }
        #endregion

        private void TechnologyCard1PictureBox_Click(object sender, EventArgs e)
        {
            if (TechnologyCardSlots[0] != null)
            {
                ActivePlayer.TechnologyCardList.Add(TechnologyCardSlots[0]);
                TechnologyCardSlots[0].Owner = ActivePlayer;
                TechnologyCardSlots[0].GetEvent();
                TechnologyCardSlots[0] = null;
                FillTechnologyCardSlots();

            }
            
        }

        private void TechnologyCard2PictureBox_Click(object sender, EventArgs e)
        {
            if (TechnologyCardSlots[1] != null)
            {
                ActivePlayer.TechnologyCardList.Add(TechnologyCardSlots[1]);
                TechnologyCardSlots[1].Owner = ActivePlayer;
                TechnologyCardSlots[1].GetEvent();
                TechnologyCardSlots[1] = null;
                FillTechnologyCardSlots();
                
            }
            
        }

        private void TechnologyCard3PictureBox_Click(object sender, EventArgs e)
        {
            if (TechnologyCardSlots[2] != null)
            {
                ActivePlayer.TechnologyCardList.Add(TechnologyCardSlots[2]);
                TechnologyCardSlots[2].Owner = ActivePlayer;
                TechnologyCardSlots[2].GetEvent();
                TechnologyCardSlots[2] = null;
                FillTechnologyCardSlots();
                
            }
            
        }

        private void TechnologyCard4PictureBox_Click(object sender, EventArgs e)
        {
            if (TechnologyCardSlots[3] != null)
            {
                ActivePlayer.TechnologyCardList.Add(TechnologyCardSlots[3]);
                TechnologyCardSlots[3].Owner = ActivePlayer;
                TechnologyCardSlots[3].GetEvent();
                TechnologyCardSlots[3] = null;
                FillTechnologyCardSlots();
                
            }
            
        }

        private void Player1TitleTextBox_TextChanged(object sender, EventArgs e)
        {
            if (Players[0] != null)
            {
                Players[0].Name = Player1TitleTextBox.Text;
            }
        }

        private void Player2TitleTextBox_TextChanged(object sender, EventArgs e)
        {
            if (Players[1] != null)
            {
                Players[1].Name = Player2TitleTextBox.Text;
            }
        }

        private void Player3TitleTextBox_TextChanged(object sender, EventArgs e)
        {
            if (Players[2] != null)
            {
                Players[2].Name = Player3TitleTextBox.Text;
            }
        }

        private void Player4TitleTextBox_TextChanged(object sender, EventArgs e)
        {
            if (Players[3] != null)
            {
                Players[3].Name = Player4TitleTextBox.Text;
            }
        }

        private void Player1TechnologyCardHomePictureBox_Click(object sender, EventArgs e)
        {
            if (ActivePlayer == Players[0])
            {
                TechnologyCardInventoryForm form = new TechnologyCardInventoryForm(Players[0]);
                form.ShowDialog();
            }
        }


        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void rulesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.Show();
        }

    }
}
