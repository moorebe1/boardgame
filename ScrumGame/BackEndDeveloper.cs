﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumGame
{
    /// <summary>
    /// Load image based on this class type and the owner (player color)
    /// </summary>
    class BackEndDeveloper : Worker
    {
        protected override void LoadImage()
        {
            if (Owner == null)
                Control.BackgroundImage = global::ScrumGame.Properties.Resources.BackEndDeveloperNeutral;
            else
            {
                switch (Owner.PlayerNumber)
                {
                    case 1:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.BackEndDeveloperPlayer1;
                        break;
                    case 2:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.BackEndDeveloperPlayer2;
                        break;
                    case 3:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.BackEndDeveloperPlayer3;
                        break;
                    case 4:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.BackEndDeveloperPlayer4;
                        break;
                }
            }
        }
    }
}
