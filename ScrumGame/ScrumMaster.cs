﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumGame
{
    /// <summary>
    /// Load image based on this class type and the owner (player color)
    /// </summary>
    class ScrumMaster : Worker
    {
        protected override void LoadImage()
        {
            if (Owner == null)
                Control.BackgroundImage = global::ScrumGame.Properties.Resources.ScrumMasterNeutral;
            else
            {
                switch (Owner.PlayerNumber)
                {
                    case 1:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.ScrumMasterPlayer1;
                        break;
                    case 2:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.ScrumMasterPlayer2;
                        break;
                    case 3:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.ScrumMasterPlayer3;
                        break;
                    case 4:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.ScrumMasterPlayer4;
                        break;
                }
            }
        }
    }
}
