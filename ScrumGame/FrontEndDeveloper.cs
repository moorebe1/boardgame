﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrumGame
{
    class FrontEndDeveloper : Worker
    {
        /// <summary>
        /// Load image based on this class type and the owner (player color)
        /// </summary>
        protected override void LoadImage()
        {
            if (Owner == null)
                Control.BackgroundImage = global::ScrumGame.Properties.Resources.FrontEndDeveloperNeutral;
            else
            {
                switch (Owner.PlayerNumber)
                {
                    case 1:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.FrontEndDeveloperPlayer1;
                        break;
                    case 2:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.FrontEndDeveloperPlayer2;
                        break;
                    case 3:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.FrontEndDeveloperPlayer3;
                        break;
                    case 4:
                        Control.BackgroundImage = global::ScrumGame.Properties.Resources.FrontEndDeveloperPlayer4;
                        break;
                }
            }
        }
    }
}
